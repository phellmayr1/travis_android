package fhhgb.travis.philipp.travis_android.rest.model.response;

/**
 * Created by Hellmayr on 06.06.2016.
 */
public class TripUser {

	boolean admin;
	String status;

	User user;

	public TripUser(boolean admin, String status, User user) {
		this.admin = admin;
		this.status = status;
		this.user = user;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
