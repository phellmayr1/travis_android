package fhhgb.travis.philipp.travis_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.fragments.TripCreatedFragment;
import fhhgb.travis.philipp.travis_android.fragments.TripInvitedFragment;
import fhhgb.travis.philipp.travis_android.fragments.TripOverFragment;

/**
 * Show the overview over all the trips.
 * The overview is divided into the three fragments: created, invited and over
 */
public class TripsOverviewActivity extends AppCompatActivity {

	@Bind(R.id.pager)
	ViewPager viewPager;

	@Bind(R.id.tabs)
	TabLayout tabLayout;

	@Bind(R.id.findLocation)
	FloatingActionButton findLocationButton;

	@Bind(R.id.selectLocation)
	FloatingActionButton selectLocationButton;

	TripCreatedFragment tripCreatedFragment;
	TripInvitedFragment tripInvitedFragment;
	TripOverFragment tripOverFragment;
	private static final int SELECT_LOCATION_REQUEST_CODE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trips_overview);
		ButterKnife.bind(this);

		setupTabs();

		setupToolbar();

		findLocationButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(TripsOverviewActivity.this, FindTripActivity.class);
				startActivity(intent);
			}
		});

		selectLocationButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent("fhhgb.travis.philipp.travis_android.SearchLocationActivity"),
						SELECT_LOCATION_REQUEST_CODE);
			}
		});
	}

	private void setupTabs() {
		tripCreatedFragment = new TripCreatedFragment();
		tripInvitedFragment = new TripInvitedFragment();
		tripOverFragment = new TripOverFragment();

		setupViewPager();

		tabLayout.setupWithViewPager(viewPager);
	}

	@Override
	public void onResume() {  // After a pause OR at startup
		super.onResume();
		setupTabs();
		viewPager.setCurrentItem(SessionData.tripOverviewFragmentId);
	}

	@Override
	protected void onStop() {
		SessionData.tripOverviewFragmentId = viewPager.getCurrentItem();
		super.onStop();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		//Plan a new trip
		if (requestCode == SELECT_LOCATION_REQUEST_CODE) {
			if (data != null) {

				Bundle resultBundle = data.getExtras();
				Intent intent = new Intent(this, NewTripActivity.class);
				intent.putExtra(Constants.LOCATION_ID, (resultBundle.getInt(Constants.LOCATION_ID)));
				intent.putExtra(Constants.LOCATION_NAME, (resultBundle.getString(Constants.LOCATION_NAME)));
				startActivity(intent);
			}
		}

	}

	private void setupViewPager() {

		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

		adapter.addFragment(tripCreatedFragment, getString(R.string.created));
		adapter.addFragment(tripInvitedFragment, getString(R.string.invited));
		adapter.addFragment(tripOverFragment, getString(R.string.over));

		viewPager.setAdapter(adapter);
	}

	class ViewPagerAdapter extends FragmentPagerAdapter {

		private final List<Fragment> mFragmentList = new ArrayList<>();
		private final List<String> mFragmentTitleList = new ArrayList<>();

		public ViewPagerAdapter(FragmentManager manager) {
			super(manager);
		}

		@Override
		public Fragment getItem(int position) {
			return mFragmentList.get(position);
		}

		@Override
		public int getCount() {
			return mFragmentList.size();
		}

		public void addFragment(Fragment fragment, String title) {
			mFragmentList.add(fragment);
			mFragmentTitleList.add(title);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return mFragmentTitleList.get(position);
		}
	}

	private void setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		onBackPressed();
		return super.onOptionsItemSelected(item);
	}
}
