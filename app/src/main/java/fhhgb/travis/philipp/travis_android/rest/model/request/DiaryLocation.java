package fhhgb.travis.philipp.travis_android.rest.model.request;

/**
 * @author Philipp
 *         Created on 08.01.2016.
 */

public class DiaryLocation {

	int sort;
	double latitude;
	double longitude;
	double altitude;

	public DiaryLocation(int sort, double latitude, double longitude, double altitude) {
		this.sort = sort;
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
	}

	public DiaryLocation(int sort, double latitude, double longitude) {
		this.sort = sort;
		this.latitude = latitude;
		this.longitude = longitude;
		altitude=0;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}
}
