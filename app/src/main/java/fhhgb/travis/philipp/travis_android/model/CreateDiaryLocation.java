package fhhgb.travis.philipp.travis_android.model;

/**
 * @author Philipp
 *         Created on 08.01.2016.
 */

public class CreateDiaryLocation extends CreateDiaryElement {

	double latitude;
	double longitude;
	double altitude;

	public CreateDiaryLocation(int sort, double latitude, double longitude, double altitude) {
		super(sort);
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}
}
