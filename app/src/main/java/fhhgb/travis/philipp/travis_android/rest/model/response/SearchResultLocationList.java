package fhhgb.travis.philipp.travis_android.rest.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

/**
 * @author Philipp
 *         Created on 08.01.2016.
 */

public class SearchResultLocationList {

	@SerializedName("searchResult")
	LinkedList<Location> searchResults;

	public LinkedList<Location> getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(LinkedList<Location> searchResults) {
		this.searchResults = searchResults;
	}
}
