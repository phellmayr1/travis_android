package fhhgb.travis.philipp.travis_android.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.io.IOException;

import fhhgb.travis.philipp.travis_android.SessionData;
import fhhgb.travis.philipp.travis_android.rest.service.DiaryService;
import fhhgb.travis.philipp.travis_android.rest.service.LocationService;
import fhhgb.travis.philipp.travis_android.rest.service.LoginService;
import fhhgb.travis.philipp.travis_android.rest.service.PushService;
import fhhgb.travis.philipp.travis_android.rest.service.RatingService;
import fhhgb.travis.philipp.travis_android.rest.service.SearchService;
import fhhgb.travis.philipp.travis_android.rest.service.TripPlannerService;
import fhhgb.travis.philipp.travis_android.rest.service.UserService;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * @author Philipp
 *         Created on 28.10.2015.
 */

public class RestClient implements Interceptor {

	private LoginService loginService;
	private UserService userService;
	private LocationService locationService;
	private DiaryService diaryService;
	private PushService pushService;
	private SearchService searchService;
	private RatingService ratingService;
	private TripPlannerService tripPlannerService;

	public RestClient() {

		//TODO remove logging interceptor at deployment
		/*
		* The HttpLoggingInterceptor is used for logging the json request and response
		* in the logcat. It should be removed at deployment
		 */
		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		// set your desired log level
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient httpClient = new OkHttpClient();
		// add your other interceptors …
		// add logging as last interceptor

		httpClient.interceptors().add(this);  // <-- this adds the header with the sessionId to every request
		httpClient.interceptors().add(logging);  // <-- this is the important line!

		//Convert Json String to Date
		Gson gson = new GsonBuilder()
				.setDateFormat("yyyy-MM-dd")
				.create();

		Retrofit restAdapter = new Retrofit.Builder()
				.baseUrl(RestConstants.BASE_URL)
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.addConverterFactory(GsonConverterFactory.create(gson))
				.client(httpClient)
				.build();

		loginService = restAdapter.create(LoginService.class);
		userService = restAdapter.create(UserService.class);
		locationService = restAdapter.create(LocationService.class);
		diaryService = restAdapter.create(DiaryService.class);
		pushService = restAdapter.create(PushService.class);
		searchService = restAdapter.create(SearchService.class);
		ratingService = restAdapter.create(RatingService.class);
		tripPlannerService = restAdapter.create(TripPlannerService.class);
	}

	public LoginService getLoginService() {
		return loginService;
	}

	public UserService getUserService() {
		return userService;
	}

	public LocationService getLocationService() {
		return locationService;
	}

	public DiaryService getDiaryService() {
		return diaryService;
	}

	public PushService getPushService() {
		return pushService;
	}

	public SearchService getSearchService() {
		return searchService;
	}

	public RatingService getRatingService() {
		return ratingService;
	}

	public TripPlannerService getTripPlannerService() {
		return tripPlannerService;
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		Request originalRequest = chain.request();

		if (SessionData.getSessionId() != null) {
			Request newRequest = originalRequest.newBuilder()
					.header("sessionId", SessionData.getSessionId())
							//		.header("Content-Type", "application/json")
					.build();
			return chain.proceed(newRequest);
		} else {
			Request newRequest = originalRequest.newBuilder()
					//	.header("Content-Type", "application/json")
					.build();
			return chain.proceed(newRequest);
		}
	}
}
