package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.ProfileActivity;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;

/**
 * Created by Hellmayr on 10.05.2016.
 */
public class LocationUserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

	ImageView profileImageView;
	TextView profileTextView;

	Activity currentActivity;
	User user;

	public void setUser(User user) {
		this.user = user;
	}

	public LocationUserViewHolder(View itemView, Activity currentActivity) {
		super(itemView);

		this.currentActivity = currentActivity;

		itemView.setOnClickListener(this);
		profileImageView = (ImageView) itemView.findViewById(R.id.visitor_image);
		profileTextView = (TextView) itemView.findViewById(R.id.visitor_text);
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(currentActivity, ProfileActivity.class);
		intent.putExtra(Constants.USER_ID_TO_SHOW, user.getId());
		currentActivity.startActivity(intent);
	}
}
