package fhhgb.travis.philipp.travis_android.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.ProfileActivity;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.SessionData;
import fhhgb.travis.philipp.travis_android.rest.model.response.SearchResultUser;
import fhhgb.travis.philipp.travis_android.rest.service.SearchService;
import fhhgb.travis.philipp.travis_android.views.SearchUserAdapter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Search for a user on the backend
 */
public class SearchUserFragment extends Fragment {


	@Bind(R.id.search_results_listview)
	ListView searchResultsListView;

	SearchUserAdapter arrayAdapter = null;


	public SearchUserFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view= inflater.inflate(R.layout.fragment_search_user, container, false);
		ButterKnife.bind(this, view);

		return view;
	}

	public void loadUsers(String searchText) {
		SearchService searchService = SessionData.getRestClient().getSearchService();


		if (searchText.equals("")) {
			searchText = " ";
		}
		Call<List<SearchResultUser>> b = searchService.searchUser(searchText);
		b.enqueue(new Callback<List<SearchResultUser>>() {

			@Override
			public void onResponse(Response<List<SearchResultUser>> response, Retrofit retrofit) {

				if (response.body() != null && response.body()!= null) {

					arrayAdapter = new SearchUserAdapter(getActivity(), R.layout.user_listitem,
							response.body());

					searchResultsListView.setAdapter(arrayAdapter);

					searchResultsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

							if (id != -1) {
								SearchResultUser selectedFromList = (SearchResultUser) (searchResultsListView.getItemAtPosition
										(position));

								Intent intent = new Intent(getActivity(), ProfileActivity.class);
								intent.putExtra(Constants.USER_ID_TO_SHOW, selectedFromList.getId());
								startActivity(intent);
							}

						}
					});
				}
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getActivity(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p/>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener {

		// TODO: Update argument type and name
		void onFragmentInteraction(Uri uri);
	}
}
