package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;

/**
 * Created by Hellmayr on 13.04.2016.
 */
public class DiaryTextViewHolder extends DiaryViewHolder {

	protected TextView diaryText;


	public DiaryTextViewHolder(View v, Activity activity, User userToShow) {
		super(v, activity, userToShow);
		diaryText = (TextView) v.findViewById(R.id.diary_text);
	}
}
