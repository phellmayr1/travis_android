package fhhgb.travis.philipp.travis_android;

import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewManager;

import fhhgb.travis.philipp.travis_android.model.CreateDiaryElement;

/**
 * @author Philipp
 *         Created on 04.01.2016.
 */

public class TextMenuItemClicked implements Toolbar.OnMenuItemClickListener {

	CreateDiaryElement diaryElement;
	CreateDiaryActivity createDiaryActivity;

	public TextMenuItemClicked(CreateDiaryElement diaryElement, CreateDiaryActivity currentActivity) {
		this.diaryElement =diaryElement;
		createDiaryActivity=currentActivity;
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {

		((ViewManager) diaryElement.getCardview().getParent()).removeView(diaryElement.getCardview());

		createDiaryActivity.removeFromList(diaryElement);

		return false;
	}
}
