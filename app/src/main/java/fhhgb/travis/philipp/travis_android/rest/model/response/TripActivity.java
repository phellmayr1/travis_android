package fhhgb.travis.philipp.travis_android.rest.model.response;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

import fhhgb.travis.philipp.travis_android.rest.model.request.LocationWithCoordinates;

/**
 * Created by Hellmayr on 06.06.2016.
 */
public class TripActivity implements Serializable {

	int id;
	String name;
	String text;
	String beginDate;
	String endDate;
	double costs;
	LocationWithCoordinates location;
	int locationId;
	String timezone;

	Calendar begin;
	Calendar end;

	String currency;

	//Only used to show the name
	String locationName;

	public TripActivity(int id) {
		this.id = id;
		begin = new GregorianCalendar();
		end = new GregorianCalendar();
	}

	public TripActivity() {
		begin = new GregorianCalendar();
		end = new GregorianCalendar();
	}

	public TripActivity(String name) {
		this.name = name;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public double getCosts() {
		return costs;
	}

	public void setCosts(double costs) {
		this.costs = costs;
	}

	public LocationWithCoordinates getLocation() {
		return location;
	}

	public void setLocation(LocationWithCoordinates location) {
		this.location = location;
	}

	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Calendar getBegin() {
		return begin;
	}

	public void setBegin(Calendar begin) {
		this.begin = begin;
	}

	public Calendar getEnd() {
		return end;
	}

	public void setEnd(Calendar end) {
		this.end = end;
	}
}
