package fhhgb.travis.philipp.travis_android.rest.model.request;

import java.io.Serializable;

/**
 * @author Philipp
 *         Created on 30.12.2015.
 */

public class LocationWithCoordinates implements Serializable {

	int id;
	String name;
	double latitude;
	double longitude;
	double altitude;
	String googlePlaceId;
	boolean visited;
	boolean rated;

	public LocationWithCoordinates(String name) {
		this.name = name;
		latitude = 0;
		longitude = 0;
		altitude = 0;
	}

	public LocationWithCoordinates(String name, double latitude, double longitude, double altitude) {
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
	}

	public LocationWithCoordinates(String name, double latitude, double longitude, double altitude, String googlePlaceId) {
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
		this.googlePlaceId = googlePlaceId;
	}

	public LocationWithCoordinates(String name, double latitude, double longitude, String googlePlaceId) {
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.googlePlaceId = googlePlaceId;
	}

	public LocationWithCoordinates(String name, double latitude, double longitude) {
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		altitude = 0;
	}

	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	public String getGooglePlaceId() {
		return googlePlaceId;
	}

	public void setGooglePlaceId(String googlePlaceId) {
		this.googlePlaceId = googlePlaceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public boolean isRated() {
		return rated;
	}

	public void setRated(boolean rated) {
		this.rated = rated;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}


