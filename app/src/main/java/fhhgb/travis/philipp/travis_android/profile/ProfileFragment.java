package fhhgb.travis.philipp.travis_android.profile;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.datepicker.DatePickerBuilder;
import com.codetroopers.betterpickers.datepicker.DatePickerDialogFragment;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.ProfileActivity;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.SessionData;
import fhhgb.travis.philipp.travis_android.helper.DateUtils;
import fhhgb.travis.philipp.travis_android.rest.model.request.Text;
import fhhgb.travis.philipp.travis_android.rest.model.response.Id;
import fhhgb.travis.philipp.travis_android.rest.model.response.SearchResultUser;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;
import fhhgb.travis.philipp.travis_android.rest.service.UserService;
import fhhgb.travis.philipp.travis_android.views.SearchUserAdapter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

	@Bind(R.id.follower_icon)
	TextView followerIcon;

	@Bind(R.id.followed_icon)
	TextView followedIcon;

	@Bind(R.id.age_icon)
	TextView ageIcon;

	@Bind(R.id.shorttext_icon)
	TextView shortTextIcon;

	@Bind(R.id.location_icon)
	TextView locationIcon;

	@Bind(R.id.next_icon)
	TextView nextIcon;

	@Bind(R.id.facebook_button)
	Button showOnFacebookButton;

	@Bind(R.id.age)
	TextView age;

	@Bind(R.id.age_text)
	TextView ageText;

	@Bind(R.id.from_location)
	TextView homeLocation;

	@Bind(R.id.next)
	TextView nextLocation;

	@Bind(R.id.shorttext)
	EditText shortText;

	@Bind(R.id.longText)
	EditText longText;

	@Bind(R.id.follower)
	TextView follower;

	@Bind(R.id.followed)
	TextView followed;

	@Bind(R.id.follower_section)
	RelativeLayout followerSection;

	@Bind(R.id.followed_section)
	RelativeLayout followedSection;

	User userToShow;

	int nextLocationRequestCode = 1001;
	int homeLocationRequestCode = 1002;

	public ProfileFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public void initInputs() {


		followerSection.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Dialog dialog = new Dialog(getActivity());
				dialog.setContentView(R.layout.dialog_follower_list);

				final ListView lv = (ListView ) dialog.findViewById(R.id.userList);

				final TextView content = (TextView ) dialog.findViewById(R.id.dialogContent);
				content.setText(getResources().getString(R.string.follower));

				UserService userService = SessionData.getRestClient().getUserService();

				Call<List<SearchResultUser>> retval = userService.findFollowerForUser(userToShow.getId(), Constants.FACEBOOK_IMAGE_WIDTH_DIALOG, Constants
						.FACEBOOK_IMAGE_HEIGHT_DIALOG);
				retval.enqueue(new Callback<List<SearchResultUser>>() {

					@Override
					public void onResponse(Response<List<SearchResultUser>> response, Retrofit retrofit) {


						SearchUserAdapter arrayAdapter = new SearchUserAdapter(getActivity(), R.layout.user_listitem,
								response.body());

						lv.setAdapter(arrayAdapter);

						lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

								if (id != -1) {
									SearchResultUser selectedFromList = (SearchResultUser) (lv.getItemAtPosition
											(position));

									Intent intent = new Intent(getActivity(), ProfileActivity.class);
									intent.putExtra(Constants.USER_ID_TO_SHOW, selectedFromList.getId());
									startActivity(intent);
								}

							}
						});
					}

					@Override
					public void onFailure(Throwable t) {
						Toast.makeText(getContext(), "FAIL", Toast.LENGTH_LONG).show();
					}
				});
				dialog.show();
			}
		});


		followedSection.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Dialog dialog = new Dialog(getActivity());
				dialog.setContentView(R.layout.dialog_follower_list);

				final ListView lv = (ListView ) dialog.findViewById(R.id.userList);
				final TextView content = (TextView ) dialog.findViewById(R.id.dialogContent);
				content.setText(getResources().getString(R.string.following));

				UserService userService = SessionData.getRestClient().getUserService();

				Call<List<SearchResultUser>> retval = userService.findFollowingsForUser(userToShow.getId(), Constants
						.FACEBOOK_IMAGE_WIDTH_DIALOG, Constants
						.FACEBOOK_IMAGE_HEIGHT_DIALOG);
				retval.enqueue(new Callback<List<SearchResultUser>>() {

					@Override
					public void onResponse(Response<List<SearchResultUser>> response, Retrofit retrofit) {

						SearchUserAdapter arrayAdapter = new SearchUserAdapter(getActivity(), R.layout.user_listitem,
								response.body());

						lv.setAdapter(arrayAdapter);

						lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

								if (id != -1) {
									SearchResultUser selectedFromList = (SearchResultUser) (lv.getItemAtPosition
											(position));

									Intent intent = new Intent(getActivity(), ProfileActivity.class);
									intent.putExtra(Constants.USER_ID_TO_SHOW, selectedFromList.getId());
									startActivity(intent);
								}

							}
						});
					}

					@Override
					public void onFailure(Throwable t) {
						Toast.makeText(getContext(), "FAIL", Toast.LENGTH_LONG).show();
					}
				});
				dialog.show();
			}
		});



		userToShow = ((ProfileActivity) getActivity()).getUserToShow();

		shortText.setText(userToShow.getShortText());
		longText.setText(userToShow.getLongText());
		follower.setText(userToShow.getFollowers() + "");
		followed.setText(userToShow.getFollowing() + "");

		if (userToShow.getId() != SessionData.getCurrentUser().getId()) {
			shortText.setKeyListener(null);
			longText.setKeyListener(null);
			homeLocation.setKeyListener(null);
			nextLocation.setKeyListener(null);

			showOnFacebookButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(userToShow.getProfileUrl()
					));
					startActivity(browserIntent);
				}
			});
		} else {

			showOnFacebookButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(SessionData.getFbProfile().getLinkUri()
									.toString()
					));
					startActivity(browserIntent);
				}
			});

			nextLocation.setText(getString(R.string.click_to_select));
			homeLocation.setText(getString(R.string.click_to_select));
			age.setText(getString(R.string.click_to_select));
			shortText.setHint(getString(R.string.click_to_select));
			longText.setHint(getString(R.string.click_to_select));

			nextLocation.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

					getActivity().startActivityForResult(new Intent("fhhgb.travis.philipp.travis_android.SearchLocationActivity"),
							nextLocationRequestCode);
				}
			});

			homeLocation.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

					getActivity().startActivityForResult(new Intent("fhhgb.travis.philipp.travis_android.SearchLocationActivity"),
							homeLocationRequestCode);
				}
			});

			//Enable possibility to change birthdate
			age.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					DatePickerBuilder dpb = new DatePickerBuilder()
							.setFragmentManager(getFragmentManager())
							.setStyleResId(R.style.BetterPickersDialogFragment);
					dpb.show();
					dpb.addDatePickerDialogHandler(new DatePickerDialogFragment.DatePickerDialogHandler() {
						@Override
						public void onDialogDateSet(int reference, int year, int monthOfYear, int dayOfMonth) {

							UserService userService = SessionData.getRestClient().getUserService();

							Call<Void> retval = userService.changeBirthday(DateUtils.intToDateString(year, monthOfYear + 1, dayOfMonth));
							retval.enqueue(new Callback<Void>() {

								@Override
								public void onResponse(Response<Void> response, Retrofit retrofit) {
									//	Toast.makeText(getContext(), "RESP ", Toast.LENGTH_LONG).show();
								}

								@Override
								public void onFailure(Throwable t) {
									Toast.makeText(getContext(), "FAIL", Toast.LENGTH_LONG).show();
								}
							});

							age.setText(DateUtils.calculateAge(DateUtils.intToDate(year, monthOfYear, dayOfMonth)) + "");
						}
					});
				}
			});

			longText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				@Override
				public void onFocusChange(View view, boolean b) {

					if (!b) {
						UserService userService = SessionData.getRestClient().getUserService();

						Text text = new Text(longText.getText().toString());

						Call<Id> retval = userService.changeLongText(text);
						retval.enqueue(new Callback<Id>() {

							@Override
							public void onResponse(Response<Id> response, Retrofit retrofit) {
//							Toast.makeText(getContext(), "RESP " + response.body().getId(), Toast.LENGTH_LONG).show();
							}

							@Override
							public void onFailure(Throwable t) {
								Toast.makeText(getContext(), "FAIL", Toast.LENGTH_LONG).show();
							}
						});
					}
				}
			});

			shortText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				@Override
				public void onFocusChange(View view, boolean b) {

					if (!b) {
						UserService userService = SessionData.getRestClient().getUserService();

						Text text = new Text(shortText.getText().toString());

						Call<Id> retval = userService.changeShortText(text);
						retval.enqueue(new Callback<Id>() {

							@Override
							public void onResponse(Response<Id> response, Retrofit retrofit) {
								//			Toast.makeText(getContext(), "RESP " + response.body().getId(), Toast
								// .LENGTH_LONG).show();
							}

							@Override
							public void onFailure(Throwable t) {
								Toast.makeText(getContext(), "FAIL", Toast.LENGTH_LONG).show();
							}
						});
					}
				}
			});
		}
			if (userToShow.getNextLocation() != null) {
				nextLocation.setText(userToShow.getNextLocation().getName());
			}
			if (userToShow.getHomeLocation() != null) {
				homeLocation.setText(userToShow.getHomeLocation().getName());
			}
			if (userToShow.getBirthday() != null) {
				age.setText(DateUtils.calculateAge(userToShow.getBirthday()) + "");
			}
			if (userToShow.getShortText() != null&& !userToShow.getShortText().equals("")) {
				shortText.setText(userToShow.getShortText());
			}
			if (userToShow.getLongText() != null && !userToShow.getLongText().equals("")) {
				longText.setText(userToShow.getLongText());
			}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_profile, container, false);
		ButterKnife.bind(this, view);

		Typeface material = Typeface.createFromAsset(getActivity().getAssets(), "material.ttf");
		locationIcon.setTypeface(material);
		nextIcon.setTypeface(material);
		shortTextIcon.setTypeface(material);
		followedIcon.setTypeface(material);
		followerIcon.setTypeface(material);
		ageIcon.setTypeface(material);

		if (userToShow != null) {
			initInputs();
		}

		return view;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == nextLocationRequestCode) {
			Bundle resultBundle = data.getExtras();
			nextLocation.setText(resultBundle.getString(Constants.LOCATION_NAME));
			assignNextLocationToUser(resultBundle.getInt(Constants.LOCATION_ID));
		}

		if (requestCode == homeLocationRequestCode) {
			Bundle resultBundle = data.getExtras();
			homeLocation.setText(resultBundle.getString(Constants.LOCATION_NAME));
			assignHomeLocationToUser(resultBundle.getInt(Constants.LOCATION_ID));
		}
	}

	private void assignHomeLocationToUser(int locationId) {

		UserService userService = SessionData.getRestClient().getUserService();

		Call<Id> retval = userService.changeHomeLocation(locationId);

		retval.enqueue(new Callback<Id>() {

			@Override
			public void onResponse(Response<Id> response, Retrofit retrofit) {
//				Toast.makeText(getContext(), "RESP " + response.body().getId(), Toast.LENGTH_LONG).show();

			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getContext(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	private void assignNextLocationToUser(int locationId) {

		UserService userService = SessionData.getRestClient().getUserService();

		Call<Id> retval = userService.changeNextLocation(locationId);

		retval.enqueue(new Callback<Id>() {

			@Override
			public void onResponse(Response<Id> response, Retrofit retrofit) {
//				Toast.makeText(getContext(), "RESP " + response.body().getId(), Toast.LENGTH_LONG).show();

			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getContext(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	public void incrementFollowerText() {
		follower.setText((Integer.parseInt(follower.getText().toString()))+1+"");

	}

	public void decrementFollowerText() {
		follower.setText((Integer.parseInt(follower.getText().toString()))-1+"");



	}
}
