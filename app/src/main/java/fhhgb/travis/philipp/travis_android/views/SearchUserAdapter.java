package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.rest.model.response.SearchResultUser;

/**
 * @author Philipp
 *         Created on 07.01.2016.
 */

public class SearchUserAdapter extends ArrayAdapter<SearchResultUser> {

	Context context;
	int layoutResourceId;
	List<SearchResultUser> data = null;

	public SearchUserAdapter(Context context, int layoutResourceId, List<SearchResultUser> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		gameHolder gh;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			gh = new gameHolder();
//			holder.imgIcon = (ImageView) row.findViewById(R.id.imgIcon);
			gh.placeName = (TextView) row.findViewById(R.id.txtTitle);
			gh.profileImage = (RoundedImageView) row.findViewById(R.id.profileimage);
//			holder.visited = (TextView) row.findViewById(R.id.visited);
//			holder.iconDown = (TextView) row.findViewById(R.id.icon_down);

//			Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fontawesome-webfont.ttf");
//			holder.iconDown.setTypeface(font);

			row.setTag(gh);
		} else {
			gh = (gameHolder) row.getTag();
		}

		final gameHolder holder = gh;

		SearchResultUser user = data.get(position);

		holder.placeName.setText(user.getFirstName() + " " + user.getLastName());

		ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

		final Bitmap[] bitmap = new Bitmap[1];

		String url = user.getLoginImageUrl();
		url = url.replace("width=100", "width=150");
		url = url.replace("height=100", "height=150");

		imageLoader.loadImage(url, new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				holder.profileImage.setImageBitmap(loadedImage);
				notifyDataSetChanged();
			}
		});

		return row;
	}

	static class gameHolder {

		ImageView imgIcon;
		TextView placeName;
		RoundedImageView profileImage;
		TextView visited;
		TextView iconDown;
	}

}
