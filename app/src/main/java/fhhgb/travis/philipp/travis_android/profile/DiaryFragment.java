package fhhgb.travis.philipp.travis_android.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.ProfileActivity;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.ServerError;
import fhhgb.travis.philipp.travis_android.SessionData;
import fhhgb.travis.philipp.travis_android.rest.model.request.DiaryElement;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;
import fhhgb.travis.philipp.travis_android.rest.service.DiaryService;
import fhhgb.travis.philipp.travis_android.views.DiaryAdapter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiaryFragment extends Fragment {

	@Bind(R.id.swipeRefreshLayout)
	SwipeRefreshLayout swipeRefreshLayout;

	int currentPage = 0;
	int pageSize = 5;

	private boolean loading = true;
	int pastVisiblesItems, visibleItemCount, totalItemCount;

	boolean reloaded = false;

	DiaryAdapter diaryAdapter;
	User userToShow;
	RecyclerView recList;

	LinkedList<DiaryElement> diaryElements;

	public DiaryFragment() {
		// Required empty public constructor
	}

	@Override
	public void onResume() {
		super.onResume();

		if(userToShow!=null) {
			if (!reloaded) {
				currentPage = 0;
				reload();
			} else {
				reloaded = false;
			}
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_diary, container, false);
		ButterKnife.bind(this, view);

		recList = (RecyclerView) view.findViewById(R.id.diaryList);
		recList.setHasFixedSize(true);
//		recList.setNestedScrollingEnabled(false);
		LinearLayoutManager llm = new LinearLayoutManager(getContext());
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		recList.setLayoutManager(llm);


		// Inflate the layout for this fragment
		return view;
	}



	private void reload() {
		diaryElements = new LinkedList<>();

		recList.setHasFixedSize(true);
		final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		recList.setLayoutManager(llm);

		diaryAdapter = new DiaryAdapter(diaryElements, getActivity(), userToShow);
		recList.setAdapter(diaryAdapter);

		recList.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				if (dy > 0) //check for scroll down
				{
					visibleItemCount = llm.getChildCount();
					totalItemCount = llm.getItemCount();
					pastVisiblesItems = llm.findFirstVisibleItemPosition();

					if (loading) {
						if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 1) {
							loading = false;
							initDiaryView();
						}
					}
				}
			}
		});

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				// Refresh items
				refreshItems();
			}

			void refreshItems() {
				// Load items
				// ...
				diaryElements.clear();
				currentPage = 0;
				DiaryService diaryService = SessionData.getRestClient().getDiaryService();

				Call<List<DiaryElement>> retval = diaryService.findDiaryEntries(userToShow.getId(), currentPage, pageSize);
				retval.enqueue(new Callback<List<DiaryElement>>() {
					@Override
					public void onResponse(Response<List<DiaryElement>> response, Retrofit retrofit) {
//					Toast.makeText(getActivity(), "YAAY ", Toast.LENGTH_LONG).show();

						if (ServerError.hasNoServerError(response, getActivity())) {
							diaryElements.addAll(response.body());
							diaryAdapter.notifyDataSetChanged();
							currentPage++;
							loading = true;
						}
						onItemsLoadComplete();
					}

					@Override
					public void onFailure(Throwable t) {
						Toast.makeText(getActivity(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
					}
				});

				// Load complete

			}

			void onItemsLoadComplete() {
				// Update the adapter and notify data set changed
				// ...

				// Stop refresh animation
				swipeRefreshLayout.setRefreshing(false);
			}
		});
		initDiaryView();
	}

	/*
	* this method gets called from the activity ProfileActivity
	 */
	public void startFromActivity(){

		userToShow = ((ProfileActivity) getActivity()).getUserToShow();
		reloaded = true;
		reload();
	}

	public void initDiaryView() {

		DiaryService userService = SessionData.getRestClient().getDiaryService();

		Call<List<DiaryElement>> retval = userService.findDiaryEntries(userToShow.getId(), currentPage, pageSize);

		retval.enqueue(new Callback<List<DiaryElement>>() {

			@Override
			public void onResponse(Response<List<DiaryElement>> response, Retrofit retrofit) {
//				Toast.makeText(getContext(), "RESP ", Toast.LENGTH_LONG).show();

				if (ServerError.hasNoServerError(response, getActivity())) {
					diaryElements.addAll(response.body());
					diaryAdapter.notifyDataSetChanged();
					currentPage++;
					loading = true;
				}
			}

			@Override
			public void onFailure(Throwable t) {
				ServerError.serverFail(getActivity(), t);
			}
		});
	}
}
