package fhhgb.travis.philipp.travis_android;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.rest.model.response.MainPageElement;
import fhhgb.travis.philipp.travis_android.rest.service.UserService;
import fhhgb.travis.philipp.travis_android.views.MainPageElementAdapter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Show all the liked diary entries from the current user
 */
public class LikedDiariesActivity extends AppCompatActivity {

	//This variables are used for the paging
	int currentPage = 0;
	int pageSize = 5;

	MainPageElementAdapter mainPageAdapter;
	RecyclerView recList;

	@Bind(R.id.swipeRefreshLayout)
	SwipeRefreshLayout swipeRefreshLayout;

	@Bind(R.id.noContentView)
	RelativeLayout noContentView;

	@Bind(R.id.infoImage)
	TextView infoImage;

	private boolean loading = true;
	int pastVisiblesItems, visibleItemCount, totalItemCount;

	LinkedList<MainPageElement> mainPageElements;

	boolean reloaded = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_liked_diaries);

		ButterKnife.bind(this);
		recList = (RecyclerView) this.findViewById(R.id.mainList);

		setupToolbar();

		Typeface material = Typeface.createFromAsset(this.getAssets(), "material.ttf");
		infoImage.setTypeface(material);

		swipeRefreshLayout.setVisibility(View.GONE);
		noContentView.setVisibility(View.VISIBLE);

		reloaded = true;
		reload();
	}

	private void reload() {
		mainPageElements = new LinkedList<>();

		recList.setHasFixedSize(true);
		final LinearLayoutManager llm = new LinearLayoutManager(this);
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		recList.setLayoutManager(llm);

		mainPageAdapter = new MainPageElementAdapter(mainPageElements, this);
		recList.setAdapter(mainPageAdapter);

		//Load new Elements while scrolling
		recList.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				if (dy > 0) //check for scroll down
				{
					visibleItemCount = llm.getChildCount();
					totalItemCount = llm.getItemCount();
					pastVisiblesItems = llm.findFirstVisibleItemPosition();

					if (loading) {
						if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 1) {
							loading = false;
							loadOverviewElements();
						}
					}
				}
			}
		});

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				// Refresh items
				refreshItems();
			}

			//Refresh items when swipeRefreshLayout was used from UI
			void refreshItems() {
				// Load items

				//Clear the old items
				mainPageElements.clear();
				currentPage = 0;
				UserService userService = SessionData.getRestClient().getUserService();

				Call<List<MainPageElement>> retval = userService.searchLikedDiaryEntries(SessionData.getUserId(), currentPage, pageSize,
						Constants
						.FACEBOOK_IMAGE_WIDTH, Constants.FACEBOOK_IMAGE_HEIGHT);
				retval.enqueue(new Callback<List<MainPageElement>>() {
					@Override
					public void onResponse(Response<List<MainPageElement>> response, Retrofit retrofit) {

						//Add the new items
						if (ServerError.hasNoServerError(response, LikedDiariesActivity.this)) {
							mainPageElements.addAll(response.body());
							mainPageAdapter.notifyDataSetChanged();
							currentPage++;
							loading = true;
						}
						onItemsLoadComplete();
					}

					@Override
					public void onFailure(Throwable t) {
						Toast.makeText(LikedDiariesActivity.this, "FAIL " + t.getMessage(), Toast
								.LENGTH_LONG).show();
					}
				});
				// Load complete
			}

			void onItemsLoadComplete() {
				// Update the adapter and notify data set changed
				// ...

				// Stop refresh animation
				swipeRefreshLayout.setRefreshing(false);
			}
		});
		loadOverviewElements();
	}

	/**
	 * Loads the next elements according to the currentPage and the pageSize
	 */
	public void loadOverviewElements() {

		UserService userService = SessionData.getRestClient().getUserService();

		Call<List<MainPageElement>> retval = userService.searchLikedDiaryEntries(SessionData.getUserId(), currentPage, pageSize, Constants
				.FACEBOOK_IMAGE_WIDTH, Constants.FACEBOOK_IMAGE_HEIGHT);
		retval.enqueue(new Callback<List<MainPageElement>>() {
			@Override
			public void onResponse(Response<List<MainPageElement>> response, Retrofit retrofit) {
				if (ServerError.hasNoServerError(response, LikedDiariesActivity.this)) {

					mainPageElements.addAll(response.body());
					mainPageAdapter.notifyDataSetChanged();
					currentPage++;
					loading = true;

					if (!mainPageElements.isEmpty()) {
						swipeRefreshLayout.setVisibility(View.VISIBLE);
						noContentView.setVisibility(View.GONE);
					}
				}
			}

			@Override
			public void onFailure(Throwable t) {
				ServerError.serverFail(LikedDiariesActivity.this, t);
			}
		});

	}

	private void setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
		}

		return super.onOptionsItemSelected(item);
	}
}
