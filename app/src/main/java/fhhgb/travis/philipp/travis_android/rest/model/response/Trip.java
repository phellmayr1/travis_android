package fhhgb.travis.philipp.travis_android.rest.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import fhhgb.travis.philipp.travis_android.chips.Person;
import fhhgb.travis.philipp.travis_android.rest.model.request.LocationWithCoordinates;

/**
 * Created by Hellmayr on 06.06.2016.
 */
public class Trip {

	int id;
	String title;

	LocationWithCoordinates location;
	List<TripUser> users;

	List<Person> persons;

	Calendar begin;
	Calendar end;
	double budget;

	String beginDate;
	String endDate;

	@SerializedName("tripInfo")
	String travelInformation;
	String packingList;
	String description;
	String currency;
	List<TripActivity> activities;
	String timezone;

	boolean admin;

	int locationId;

	public Trip() {
		begin = new GregorianCalendar();
		end = new GregorianCalendar();
		activities = new LinkedList<>();
		users = new LinkedList<>();
	}

	public Trip(int id) {
		this.id = id;
		begin = new GregorianCalendar();
		end = new GregorianCalendar();
		activities = new LinkedList<>();
		users = new LinkedList<>();
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public List<Person> getPersons() {
		return persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public LocationWithCoordinates getLocation() {
		return location;
	}

	public void setLocation(LocationWithCoordinates location) {
		this.location = location;
	}

	public List<TripUser> getUsers() {
		return users;
	}

	public void setUsers(List<TripUser> users) {
		this.users = users;
	}

	public Calendar getBegin() {
		return begin;
	}

	public void setBegin(Calendar begin) {
		this.begin = begin;
	}

	public Calendar getEnd() {
		return end;
	}

	public void setEnd(Calendar end) {
		this.end = end;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public String getTravelInformation() {
		return travelInformation;
	}

	public void setTravelInformation(String travelInformation) {
		this.travelInformation = travelInformation;
	}

	public String getPackingList() {
		return packingList;
	}

	public void setPackingList(String packingList) {
		this.packingList = packingList;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<TripActivity> getActivities() {
		return activities;
	}

	public void setActivities(List<TripActivity> activities) {
		this.activities = activities;
	}
}
