package fhhgb.travis.philipp.travis_android.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.SessionData;
import fhhgb.travis.philipp.travis_android.rest.model.response.FoundTrip;
import fhhgb.travis.philipp.travis_android.rest.service.TripPlannerService;
import fhhgb.travis.philipp.travis_android.views.FoundTripsAdapter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Hellmayr on 04.06.2016.
 */
public class TripOverFragment extends GooglePlaceFragment {

	@Bind(R.id.swipeRefreshLayout)
	SwipeRefreshLayout swipeRefreshLayout;

	@Bind(R.id.tripList)
	RecyclerView tripsRecyclerView;

	FoundTripsAdapter foundTripsAdapter;
	LinkedList<FoundTrip> foundTrips;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_trip_overview, container, false);
		ButterKnife.bind(this, view);

		foundTrips = new LinkedList<>();

		tripsRecyclerView.setHasFixedSize(true);
		final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		tripsRecyclerView.setLayoutManager(llm);

		foundTripsAdapter = new FoundTripsAdapter(foundTrips, getActivity(), this);
		tripsRecyclerView.setAdapter(foundTripsAdapter);

		loadCreatedTrips();

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

			@Override
			public void onRefresh() {

				foundTrips.clear();
				loadCreatedTrips();
			}
		});

		return view;
	}

	public void loadCreatedTrips() {

		TripPlannerService tripPlannerService = SessionData.getRestClient().getTripPlannerService();

		Call<List<FoundTrip>> retval = tripPlannerService.findTrips(Constants.FILTER_OVER);
		retval.enqueue(new Callback<List<FoundTrip>>() {
			@Override
			public void onResponse(Response<List<FoundTrip>> response, Retrofit retrofit) {

				swipeRefreshLayout.setRefreshing(false);
				foundTrips.addAll(response.body());
				foundTripsAdapter.notifyDataSetChanged();
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getActivity(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
			}
		});

	}

	@Override
	public void onConnected(@Nullable Bundle bundle) {

	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

}
