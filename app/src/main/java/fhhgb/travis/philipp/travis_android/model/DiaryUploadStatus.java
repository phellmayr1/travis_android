package fhhgb.travis.philipp.travis_android.model;

import java.util.LinkedList;

import fhhgb.travis.philipp.travis_android.rest.model.response.NewDiary;
import fhhgb.travis.philipp.travis_android.views.ProgressRequestBody;

/**
 * Created by Hellmayr on 22.02.2016.
 */
public class DiaryUploadStatus {

	private NewDiary newDiary;
	private String locationName;
	private LinkedList<ProgressRequestBody> uploadImages;

	public DiaryUploadStatus(NewDiary newDiary, String locationName) {
		this.newDiary = newDiary;
		this.locationName=locationName;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public NewDiary getNewDiary() {
		return newDiary;
	}

	public void setNewDiary(NewDiary newDiary) {
		this.newDiary = newDiary;
	}

	public LinkedList<ProgressRequestBody> getUploadImages() {
		if(uploadImages==null){
			uploadImages= new LinkedList<>();
		}

		return uploadImages;
	}

	public void setUploadImages(LinkedList<ProgressRequestBody> uploadImages) {
		this.uploadImages = uploadImages;
	}
}

