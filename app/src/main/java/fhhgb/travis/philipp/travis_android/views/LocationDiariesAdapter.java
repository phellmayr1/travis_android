package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.helper.DateUtils;
import fhhgb.travis.philipp.travis_android.rest.model.response.MainPageElement;

/**
 * @author Philipp
 *         Created on 07.01.2016.
 */

public class LocationDiariesAdapter extends ArrayAdapter<MainPageElement> {

	Context context;
	int layoutResourceId;
	List<MainPageElement> data = null;

	public LocationDiariesAdapter(Context context, int layoutResourceId, List<MainPageElement> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		diaryHolder gh;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			gh = new diaryHolder();
			gh.diaryName = (TextView) row.findViewById(R.id.diaryTitle);
			gh.diaryDate = (TextView) row.findViewById(R.id.diaryDate);

			row.setTag(gh);
		} else {
			gh = (diaryHolder) row.getTag();
		}

		final diaryHolder holder = gh;

		MainPageElement user = data.get(position);

		holder.diaryName.setText(user.getDiaryElement().getName());
		holder.diaryDate.setText(DateUtils.formatDate(user.getDiaryElement().getCreatedTime()));

		return row;
	}

	static class diaryHolder {

		TextView diaryName;
		TextView diaryDate;
	}

}
