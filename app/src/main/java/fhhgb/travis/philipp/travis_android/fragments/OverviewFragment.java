package fhhgb.travis.philipp.travis_android.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.CreateDiaryActivity;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.SearchActivity;
import fhhgb.travis.philipp.travis_android.ServerError;
import fhhgb.travis.philipp.travis_android.SessionData;
import fhhgb.travis.philipp.travis_android.rest.model.response.MainPageElement;
import fhhgb.travis.philipp.travis_android.rest.service.UserService;
import fhhgb.travis.philipp.travis_android.views.MainPageElementAdapter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Show all the overview elements
 */
public class OverviewFragment extends Fragment {

	int currentPage = 0;
	int pageSize = 5;

	MainPageElementAdapter mainPageAdapter;
	RecyclerView recList;

	@Bind(R.id.swipeRefreshLayout)
	SwipeRefreshLayout swipeRefreshLayout;

	@Bind(R.id.newDiaryEntry)
	android.support.design.widget.FloatingActionButton newDiaryButton;

	@Bind(R.id.noContentView)
	RelativeLayout noContentView;

	@Bind(R.id.infoImage)
	TextView infoImage;

	@Bind(R.id.searchText)
	TextView searchText;

	private boolean loading = true;
	int pastVisiblesItems, visibleItemCount, totalItemCount;

	LinkedList<MainPageElement> mainPageElements;

	boolean reloaded = false;

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_overview, container, false);
		ButterKnife.bind(this, view);
		recList = (RecyclerView) view.findViewById(R.id.mainList);

		newDiaryButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), CreateDiaryActivity.class);
				startActivity(intent);
			}
		});

		Typeface material = Typeface.createFromAsset(this.getActivity().getAssets(), "material.ttf");
		infoImage.setTypeface(material);

		searchText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), SearchActivity.class);
				startActivity(intent);
			}
		});

		swipeRefreshLayout.setVisibility(View.GONE);
		noContentView.setVisibility(View.VISIBLE);

		reloaded = true;
		reload();

		return view;
	}

	private void reload() {
		mainPageElements = new LinkedList<>();

		recList.setHasFixedSize(true);
		final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		recList.setLayoutManager(llm);

		mainPageAdapter = new MainPageElementAdapter(mainPageElements, getActivity());
		recList.setAdapter(mainPageAdapter);

		recList.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				if (dy > 0) //check for scroll down
				{
					visibleItemCount = llm.getChildCount();
					totalItemCount = llm.getItemCount();
					pastVisiblesItems = llm.findFirstVisibleItemPosition();

					if (loading) {
						if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 1) {
							loading = false;
							loadOverviewElements();
						}
					}
				}
			}
		});

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				// Refresh items
				refreshItems();
			}

			void refreshItems() {
				// Load items
				// ...
				mainPageElements.clear();
				currentPage = 0;
				UserService userService = SessionData.getRestClient().getUserService();

				Call<List<MainPageElement>> retval = userService.createMainPage(currentPage, pageSize);
				retval.enqueue(new Callback<List<MainPageElement>>() {
					@Override
					public void onResponse(Response<List<MainPageElement>> response, Retrofit retrofit) {
//					Toast.makeText(getActivity(), "YAAY ", Toast.LENGTH_LONG).show();

						if (ServerError.hasNoServerError(response, getActivity())) {
							mainPageElements.addAll(response.body());
							mainPageAdapter.notifyDataSetChanged();
							currentPage++;
							loading = true;
						}
						onItemsLoadComplete();
					}

					@Override
					public void onFailure(Throwable t) {
						Toast.makeText(getActivity(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
					}
				});

				// Load complete

			}

			void onItemsLoadComplete() {
				// Update the adapter and notify data set changed
				// ...

				// Stop refresh animation
				swipeRefreshLayout.setRefreshing(false);
			}
		});
		loadOverviewElements();
	}

	public void loadOverviewElements() {

		UserService userService = SessionData.getRestClient().getUserService();

		Call<List<MainPageElement>> retval = userService.createMainPage(currentPage, pageSize);
		retval.enqueue(new Callback<List<MainPageElement>>() {
			@Override
			public void onResponse(Response<List<MainPageElement>> response, Retrofit retrofit) {
//					Toast.makeText(getActivity(), "YAAY ", Toast.LENGTH_LONG).show();
				if (ServerError.hasNoServerError(response, getActivity())) {

					mainPageElements.addAll(response.body());
					mainPageAdapter.notifyDataSetChanged();
					currentPage++;
					loading = true;

					if (!mainPageElements.isEmpty()) {
						swipeRefreshLayout.setVisibility(View.VISIBLE);
						noContentView.setVisibility(View.GONE);
					}
				}
			}

			@Override
			public void onFailure(Throwable t) {
				ServerError.serverFail(getActivity(), t);
			}
		});

	}
}


