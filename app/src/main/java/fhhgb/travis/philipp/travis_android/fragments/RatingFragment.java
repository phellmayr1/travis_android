package fhhgb.travis.philipp.travis_android.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.SessionData;
import fhhgb.travis.philipp.travis_android.rest.model.request.Rating;
import fhhgb.travis.philipp.travis_android.rest.service.RatingService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

/**
 * A RatingFragment represents a rating for a single category.
 * Each category is in it's own Ratingfragment
 */
public class RatingFragment extends Fragment {

	@Bind(R.id.categoryImage)
	ImageView categoryImage;

	@Bind(R.id.categoryText)
	TextView categoryText;

	@Bind(R.id.categoryRating)
	RatingBar categoryRating;

	@Bind(R.id.finishButton)
	Button finishButton;

	private String category = "";
	private int locationId = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			category = getArguments().getString("category");
			locationId = getArguments().getInt(Constants.LOCATION_ID);
		}

	}

	/**
	 * Just show an information how the rating works
	 */
	private void showShowCaseView() {

		// single example
		new MaterialShowcaseView.Builder(getActivity())
				.setTarget(categoryText)
				.setDismissText("GOT IT")
				.setContentText(getString(R.string.showcase_rate_by_swipping))
				.setDelay(500) // optional but starting animations immediately in onCreate can make them choppy
				.singleUse("showCaseRateLocation") // provide a unique ID used to ensure it is only shown once
				.show();

		// sequence example
		ShowcaseConfig config = new ShowcaseConfig();
		config.setDelay(500); // half second between each showcase view

		MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(getActivity(), "showCaseRateLocation");

		sequence.setConfig(config);

		sequence.start();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_rating, container, false);

		ButterKnife.bind(this, view);
		categoryRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
				saveRating();
			}
		});
		loadRatingFromServer();

		Bitmap bm;

		//Depending on the rating, load the suiting text and image
		switch (category) {
		case Constants.RATING_CATEGORY_FOOD:
			bm = BitmapFactory.decodeResource(getResources(), R.drawable.food);
			categoryText.setText(getResources().getString(R.string.food));
			break;
		case Constants.RATING_CATEGORY_PARTY:
			bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_party);
			categoryText.setText(getResources().getString(R.string.party));
			break;
		case Constants.RATING_CATEGORY_ATTRACTIONS:
			bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_attractions);
			categoryText.setText(getResources().getString(R.string.attractions));
			break;
		case Constants.RATING_CATEGORY_ACCOMODATION:
			bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_accomodation);
			categoryText.setText(getResources().getString(R.string.accomodation));
			break;
		case Constants.RATING_CATEGORY_SHOPPING:
			bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_shopping);
			categoryText.setText(getResources().getString(R.string.shopping));
			break;
		case Constants.RATING_CATEGORY_ACTIVITIES:
			bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_activities);
			categoryText.setText(getResources().getString(R.string.activities));
			break;
		case Constants.RATING_CATEGORY_ADVENTURE:
			bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_adventure);
			categoryText.setText(getResources().getString(R.string.adventure));
			break;
		case Constants.RATING_CATEGORY_SAFETY:
			bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_security);
			categoryText.setText(getResources().getString(R.string.safety));
			break;
		case Constants.RATING_CATEGORY_WELLNES:
			bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_wellness);
			categoryText.setText(getResources().getString(R.string.wellness));
			finishButton.setVisibility(View.VISIBLE);
			break;
		default:
			bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_attractions);
			break;
		}

		categoryImage.setImageBitmap(bm);

		finishButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getActivity().onBackPressed();
			}
		});

		showShowCaseView();
		return view;
	}

	/**
	 * Load the rating from the server to prefill the ratingbar
	 */
	private void loadRatingFromServer() {

		final RatingService ratingService = SessionData.getRestClient().getRatingService();

		Call<List<fhhgb.travis.philipp.travis_android.rest.model.response.Rating>> retval = ratingService
				.findRatingsForLocationAndUserAndCategory(locationId, SessionData.getUserId(),
						category);

		retval.enqueue(new Callback<List<fhhgb.travis.philipp.travis_android.rest.model.response.Rating>>() {

			@Override
			public void onResponse(Response<List<fhhgb.travis.philipp.travis_android.rest.model.response.Rating>> response, Retrofit
					retrofit) {

				if (response.body() != null) {
					if (response.body().size() >= 1) {

						categoryRating.setRating((float) response.body().get(0).getAverage());

					}
				}

			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getActivity(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});

	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}

	/**
	 * Save the rating on the server
	 */
	public void saveRating() {
		super.onDetach();

		int rating = (int) categoryRating.getRating();

		final RatingService ratingService = SessionData.getRestClient().getRatingService();

		List<Rating> ratings = new LinkedList<>();
		Rating newRating = new Rating(category, rating);
		ratings.add(newRating);

		Call<Void> retval = ratingService.saveRatings(locationId, ratings);

		retval.enqueue(new Callback<Void>() {

			@Override
			public void onResponse(Response<Void> response, Retrofit retrofit) {

			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getActivity(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}
}
