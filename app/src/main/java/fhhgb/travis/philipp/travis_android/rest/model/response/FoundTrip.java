package fhhgb.travis.philipp.travis_android.rest.model.response;

import java.util.Date;

import fhhgb.travis.philipp.travis_android.rest.model.request.LocationWithCoordinates;

/**
 * Created by Hellmayr on 06.06.2016.
 */
public class FoundTrip {


	int id;
	String title;
	LocationWithCoordinates location;
	Date beginDate;
	Date endDate;
	double budget;
	boolean admin;
	boolean accepted;

	String status;

	public FoundTrip(int id, String title, LocationWithCoordinates location, Date beginDate, Date endDate, double budget, boolean admin,
	                 boolean accepted) {
		this.id = id;
		this.title = title;
		this.location = location;
		this.beginDate = beginDate;
		this.endDate = endDate;
		this.budget = budget;
		this.admin = admin;
		this.accepted = accepted;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public LocationWithCoordinates getLocation() {
		return location;
	}

	public void setLocation(LocationWithCoordinates location) {
		this.location = location;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isAccepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}
}
