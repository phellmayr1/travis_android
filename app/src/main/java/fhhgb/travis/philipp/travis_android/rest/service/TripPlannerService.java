package fhhgb.travis.philipp.travis_android.rest.service;

import java.util.ArrayList;
import java.util.List;

import fhhgb.travis.philipp.travis_android.rest.model.response.FoundLocation;
import fhhgb.travis.philipp.travis_android.rest.model.response.FoundTrip;
import fhhgb.travis.philipp.travis_android.rest.model.response.Trip;
import fhhgb.travis.philipp.travis_android.rest.model.response.TripActivity;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Hellmayr on 29.05.2016.
 */
public interface TripPlannerService {

	@Headers("Content-Type: application/json")
	@POST("tripPlanner")
	public Call<List<FoundLocation>> findTrip(@Body ArrayList<Object> categoryRatings);

	@Headers("Content-Type: application/json")
	@POST("tripPlanner/trip")
	public Call<Trip> createTrip(@Body Trip trip);

	@Headers("Content-Type: application/json")
	@GET("tripPlanner/trip")
	public Call<List<FoundTrip>> findTrips(@Query("filter") String filter);

	@Headers("Content-Type: application/json")
	@GET("tripPlanner/trip/{tripId}")
	public Call<Trip> loadTrip(@Path("tripId") int tripId);

	@Headers("Content-Type: application/json")
	@POST("tripPlanner/trip/{tripId}")
	public Call<Void> changeTrip(@Path("tripId") int tripId, @Body Trip trip);

	@Headers("Content-Type: application/json")
	@POST("/tripPlanner/trip/{tripId}/activity")
	public Call<Void> addTripToActivity(@Path("tripId") int tripId, @Body TripActivity trip);

	@Headers("Content-Type: application/json")
	@DELETE("/tripPlanner/activity/{activityId}")
	public Call<Void> removeTripFromActivity(@Path("activityId") int activityId);

	@Headers("Content-Type: application/json")
	@PUT("/tripPlanner/activity/{activityId}")
	public Call<Void> changeTripActivityData(@Path("activityId") int activityId, @Body TripActivity activity);

	@Headers("Content-Type: application/json")
	@POST("/tripPlanner/trip/{tripId}/inviteUser")
	public Call<Void> addUserToTrip(@Path("tripId") int tripId, @Query("userId") int userId);

	@Headers("Content-Type: application/json")
	@DELETE("/tripPlanner/trip/{tripId}/inviteUser")
	public Call<Void> removeUserFromTrip(@Path("tripId") int tripId, @Query("userId") int userId);

	@Headers("Content-Type: application/json")
	@PUT("/tripPlanner/trip/{tripId}/accept")
	public Call<Void> acceptTrip(@Path("tripId") int tripId);

	@Headers("Content-Type: application/json")
	@DELETE("/tripPlanner/trip/{tripId}/accept")
	public Call<Void> denyTrip(@Path("tripId") int tripId);


}
