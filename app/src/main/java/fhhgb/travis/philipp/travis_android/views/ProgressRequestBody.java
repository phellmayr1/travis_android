package fhhgb.travis.philipp.travis_android.views;

/**
 * Created by Hellmayr on 22.02.2016.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.SessionData;
import okio.BufferedSink;

public class ProgressRequestBody extends RequestBody {

	private File mFile;
	private ProgressBar progressBar;
	Bitmap bitmap;
	ImageView imageView;

	public ProgressBar getProgressBar() {
		return progressBar;
	}

	public void setProgressBar(ProgressBar progressBar) {
		this.progressBar = progressBar;
	}

	public ImageView getImageView() {
		return imageView;
	}

	public void setImageView(ImageView imageView) {
		this.imageView = imageView;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	private static final int DEFAULT_BUFFER_SIZE = 2048;

	RelativeLayout progressElement;

	public RelativeLayout getProgressElement() {
		return progressElement;
	}

	public void setProgressElement(RelativeLayout progressElement) {
		this.progressElement = progressElement;
	}

	public ProgressRequestBody(final File file, Bitmap b, final Context context) {
		mFile = file;

		final LayoutInflater inflater = LayoutInflater.from(context);

		CardView layout = (CardView) inflater.inflate(R.layout.cardview_diary_upload, null, false);
		LinearLayout ll = (LinearLayout) layout.findViewById(R.id.progressContainer);

		progressElement = (RelativeLayout) inflater.inflate(R.layout.cardview_upload, ll, false);

		progressBar = (ProgressBar) progressElement.findViewById(R.id.progressBar);
		bitmap = b;
	}

	@Override
	public MediaType contentType() {
		// i want to upload only images
		return MediaType.parse("image/*");
	}

	@Override
	public void writeTo(BufferedSink sink) throws IOException {
		SessionData.increaseCurrentUploadingCounter();
		long fileLength = mFile.length();
		byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
		FileInputStream in = new FileInputStream(mFile);
		long uploaded = 0;

		try {
			int read;
			Handler handler = new Handler(Looper.getMainLooper());
			while ((read = in.read(buffer)) != -1) {

				// update progress on UI thread
				handler.post(new ProgressUpdater(uploaded, fileLength));

				uploaded += read;
				sink.write(buffer, 0, read);
			}
		} finally {
			in.close();
		}
	}

	private class ProgressUpdater implements Runnable {

		private long mUploaded;
		private long mTotal;

		public ProgressUpdater(long uploaded, long total) {
			mUploaded = uploaded;
			mTotal = total;
		}

		@Override
		public void run() {
			progressBar.setProgress((int) (100 * mUploaded / mTotal));

//			if ((int) (100 * mUploaded / mTotal) >= 100) {
//				SessionData.decreaseCurrentUploadingCounter();
//
//				if(SessionData.getCurrentUploadingCounter()==0){
//					MainPageActivity.uploadingFinished();
//				}
//			}

		}
	}
}