package fhhgb.travis.philipp.travis_android.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.FindTripResultsActivity;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.model.TinderSwiperData;
import fhhgb.travis.philipp.travis_android.rest.model.request.CategoryRating;
import fhhgb.travis.philipp.travis_android.views.SwipeFlingAdapterView;

/**
 * Created by Hellmayr on 21.05.2016.
 */
public class TinderSwipeFragment extends Fragment {

	public static MyAppAdapter myAppAdapter;
	public static ViewHolder viewHolder;
	private ArrayList<TinderSwiperData> al;
	private SwipeFlingAdapterView flingContainer;

	private ArrayList<CategoryRating> categoryRatings;

	private ArrayList<String> categories;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		categories = new ArrayList<>();
		categoryRatings = new ArrayList<>();
		if (getArguments() != null) {
			categories = getArguments().getStringArrayList("cats");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.swipe_adapter_view, container, false);

		Bitmap bm;

		al = new ArrayList<>();
		String categoryText = "";

		for (String category : categories) {

			switch (category) {
			case Constants.RATING_CATEGORY_FOOD:
				bm = BitmapFactory.decodeResource(getResources(), R.drawable.food);
				categoryText = (getResources().getString(R.string.food));
				break;
			case Constants.RATING_CATEGORY_PARTY:
				bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_party);
				categoryText = (getResources().getString(R.string.party));
				break;
			case Constants.RATING_CATEGORY_ATTRACTIONS:
				bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_attractions);
				categoryText = (getResources().getString(R.string.attractions));
				break;
			case Constants.RATING_CATEGORY_ACCOMODATION:
				bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_accomodation);
				categoryText = (getResources().getString(R.string.accomodation));
				break;
			case Constants.RATING_CATEGORY_SHOPPING:
				bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_shopping);
				categoryText = (getResources().getString(R.string.shopping));
				break;
			case Constants.RATING_CATEGORY_ACTIVITIES:
				bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_activities);
				categoryText = (getResources().getString(R.string.activities));
				break;
			case Constants.RATING_CATEGORY_ADVENTURE:
				bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_adventure);
				categoryText = (getResources().getString(R.string.adventure));
				break;
			case Constants.RATING_CATEGORY_SAFETY:
				bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_security);
				categoryText = (getResources().getString(R.string.safety));
				break;
			case Constants.RATING_CATEGORY_WELLNES:
				bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_wellness);
				categoryText = (getResources().getString(R.string.wellness));
				break;
			default:
				bm = BitmapFactory.decodeResource(getResources(), R.drawable.category_attractions);
				break;
			}
			al.add(new TinderSwiperData(categoryText, bm, category));
		}

		flingContainer = (SwipeFlingAdapterView) view.findViewById(R.id.swiper);
		myAppAdapter = new MyAppAdapter(al, getActivity());
		flingContainer.setAdapter(myAppAdapter);

		flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
			@Override
			public void removeFirstObjectInAdapter() {

			}

			@Override
			public void onLeftCardExit(Object dataObject) {

				CategoryRating categoryRating = new CategoryRating(al.get(0).getCategoryType(), true);
				categoryRatings.add(categoryRating);

				al.remove(0);

				if (al.size() == 0) {
					searchFinished();
				}

				myAppAdapter.notifyDataSetChanged();
				//Do something on the left!
				//You also have access to the original object.
				//If you want to use it just cast it (String) dataObject

			}

			@Override
			public void onRightCardExit(Object dataObject) {

				CategoryRating categoryRating = new CategoryRating(al.get(0).getCategoryType(), true);
				categoryRatings.add(categoryRating);

				al.remove(0);

				if (al.size() == 0) {
					searchFinished();
				}

				myAppAdapter.notifyDataSetChanged();
			}

			@Override
			public void onAdapterAboutToEmpty(int itemsInAdapter) {

			}

			@Override
			public void onScroll(float scrollProgressPercent) {

				View view = flingContainer.getSelectedView();

				if (view != null) {
					view.findViewById(R.id.background).setAlpha(0);
					view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent
							: 0);
					view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent :
							0);
				}
			}
		});

		return view;
	}

	private void searchFinished() {
		Bundle extra = new Bundle();
		extra.putSerializable("objects", categoryRatings);

		Intent intent = new Intent(getActivity(), FindTripResultsActivity.class);
		intent.putExtra(Constants.CATEGORY_RATINGS, extra);
		startActivity(intent);
		//getActivity().finish();
	}

	public static class ViewHolder {

		public static FrameLayout background;
		public TextView DataText;
		public ImageView cardImage;
	}

	public class MyAppAdapter extends BaseAdapter {

		public List<TinderSwiperData> parkingList;
		public Context context;

		public MyAppAdapter(List<TinderSwiperData> apps, Context context) {
			this.parkingList = apps;
			this.context = context;
		}

		@Override
		public int getCount() {
			if (parkingList != null) {
				return parkingList.size();
			} else {
				return 0;
			}
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			View rowView = convertView;

			if (rowView == null) {

				LayoutInflater inflater = getActivity().getLayoutInflater();
				rowView = inflater.inflate(R.layout.tinderswiper_item, parent, false);
				// configure view holder
				viewHolder = new ViewHolder();
				viewHolder.DataText = (TextView) rowView.findViewById(R.id.categoryText);
				viewHolder.background = (FrameLayout) rowView.findViewById(R.id.background);
				viewHolder.cardImage = (ImageView) rowView.findViewById(R.id.cardImage);
				rowView.setTag(viewHolder);

			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			viewHolder.DataText.setText(parkingList.get(position).getDescription() + "");

			viewHolder.cardImage.setImageBitmap(parkingList.get(position).getImage());

			return rowView;
		}
	}
}
