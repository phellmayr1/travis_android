package fhhgb.travis.philipp.travis_android;

import android.app.Activity;
import android.widget.Toast;

import retrofit.Response;

/**
 * Created by Hellmayr on 30.01.2016.
 */
public class ServerError {

	public static boolean hasNoServerError(Response t, Activity activity) {
		boolean serverError = true;
		if (activity != null) {

			if (t.body() == null) {
				serverError = false;
				Toast.makeText(activity, "No content From Server. Try again later.", Toast.LENGTH_LONG).show();
			//	activity.finish();
			}
		}
		return serverError;
	}

	public static void serverFail(Activity activity, Throwable cause) {
		if (activity != null) {
			if (cause != null || cause.getLocalizedMessage() != null) {
				Toast.makeText(activity, "Communication failed due to " + cause.getLocalizedMessage(), Toast.LENGTH_LONG).show();
			} else {
				serverFail(activity);
			}
		}
	}

	public static void serverFail(Activity activity) {
		Toast.makeText(activity, "Communication failed.", Toast.LENGTH_LONG).show();
	}

}
