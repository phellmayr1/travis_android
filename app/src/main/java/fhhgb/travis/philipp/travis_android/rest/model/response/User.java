package fhhgb.travis.philipp.travis_android.rest.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.LinkedList;

/**
 * @author Philipp
 *         Created on 30.12.2015.
 */

public class User {

	private int id;
	private String firstName;
	private String lastName;
	@SerializedName("loginProfileUrl")
	private String profileUrl;

	boolean follow;
	private Date birthday;

	@SerializedName("loginImageUrl")
	private String imageUrl;
	private int followers;
	private int following;
	private String shortText;
	private String longText;
	private LinkedList<String> profileTags;

	private Location homeLocation;
	private Location currentLocation;
	private Location nextLocation;

	public boolean isFollow() {
		return follow;
	}

	public void setFollow(boolean follow) {
		this.follow = follow;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public int getFollowers() {
		return followers;
	}

	public void setFollowers(int followers) {
		this.followers = followers;
	}

	public int getFollowing() {
		return following;
	}

	public void setFollowing(int following) {
		this.following = following;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getLongText() {
		return longText;
	}

	public void setLongText(String longText) {
		this.longText = longText;
	}

	public LinkedList<String> getProfileTags() {
		return profileTags;
	}

	public void setProfileTags(LinkedList<String> profileTags) {
		this.profileTags = profileTags;
	}

	public Location getHomeLocation() {
		return homeLocation;
	}

	public void setHomeLocation(Location homeLocation) {
		this.homeLocation = homeLocation;
	}

	public Location getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(Location currentLocation) {
		this.currentLocation = currentLocation;
	}

	public Location getNextLocation() {
		return nextLocation;
	}

	public void setNextLocation(Location nextLocation) {
		this.nextLocation = nextLocation;
	}
}
