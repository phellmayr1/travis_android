package fhhgb.travis.philipp.travis_android;

/**
 * @author Philipp
 *         Created on 22.10.2015.
 */

public class Constants {

	public static String[] CURRENCIES = {"EUR", "USD", "GBP", "AUD", "CAD", "CHF", "CNY", "RUB"};

	public static String[] TIME_ZONES = new String[]{"Adelaide, UTC+09:30", "Almaty, UTC+06:00", "Amman, UTC+02:00", "Amsterdam, " +
			"UTC+01:00",
			"Anchorage, UTC-09:00", "Athens, UTC+02:00", "Auckland, UTC+12:00", "Azores, UTC-01:00", "Baghdad, UTC+03:00", "Baku, " +
			"UTC+04:00", "Bangkok, UTC+07:00", "Barbados, UTC-04:00", "Beirut, UTC+02:00", "Belgrade, UTC+01:00", "Bogota, UTC-05:00",
			"Brazzaville, UTC+01:00", "Brisbane, UTC+10:00", "Brussels, UTC+01:00", "Buenos Aires, UTC-03:00", "Cairo, UTC+02:00",
			"Calcutta, UTC+05:30", "Cape Verde, UTC-01:00", "Caracas, UTC-04:30", "Casablanca, UTC+00:00", "Chicago, UTC-06:00",
			"Chihuahua, UTC-07:00", "Colombo, UTC+05:30", "Costa Rica, UTC-06:00", "Darwin, UTC+09:30", "Denver, UTC-07:00", "Dubai, " +
			"UTC+04:00", "Fiji, UTC+12:00", "Godthab, UTC-03:00", "Guam, UTC+10:00", "Halifax, UTC-04:00", "Harare, UTC+02:00",
			"Helsinki," +
					" UTC+02:00", "Hobart, UTC+10:00", "Hong Kong, UTC+08:00", "Honolulu, UTC-10:00", "Irkutsk, UTC+08:00", "Jerusalem, " +
			"UTC+02:00", "Kabul, UTC+04:30", "Karachi, UTC+05:00", "Katmandu, UTC+05:45", "Krasnoyarsk, UTC+07:00", "Kuala Lumpur, " +
			"UTC+08:00", "Kuwait, UTC+03:00", "London, UTC+00:00", "Los Angeles, UTC-08:00", "Magadan, UTC+10:00", "Majuro, UTC+12:00",
			"Manaus, UTC-04:00", "Mexico City, UTC-06:00", "Midway, UTC-11:00", "Minsk, UTC+03:00", "Montevideo, UTC-03:00", "Moscow, " +
			"UTC+03:00", "Nairobi, UTC+03:00", "New York, UTC-05:00", "Oral, UTC+05:00", "Perth, UTC+08:00", "Phoenix, UTC-07:00",
			"Rangoon, UTC+06:30", "Regina, UTC-06:00", "Santiago, UTC-03:00", "Sao Paulo, UTC-03:00", "Sarajevo, UTC+01:00", "Seoul, " +
			"UTC+09:00", "Shanghai, UTC+08:00", "South Georgia, UTC-02:00", "St Johns, UTC-03:30", "Sydney, UTC+10:00", "Taipei, " +
			"UTC+08:00", "Tbilisi, UTC+04:00", "Tehran, UTC+03:30", "Tijuana, UTC-08:00", "Tokyo, UTC+09:00", "Tongatapu, UTC+13:00",
			"Vladivostok, UTC+10:00", "Windhoek, UTC+01:00", "Yakutsk, UTC+09:00", "Yekaterinburg, UTC+05:00", "Yerevan, UTC+04:00"};

	/*
	*Keys for shared Preferences
	 */
	public static final String PREFERENCE_SERVER = "server";
	public static final String PREFERENCE_PUSH_DIARY_CREATED = "pushdiarycreated";
	public static final String PREFERENCE_PUSH_TRIP_INVITED = "pushtripinvited";

	/**
	*Every rating category belongs to a rating group
	 */
	public static final String RATING_GROUP_LOCATION = "LOCATION";

	/**
	*Rating categories server names
	 */
	public static final String RATING_CATEGORY_FOOD = "LOCATION_FOOD";
	public static final String RATING_CATEGORY_PARTY = "LOCATION_NIGHTLIFE";
	public static final String RATING_CATEGORY_ATTRACTIONS = "LOCATION_ATTRACTIONS";
	public static final String RATING_CATEGORY_ACCOMODATION = "LOCATION_ACCOMODATION";
	public static final String RATING_CATEGORY_SHOPPING = "LOCATION_SHOPPING";
	public static final String RATING_CATEGORY_ACTIVITIES = "LOCATION_ACTIVITIES";
	public static final String RATING_CATEGORY_ADVENTURE = "LOCATION_ADVENTURE";
	public static final String RATING_CATEGORY_SAFETY = "LOCATION_SAFETY";
	public static final String RATING_CATEGORY_WELLNES = "LOCATION_WELLNES";
	public static final String RATING_CATEGORY_ALL = "ALL";

	/**
	* Facebook Profile image sizes
	 */
	public static final int FACEBOOK_IMAGE_WIDTH = 300;
	public static final int FACEBOOK_IMAGE_HEIGHT = 300;
	public static final int FACEBOOK_IMAGE_WIDTH_DIALOG = 100;
	public static final int FACEBOOK_IMAGE_HEIGHT_DIALOG = 100;

	/**
	* Constants for the ViewpagerIndicators
	 */
	public static final int INFO_NUM_PAGES = 2;
	public static final int PROFILE_NUM_PAGES = 3;

	/**
	* This fields are used for the intents
	 */
	public static final String USER_ID_TO_SHOW = "userIdToShow";
	public static final String SHOW_MY_USER = "showmyuser";
	public static final String LOCATION_NAME = "locationname";
	public static final String LOCATION_ID = "locaitonid";
	public static final String DIARY_ELEMENTS = "diaryelements";
	public static final String CURRENT_PROFILE_USERNAME = "username";
	public static final String DIARY_TITLE = "diaryname";
	public static final String DIARY_ID = "diaryid";
	public static final String FOUND_LOCATION = "foundlocation";
	public static final String GOOGLE_PLACE_ID = "googleplaceid";
	public static final String CATEGORY_RATINGS = "categoryRatings";
	public static final String TRIPACTIVITY = "tripactivity";
	public static final String TRIP_ID = "tripid";
	public static final String IS_ADMIN = "isAdmin";

	/**
	*Logintypes, they define how the user wants to log in
	 */
	public static final String LOGIN_TYPE_FACEBOOK = "facebook";

	public static final int MAX_DIARY_ENTRIES = 20;

	/**
	 * Constants for Show Trips View. This Constants are used for Filtering
	 */
	public static final String FILTER_ADMIN = "admin";
	public static final String FILTER_INVITED = "invited";
	public static final String FILTER_OVER = "over";

	public static final String USER_TRIP_ACCEPTED = "A";
	public static final String USER_TRIP_REJECTED = "R";
	public static final String USER_TRIP_INVITED = "I";

}
