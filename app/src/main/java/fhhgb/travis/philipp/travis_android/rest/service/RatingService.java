package fhhgb.travis.philipp.travis_android.rest.service;

import java.util.List;

import fhhgb.travis.philipp.travis_android.rest.model.request.Rating;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Hellmayr on 30.04.2016.
 */
public interface RatingService {

	@Headers("Content-Type: application/json")
	@GET("rating/ratingCategories/{categoryGroup}")
	Call<List<String>> findRatingCategories(@Path("categoryGroup") String categoryGroup);

	@Headers("Content-Type: application/json")
	@POST("rating/location/{locationId}")
	Call<Void> saveRatings(@Path("locationId") int locationId, @Body List<Rating> rating);

	@Headers("Content-Type: application/json")
	@GET("rating/location/{locationId}")
	Call<List<fhhgb.travis.philipp.travis_android.rest.model.response.Rating>> findRatingsForLocation(@Path("locationId") int locationId);

	@Headers("Content-Type: application/json")
	@GET("rating/location/{locationId}")
	Call<List<fhhgb.travis.philipp.travis_android.rest.model.response.Rating>> findRatingsForLocationAndUserAndCategory(@Path("locationId")
	                                                                                                                    int
			                                                                                                                    locationId, @Query
			                                                                                                                    ("userId")
	int userId, @Query
			                                                                                                                    ("category") String category);
}
