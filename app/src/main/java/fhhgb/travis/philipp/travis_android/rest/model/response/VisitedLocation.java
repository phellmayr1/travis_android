package fhhgb.travis.philipp.travis_android.rest.model.response;

/**
 * Created by Hellmayr on 28.01.2016.
 */
public class VisitedLocation {

	private VisitedLocationElement location;

	public VisitedLocationElement getLocation() {
		return location;
	}

	public void setLocation(VisitedLocationElement location) {
		this.location = location;
	}
}
