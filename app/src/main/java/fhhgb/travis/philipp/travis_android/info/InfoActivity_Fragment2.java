package fhhgb.travis.philipp.travis_android.info;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fhhgb.travis.philipp.travis_android.R;


/**
 * @author Philipp
 *         Created by Philipp on 22.05.2015.
 */

public class InfoActivity_Fragment2 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(
                R.layout.activity_info_fragment2, container, false);
    }

}
