package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.api.GoogleApiClient;

import java.util.List;

import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.async.AttributedPhoto;
import fhhgb.travis.philipp.travis_android.async.LoadGooglePlacesPhotoTask;
import fhhgb.travis.philipp.travis_android.rest.model.response.FoundLocation;

/**
 * Created by Hellmayr on 30.05.2016.
 */
public class TripResultsAdapter extends RecyclerView.Adapter<TripResultViewHolder>{
//		implements OnMapReadyCallback {

	private GoogleApiClient mGoogleApiClient;

	private List<FoundLocation> foundTrips;
	private Activity currentActivity;

	public TripResultsAdapter(List<FoundLocation> mainPageElements, Activity currentActivity, GoogleApiClient mGoogleApiClient) {
		this.foundTrips = mainPageElements;
		this.currentActivity = currentActivity;
		this.mGoogleApiClient = mGoogleApiClient;
	}

	@Override
	public TripResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.
				from(parent.getContext()).
				inflate(R.layout.cardview_found_location, parent, false);
		return new TripResultViewHolder(itemView, currentActivity);
	}

	@Override
	public void onBindViewHolder(final TripResultViewHolder holder, int position) {
		FoundLocation currentElement = foundTrips.get(position);

		holder.setLocationId(currentElement.getLocation().getId());
		holder.setLocationName(currentElement.getLocation().getName());
		holder.nameTextView.setText(currentElement.getLocation().getName());

		Display display = currentActivity.getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;

		// Create a new AsyncTask that displays the bitmap and attribution once loaded.
		new LoadGooglePlacesPhotoTask(width, width, mGoogleApiClient) {
			@Override
			protected void onPreExecute() {
				// Display a temporary image to show while bitmap is loading.
				//mImageView.setImageResource(R.drawable.profile_background);
			}

			@Override
			protected void onPostExecute(AttributedPhoto attributedPhoto) {
				if (attributedPhoto != null) {
					// Photo has been loaded, display it.
					holder.cityImageView.setImageBitmap(attributedPhoto.bitmap);
				}
			}
		}.execute(currentElement.getLocation().getGooglePlaceId());


//		holder.mapView.onCreate(null);
//		holder.mapView.getMapAsync(this);
//
//		holder.mapView.getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(45, 15), 13));

	}

	@Override
	public int getItemCount() {
		return foundTrips.size();
	}


//	@Override
//	public void onMapReady(GoogleMap map) {
//		map.getUiSettings().setScrollGesturesEnabled(false);
//
//		map.getUiSettings().setMapToolbarEnabled(false);
//	}

}
