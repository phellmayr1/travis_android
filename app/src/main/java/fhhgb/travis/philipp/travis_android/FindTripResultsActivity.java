package fhhgb.travis.philipp.travis_android;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.rest.model.response.FoundLocation;
import fhhgb.travis.philipp.travis_android.rest.service.TripPlannerService;
import fhhgb.travis.philipp.travis_android.views.TripResultsAdapter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class FindTripResultsActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener {

	@Bind(R.id.resultsRecyclerView)
	RecyclerView resultsRecyclerView;

	TripResultsAdapter tripResultsAdapter;

	GoogleApiClient mGoogleApiClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_find_trip_results);
		ButterKnife.bind(this);

		//The extra contains objects, each object is a category and a boolean which defines if this category
		// is important for the user (according to the swipe)
		Bundle extra = getIntent().getBundleExtra(Constants.CATEGORY_RATINGS);
		ArrayList<Object> objects = (ArrayList<Object>) extra.getSerializable("objects");

		setupToolbar();

		//This is needed for the Google Places API to show city images
		mGoogleApiClient = new GoogleApiClient
				.Builder(this)
				.addApi(Places.GEO_DATA_API)
				.addApi(Places.PLACE_DETECTION_API)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.build();

		//Some Layout Settings for the RecyclerView
		resultsRecyclerView.setHasFixedSize(true);
		final LinearLayoutManager llm = new LinearLayoutManager(this);
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		resultsRecyclerView.setLayoutManager(llm);

		loadResults(objects);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
		}

		return super.onOptionsItemSelected(item);
	}

	private void setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	}

	/**
	 *Send the result of the user's swipe through the categories to the server and show the results, that are returned from the
	 * server, in a recyclerview. The results from the server are cities, that match the user's preferences the most.
	 * @param objects a List of objects, where each object consist of a category and the user's decision if
	 *                this category is important for him
	 */
	private void loadResults(ArrayList<Object> objects) {
		final TripPlannerService tripPlannerService = SessionData.getRestClient().getTripPlannerService();

		Call<List<FoundLocation>> retval = tripPlannerService.findTrip(objects);

		retval.enqueue(new Callback<List<FoundLocation>>() {

			@Override
			public void onResponse(Response<List<FoundLocation>> response, Retrofit retrofit) {

				tripResultsAdapter = new TripResultsAdapter(response.body(), FindTripResultsActivity.this, mGoogleApiClient);
				resultsRecyclerView.setAdapter(tripResultsAdapter);

			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	}

	@Override
	public void onStop() {
		mGoogleApiClient.disconnect();
		super.onStop();
	}


	@Override
	public void onConnected(@Nullable Bundle bundle) {

	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

	}
}
