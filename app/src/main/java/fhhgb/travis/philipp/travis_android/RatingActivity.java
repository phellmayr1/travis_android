package fhhgb.travis.philipp.travis_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.fragments.RatingFragment;
import fhhgb.travis.philipp.travis_android.rest.service.RatingService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Activity which contains a fragment for each rating category
 */
public class RatingActivity extends AppCompatActivity {

	@Bind(R.id.ratingView)
	CoordinatorLayout ratingFrame;

	List<String> categories;

	//List of all fragments, each one representing a rating category
	LinkedList<RatingFragment> ratingFragments;

	private int currentCategory = -1;

	private int locationId;

	private void init() {
		categories = new LinkedList<>();
		ratingFragments = new LinkedList<>();
	}

	private void loadExtras() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			locationId = extras.getInt(Constants.LOCATION_ID, -1);
		}
	}

	@Override
	public void onBackPressed() {

		finish();
		Intent intent = new Intent(this, LocationDetailsActivity.class);
		intent.putExtra(Constants.LOCATION_ID, locationId);
		startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rating);
		ButterKnife.bind(this);

		init();

		loadExtras();

		final RatingService ratingService = SessionData.getRestClient().getRatingService();

		//Load all the rating categories available for locations
		Call<List<String>> retval = ratingService.findRatingCategories(Constants.RATING_GROUP_LOCATION);

		retval.enqueue(new Callback<List<String>>() {

			@Override
			public void onResponse(Response<List<String>> response, Retrofit retrofit) {
				currentCategory = -1;
				ratingFragments = new LinkedList<>();
				boolean showFirst = false;

				if (response.body() != null) {
					categories.addAll(response.body());

					//Iterate over all the categories
					for (String categoryName : response.body()) {

						//Create a fragment for each category
						RatingFragment ratingFragment = new RatingFragment();

						//Tell the fragment, what it has to show
						Bundle bundle = new Bundle();
						bundle.putString("category", categoryName);
						bundle.putInt(Constants.LOCATION_ID, locationId);

						ratingFragment.setArguments(bundle);
						ratingFragments.add(ratingFragment);

						//If it's the first fragment, then show it
						if (!showFirst) {
							getSupportFragmentManager().beginTransaction()
									.add(R.id.ratingView, ratingFragment).commit();
							showFirst = true;
							currentCategory++;
						}
					}

				}
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});

		//Depending on the SwipeDirection, show the next or the previous rating category
		ratingFrame.setOnTouchListener(new OnSwipeTouchListener(this) {
			@Override
			public void onSwipeLeft() {

				if (currentCategory != ratingFragments.size() - 1) {
					currentCategory++;
					getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right,
							R.anim.exit_to_left)
							.replace(R.id.ratingView, ratingFragments.get(currentCategory)).commit();
				}
			}

			@Override
			public void onSwipeRight() {
				super.onSwipeRight();

				if (currentCategory != 0) {
					currentCategory--;
					getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_left,
							R.anim.exit_to_right)
							.replace(R.id.ratingView, ratingFragments.get(currentCategory)).commit();

				}
			}
		});
	}
}
