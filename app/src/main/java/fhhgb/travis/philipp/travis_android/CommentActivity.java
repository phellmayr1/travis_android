package fhhgb.travis.philipp.travis_android;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.rest.model.request.CommentText;
import fhhgb.travis.philipp.travis_android.rest.model.response.UserComment;
import fhhgb.travis.philipp.travis_android.rest.service.DiaryService;
import fhhgb.travis.philipp.travis_android.views.CommentAdapter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class CommentActivity extends AppCompatActivity {

    int currentPage = 0;
    int pageSize = 5;

    @Bind(R.id.commentsListView)
    ListView commentListView;

    @Bind(R.id.textInput)
    EditText commentText;

    @Bind(R.id.btnSend)
    Button commentButton;

    int diaryID = 0;

    LinkedList<UserComment> userComments;

    CommentAdapter adapter = null;

    DiaryService mDiaryService = null;

    private boolean loading = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        if(intent.getIntExtra(Constants.DIARY_ID,-1) != -1) {
            diaryID = intent.getIntExtra(Constants.DIARY_ID,-1);
        }

        mDiaryService = SessionData.getRestClient().getDiaryService();

        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!commentText.getText().toString().isEmpty() && diaryID != -1){
                    Call<Void> retval = mDiaryService.addCommentToDiary(diaryID, new CommentText(commentText.getText().toString()));
                    retval.enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Response<Void> response, Retrofit retrofit) {
                            commentText.setText("");
                            getComment();
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        init();
    }

    /**
     * Initialize all needed classes for the activity
     */
    private void init(){
        userComments = new LinkedList<>();
        adapter = new CommentAdapter(CommentActivity.this,R.layout.listitem_comment,userComments);
        commentListView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_NORMAL);
        commentListView.setAdapter(adapter);

        commentListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int currentFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
            }

            /**
             * Checks if list is on the top and fetch new data from the server
             */
            public void isScrollCompleted(){
                if(loading){
                    if(currentFirstVisibleItem == 0){
                        loading = false;
                        getComments();
                    }
                }
            }
        });
        getComments();
    }

    /**
     * Get comments from the server and notify the adapter, that the data has changed
     */
    private void getComments(){

        Call<List<UserComment>> retval = mDiaryService.findCommentsForDiaryEntry(diaryID,currentPage,pageSize,150,150);
        retval.enqueue(new Callback<List<UserComment>>() {
            @Override
            public void onResponse(Response<List<UserComment>> response, Retrofit retrofit) {

                if(response.body() != null){
                    for(int i = response.body().size()-1; i>=0 ; i--){
                        userComments.addFirst(response.body().get(i));
                    }
                    adapter.notifyDataSetChanged();
                    currentPage++;
                    loading = true;
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getBaseContext(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Gets the newest comment from the server and add it at the last position to the list
     */
    private void getComment(){

        Call<List<UserComment>> retval = mDiaryService.findCommentsForDiaryEntry(diaryID,0,1,150,150);
        retval.enqueue(new Callback<List<UserComment>>() {
            @Override
            public void onResponse(Response<List<UserComment>> response, Retrofit retrofit) {

                if(response.body() != null && response.body().size() != 0){
                    userComments.addLast(response.body().get(0));
                    adapter.notifyDataSetChanged();
                    loading = true;
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getBaseContext(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}

