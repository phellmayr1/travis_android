package fhhgb.travis.philipp.travis_android.async;

/**
 * Created by Hellmayr on 24.03.2016.
 */

import android.graphics.Bitmap;

/**
 * Holder for an image and its attribution.
 */
public class AttributedPhoto {

	public final CharSequence attribution;

	public final Bitmap bitmap;

	public AttributedPhoto(CharSequence attribution, Bitmap bitmap) {
		this.attribution = attribution;
		this.bitmap = bitmap;
	}
}