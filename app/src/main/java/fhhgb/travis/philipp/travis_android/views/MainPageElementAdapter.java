package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.SessionData;
import fhhgb.travis.philipp.travis_android.helper.DateUtils;
import fhhgb.travis.philipp.travis_android.helper.UrlBuilder;
import fhhgb.travis.philipp.travis_android.rest.RestConstants;
import fhhgb.travis.philipp.travis_android.rest.model.response.DiaryCoordinatesElementDetails;
import fhhgb.travis.philipp.travis_android.rest.model.response.MainPageElement;
import fhhgb.travis.philipp.travis_android.rest.model.response.UserComment;
import fhhgb.travis.philipp.travis_android.rest.service.DiaryService;
import fhhgb.travis.philipp.travis_android.rest.service.LocationService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * @author Philipp
 *         Created on 26.01.2016.
 */

public class MainPageElementAdapter extends RecyclerView.Adapter<MainPageElementViewHolder> {

	private List<MainPageElement> mainPageElements;

	private Activity currentActivity;

	public static final int VIEW_TYPE_IMAGE = 0;
	public static final int VIEW_TYPE_COORDINATES = 1;
	public static final int VIEW_TYPE_TEXT = 2;

	@Override
	public int getItemViewType(int position) {
		MainPageElement currentElement = mainPageElements.get(position);

		if (currentElement.getDiaryElement().getDiaryImageElement() != null) {
			return VIEW_TYPE_IMAGE;
		} else if (currentElement.getDiaryElement().getCoordinatesElement() != null) {
			return VIEW_TYPE_COORDINATES;
		} else {
			return VIEW_TYPE_TEXT;
		}
	}

	public MainPageElementAdapter(List<MainPageElement> mainPageElements, Activity currentActivity) {
		this.mainPageElements = mainPageElements;
		this.currentActivity = currentActivity;
	}

	@Override
	public MainPageElementViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

		if (viewType == VIEW_TYPE_IMAGE || viewType == VIEW_TYPE_COORDINATES) {
			View itemView = LayoutInflater.
					from(parent.getContext()).
					inflate(R.layout.cardview_mainpage_diaryimage, parent, false);
			return new MainPageImageElementViewHolder(itemView, currentActivity);

		} else {
			View itemView = LayoutInflater.
					from(parent.getContext()).
					inflate(R.layout.cardview_mainpage_diarytext, parent, false);
			return new MainPageElementViewHolder(itemView, currentActivity);
		}
	}

	@Override
	public void onBindViewHolder(final MainPageElementViewHolder holder, int position) {

		MainPageElement currentElement = mainPageElements.get(position);

		holder.nameTextView.setText(currentElement.getUser().getFirstName() + " " + currentElement.getUser().getLastName());
		holder.timeTextView.setText(DateUtils.formatDate(currentElement.getDiaryElement().getCreatedTime()));
		holder.diaryLocation.setText(currentElement.getDiaryElement().getLocation().getName());
		holder.diaryName.setText(currentElement.getDiaryElement().getName());
		holder.setDiaryId(currentElement.getDiaryElement().getId());
		holder.setIsLiked(currentElement.getDiaryElement().isLiked());

		if (currentElement.getDiaryElement().isLiked()) {
			holder.likeIcon.setTextColor(currentActivity.getResources().getColor(R.color.travis_primary));
		}

		//TODO: Backend Support not yet implemented
		holder.setIsLiked(currentElement.getDiaryElement().isLiked());

		holder.setUsername(currentElement.getUser().getFirstName() + " " + currentElement.getUser().getLastName());
		holder.setDiaryTitle(currentElement.getDiaryElement().getName());
		holder.setUserId(currentElement.getUser().getId());
		//holder.setUserId(currentElement.getUserId());
		holder.commentCount.setText(Integer.toString(currentElement.getDiaryElement().getCommentCount()));

		ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

		imageLoader.loadImage(currentElement.getUser().getImageUrl(), new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				holder.profileImage.setImageBitmap(loadedImage);
			}
		});

		if (holder instanceof MainPageImageElementViewHolder) {

			if (currentElement.getDiaryElement().getDiaryImageElement() != null) {
				imageLoader.loadImage(RestConstants.BASE_URL + currentElement.getDiaryElement().getDiaryImageElement().getImageUrl(), new
						SimpleImageLoadingListener() {
							@Override
							public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
								((MainPageImageElementViewHolder) holder).diaryImage.setImageBitmap(loadedImage);
							}
						});
			} else if (currentElement.getDiaryElement().getCoordinatesElement() != null) {

				LocationService locationService = SessionData.getRestClient().getLocationService();

				imageLoader = ImageLoader.getInstance(); // Get singleton instance

				imageLoader.loadImage(UrlBuilder.buildMapImageUrl(currentElement.getDiaryElement().getCoordinatesElement()
								.getCoordinates().getLongitude(),
						currentElement.getDiaryElement().getCoordinatesElement().getCoordinates().getLatitude()), new
						SimpleImageLoadingListener
								() {
							@Override
							public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
								((MainPageImageElementViewHolder) holder).diaryImage.setImageBitmap(loadedImage);
							}
						});
			}

		}
	}

//		System.out.println("hallo");
//		System.out.println(currentElement.getDiaryElement().getId());
//		DiaryService diaryService = SessionData.getRestClient().getDiaryService();
//		Call<List<UserComment>> retval = diaryService.findCommentsForDiaryEntry(currentElement.getDiaryElement().getId(),10,150,150);
//		retval.enqueue(new Callback<List<UserComment>>() {
//			@Override
//			public void onResponse(Response<List<UserComment>> response, Retrofit retrofit) {
//
//				if(response.body() != null){
//					UserComment comment = response.body().get(response.body().size()-1);
//					holder.commentProfileName.setText(comment.getUser().getFirstName()+" "+comment.getUser().getLastName());
//					holder.commentText.setText(comment.getText());
//					//holder.commentCount.setText(response.body().size());
//
//					ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance
//
//					final Bitmap[] bitmap = new Bitmap[1];
//
//					String url = comment.getUser().getImageUrl();
//					url = url.replace("width=100", "width=150");
//					url = url.replace("height=100", "height=150");
//
//
//					imageLoader.loadImage(url, new SimpleImageLoadingListener() {
//						@Override
//						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//							holder.commentProfileImage.setImageBitmap(loadedImage);
//							notifyDataSetChanged();
//						}
//					});
//				}
//			}
//
//			@Override
//			public void onFailure(Throwable t) {
//
//			}
//		});

	@Override
	public int getItemCount() {
		return mainPageElements.size();
	}
}
