package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.LocationDetailsActivity;
import fhhgb.travis.philipp.travis_android.NewTripActivity;
import fhhgb.travis.philipp.travis_android.R;

/**
 * Created by Hellmayr on 30.05.2016.
 */
public class TripResultViewHolder extends RecyclerView.ViewHolder {

	TextView nameTextView;
//	MapView mapView;

	ImageView cityImageView;
	TextView planATripTextView;
	FrameLayout content;

	int locationId;
	String locationName;

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public TripResultViewHolder(View itemView, final Activity currentActivity) {
		super(itemView);

		nameTextView = (TextView) itemView.findViewById(R.id.nameTextView);

		cityImageView = (ImageView) itemView.findViewById(R.id.city_image);
		planATripTextView = (TextView) itemView.findViewById(R.id.planATrip);
		content = (FrameLayout) itemView.findViewById(R.id.content);

		planATripTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(currentActivity, NewTripActivity.class);
				intent.putExtra(Constants.LOCATION_ID, locationId);
				intent.putExtra(Constants.LOCATION_NAME, locationName);
				currentActivity.startActivity(intent);
			}
		});

		content.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(currentActivity, LocationDetailsActivity.class);
				intent.putExtra(Constants.LOCATION_ID, locationId);
				currentActivity.startActivity(intent);
			}
		});

	}

}
