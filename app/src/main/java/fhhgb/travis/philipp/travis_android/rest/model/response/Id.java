package fhhgb.travis.philipp.travis_android.rest.model.response;

/**
 * @author Philipp
 *         Created on 23.12.2015.
 */

public class Id {

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
