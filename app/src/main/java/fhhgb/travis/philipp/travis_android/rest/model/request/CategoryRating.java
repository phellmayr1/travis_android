package fhhgb.travis.philipp.travis_android.rest.model.request;

import java.io.Serializable;

/**
 * Created by Hellmayr on 28.05.2016.
 */
public class CategoryRating implements Serializable {

	private String ratingCategory;
	private boolean like;

	public CategoryRating(String ratingCategory, boolean like) {
		this.ratingCategory = ratingCategory;
		this.like = like;
	}

	public String getRatingCategory() {
		return ratingCategory;
	}

	public void setRatingCategory(String ratingCategory) {
		this.ratingCategory = ratingCategory;
	}

	public boolean isLike() {
		return like;
	}

	public void setLike(boolean like) {
		this.like = like;
	}
}
