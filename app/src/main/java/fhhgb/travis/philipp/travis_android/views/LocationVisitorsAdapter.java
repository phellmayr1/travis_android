package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;

/**
 * Created by Hellmayr on 10.05.2016.
 */
public class LocationVisitorsAdapter extends RecyclerView.Adapter<LocationUserViewHolder> {

	private List<User> users;

	private Activity currentActivity;

	public LocationVisitorsAdapter(List<User> users, Activity currentActivity) {
		this.users = users;
		this.currentActivity = currentActivity;
	}

	@Override
	public LocationUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.
				from(parent.getContext()).
				inflate(R.layout.cardview_location_visitors, parent, false);
		return new LocationUserViewHolder(itemView, currentActivity);
	}

	@Override
	public void onBindViewHolder(final LocationUserViewHolder holder, int position) {
		User user = users.get(position);

		holder.setUser(user);

		holder.profileTextView.setText(user.getFirstName() + " " + user.getLastName());
		ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

		imageLoader.loadImage(user.getImageUrl(), new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				holder.profileImageView.setImageBitmap(loadedImage);
			}
		});

	}

	@Override
	public int getItemCount() {
		return users.size();
	}
}
