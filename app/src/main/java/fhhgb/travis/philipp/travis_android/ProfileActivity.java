package fhhgb.travis.philipp.travis_android;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;
import fhhgb.travis.philipp.travis_android.profile.DiaryFragment;
import fhhgb.travis.philipp.travis_android.profile.PlacesFragment;
import fhhgb.travis.philipp.travis_android.profile.ProfileFragment;
import fhhgb.travis.philipp.travis_android.rest.model.response.Id;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;
import fhhgb.travis.philipp.travis_android.rest.service.UserService;
import fhhgb.travis.philipp.travis_android.views.BlurBuilder;
import fhhgb.travis.philipp.travis_android.views.RoundedImageView;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ProfileActivity extends AppCompatActivity {

	//	private Toolbar toolbar;
	private TabLayout tabLayout;
	private ViewPager viewPager;

	@Bind(R.id.collapse_toolbar)
	CollapsingToolbarLayout collapseToolbar;

	@Bind(R.id.username)
	TextView username;

	@Bind(R.id.toolbar)
	Toolbar toolbar;

	@Bind(R.id.follow_action_button)
	FloatingActionButton floatingActionButton;

	@Bind(R.id.profileimage)
	RoundedImageView profileImage;

	@Bind(R.id.currentlocation)
	TextView currentLocation;

	@Bind(R.id.MyAppbar)
	AppBarLayout appBarLayout;

	int userToShowId = -1;
	boolean showCurrentUser;

	User userToShow;

	AlertDialog dialog;

	ProfileFragment profileFragment;
	DiaryFragment diaryFragment;
	PlacesFragment placesFragment;

	//This is used for the visibility of the follow button when scrolling
	AppBarLayout.OnOffsetChangedListener scrollListener;

	int currentLocationRequestCode = 1003;

	public User getUserToShow() {
		return userToShow;
	}

	public void setUserToShow(User userToShow) {
		this.userToShow = userToShow;
	}

	private void loadUser() {

		if (SessionData.getCurrentUser() == null || showCurrentUser) {

			UserService userService = SessionData.getRestClient().getUserService();

			Call<User> retval = userService.getMyProfile();
			retval.enqueue(new Callback<User>() {

				@Override
				public void onResponse(Response<User> response, Retrofit retrofit) {

					if (ServerError.hasNoServerError(response, ProfileActivity.this)) {

						SessionData.setCurrentUser(response.body());

						if (showCurrentUser) {
							userToShow = response.body();
							userToShowId = userToShow.getId();
							profileFragment.initInputs();
							placesFragment.setUserToShow(userToShow);
							diaryFragment.startFromActivity();
							setupInputs();
						} else {
							loadProfileForId();
						}
						handleFollowButtonVisivility();
					}
					dialog.dismiss();
				}

				@Override
				public void onFailure(Throwable t) {
					Toast.makeText(getBaseContext(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
				}
			});

		} else {
			loadProfileForId();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
		}

		return super.onOptionsItemSelected(item);
	}

	private void addFollowUserButton() {

		if (userToShow.isFollow()) {
			setMinusFollowButton();
		}
		floatingActionButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				UserService userService = SessionData.getRestClient().getUserService();

				if (!userToShow.isFollow()) {

					Call<Void> retval = userService.followUser(userToShow.getId());

					retval.enqueue(new Callback<Void>() {

						@Override
						public void onResponse(Response<Void> response, Retrofit retrofit) {
							//Toast.makeText(getContext(), "RESP " + response.body().getId(), Toast.LENGTH_LONG).show();
							Snackbar.make(view, getString(R.string.you_follow_this_user), Snackbar.LENGTH_LONG)
									.setAction("Action", null).show();

							if(profileFragment!=null) {
								profileFragment.incrementFollowerText();
							}

							setMinusFollowButton();
							userToShow.setFollow(true);
						}

						@Override
						public void onFailure(Throwable t) {
							Toast.makeText(getBaseContext(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
						}
					});
				} else {
					Call<Void> retval = userService.unFollowUser(userToShow.getId());

					retval.enqueue(new Callback<Void>() {

						@Override
						public void onResponse(Response<Void> response, Retrofit retrofit) {
							//Toast.makeText(getContext(), "RESP " + response.body().getId(), Toast.LENGTH_LONG).show();
							Snackbar.make(view, getString(R.string.you_stopped_following_this_user), Snackbar.LENGTH_LONG)
									.setAction("Action", null).show();

							if(profileFragment!=null) {
								profileFragment.decrementFollowerText();
							}

							setPlusFollowButton();
							userToShow.setFollow(false);
						}

						@Override
						public void onFailure(Throwable t) {
							Toast.makeText(getBaseContext(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
						}
					});
				}
			}

		});
	}

	private void setPlusFollowButton() {
		Bitmap icon = BitmapFactory.decodeResource(getResources(),
				android.R.drawable.ic_input_add);
		floatingActionButton.setImageBitmap(icon);
	}

	private void setMinusFollowButton() {
		Bitmap icon = BitmapFactory.decodeResource(getResources(),
				R.drawable.minus);
		floatingActionButton.setImageBitmap(icon);
	}

	private void loadProfileForId() {
		UserService userService = SessionData.getRestClient().getUserService();

		Call<User> retval = userService.getProfile(userToShowId + "", Constants.FACEBOOK_IMAGE_WIDTH, Constants.FACEBOOK_IMAGE_HEIGHT);
		retval.enqueue(new Callback<User>() {

			@Override
			public void onResponse(Response<User> response, Retrofit retrofit) {

				if (ServerError.hasNoServerError(response, ProfileActivity.this)) {

					userToShow = response.body();

					addFollowUserButton();
					profileFragment.initInputs();
					placesFragment.setUserToShow(userToShow);
					diaryFragment.startFromActivity();
					setupInputs();
					//Toast.makeText(getBaseContext(), "RESP " + response.body().getLastName(), Toast.LENGTH_LONG).show();
				}
				dialog.dismiss();
			}

			@Override
			public void onFailure(Throwable t) {
				ServerError.serverFail(ProfileActivity.this, t);
			}
		});
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		ButterKnife.bind(this);

		dialog = new SpotsDialog(this, getString(R.string.loading));
		dialog.show();

		viewPager = (ViewPager) findViewById(R.id.viewpager);
		setupViewPager(viewPager);

		setupToolbar();
		loadExtras();
		blurBackground();

		loadUser();

		scrollListener = new AppBarLayout.OnOffsetChangedListener() {
			@Override
			public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

				if (verticalOffset < -365) {
					floatingActionButton.setVisibility(View.INVISIBLE);
				} else {
					floatingActionButton.setVisibility(View.VISIBLE);
				}
			}
		};

		handleFollowButtonVisivility();

		tabLayout = (TabLayout) findViewById(R.id.tabs);
		tabLayout.setupWithViewPager(viewPager);
	}

	private void handleFollowButtonVisivility() {

		if (SessionData.getCurrentUser() != null) {
			if (userToShowId == SessionData.getCurrentUser().getId()) {
				floatingActionButton.setVisibility(View.INVISIBLE);
				appBarLayout.removeOnOffsetChangedListener(scrollListener);
			} else {
				floatingActionButton.setVisibility(View.VISIBLE);
				appBarLayout.addOnOffsetChangedListener(scrollListener);
			}
		}
	}

	private void setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
//		getSupportActionBar().setTitle("");
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	}

	private void setupInputs() {

		if (SessionData.getCurrentUser().getId() != userToShow.getId()) {
			currentLocation.setKeyListener(null);

			ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

			imageLoader.loadImage(userToShow.getImageUrl(), new SimpleImageLoadingListener() {
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					profileImage.setImageBitmap(loadedImage);
				}
			});

		} else {

			currentLocation.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

					startActivityForResult(new Intent("fhhgb.travis.philipp.travis_android.SearchLocationActivity"),
							currentLocationRequestCode);
				}
			});
			profileImage.setImageBitmap(SessionData.getFbProfilePic());
		}

		username.setText(userToShow.getFirstName() + " " + userToShow.getLastName());

		if (userToShow.getCurrentLocation() != null) {
			currentLocation.setText(userToShow.getCurrentLocation().getName());
		} else if (showCurrentUser) {
			currentLocation.setText(getString(R.string.click_to_select));
		}

	}

	@Override
	public void onBackPressed() {
		currentLocation.clearFocus();
		finish();
		super.onBackPressed();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			if (requestCode == currentLocationRequestCode) {
				Bundle resultBundle = data.getExtras();
				currentLocation.setText(resultBundle.getString(Constants.LOCATION_NAME));
				assignCurrentLocationToUser(resultBundle.getInt(Constants.LOCATION_ID));
			} else {
				profileFragment.onActivityResult(requestCode, resultCode, data);
				placesFragment.onActivityResult(requestCode, resultCode, data);
			}
		}
	}

	private void assignCurrentLocationToUser(int locationId) {

		UserService userService = SessionData.getRestClient().getUserService();

		Call<Id> retval = userService.changeCurrentLocation(locationId);

		retval.enqueue(new Callback<Id>() {

			@Override
			public void onResponse(Response<Id> response, Retrofit retrofit) {
//				Toast.makeText(getContext(), "RESP " + response.body().getId(), Toast.LENGTH_LONG).show();

			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	private void loadExtras() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			userToShowId = extras.getInt(Constants.USER_ID_TO_SHOW, -1);
			showCurrentUser = extras.getBoolean(Constants.SHOW_MY_USER, false);
		}
	}

	private void blurBackground() {

		Bitmap image = SessionData.getBlurredStandardBackgroundImage();
		if (image == null) {
			Bitmap img = BitmapFactory.decodeResource(getResources(),
					R.drawable.profile_background);
			View content = findViewById(android.R.id.content).getRootView();
			image = BlurBuilder.blur(content, img);
			SessionData.setBlurredStandardBackgroundImage(image);
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			collapseToolbar.setBackground(new BitmapDrawable(getResources(), image));
		} else {
			collapseToolbar.setBackgroundDrawable(new BitmapDrawable(getResources(), image));
		}
	}

	private void setupViewPager(ViewPager viewPager) {

		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

		profileFragment = new ProfileFragment();
		diaryFragment = new DiaryFragment();
		placesFragment = new PlacesFragment();

		adapter.addFragment(profileFragment, getString(R.string.profile));
		adapter.addFragment(diaryFragment, getString(R.string.diary));
		adapter.addFragment(placesFragment, getString(R.string.places));
		viewPager.setAdapter(adapter);
	}

	class ViewPagerAdapter extends FragmentPagerAdapter {

		private final List<Fragment> mFragmentList = new ArrayList<>();
		private final List<String> mFragmentTitleList = new ArrayList<>();

		public ViewPagerAdapter(FragmentManager manager) {
			super(manager);
		}

		@Override
		public Fragment getItem(int position) {
			return mFragmentList.get(position);
		}

		@Override
		public int getCount() {
			return mFragmentList.size();
		}

		public void addFragment(Fragment fragment, String title) {
			mFragmentList.add(fragment);
			mFragmentTitleList.add(title);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return mFragmentTitleList.get(position);
		}
	}

}
