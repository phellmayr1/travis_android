package fhhgb.travis.philipp.travis_android.rest.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

/**
 * @author Philipp
 *         Created on 30.12.2015.
 */

public class SearchResultUserList {

	@SerializedName("searchResult")
	LinkedList<SearchResultUser> searchResults;

	public SearchResultUserList(LinkedList<SearchResultUser> searchResults) {
		this.searchResults = searchResults;
	}

	public LinkedList<SearchResultUser> getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(LinkedList<SearchResultUser> searchResults) {
		this.searchResults = searchResults;
	}
}
