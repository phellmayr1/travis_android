package fhhgb.travis.philipp.travis_android;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.ParseException;
import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.helper.DateUtils;
import fhhgb.travis.philipp.travis_android.helper.ISO8601;
import fhhgb.travis.philipp.travis_android.rest.model.response.TripActivity;

/**
 * Add, Modify a Tripactivity
 */
public class NewTripactivityActivity extends AppCompatActivity {

	private static final int SELECT_LOCATION_REQUEST_CODE = 3;
	@Bind(R.id.descriptionTextContent)
	EditText mActivityName;

	@Bind(R.id.informationTextContent)
	EditText mDescription;

	@Bind(R.id.costsTextContent)
	EditText mCosts;

	@Bind(R.id.currencySpinner)
	Spinner mCurrencySpinner;

	@Bind(R.id.fromTextContent)
	TextView mFromDate;

	@Bind(R.id.fromTimeTextContent)
	TextView mFromTime;

	@Bind(R.id.toTextContent)
	TextView mToDate;

	@Bind(R.id.toTimeTextContent)
	TextView mToTime;

	@Bind(R.id.timeZoneSpinner)
	Spinner mTimeZone;

	@Bind(R.id.locationTextView)
	TextView mLocationTextView;

	TripActivity tripactivity;

	@Bind(R.id.informationCount)
	TextView informationCount;

	@Bind(R.id.descriptionCount)
	TextView descriptionCount;

	boolean isAdmin = false;

	private void loadExtras() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			tripactivity = (TripActivity) extras.getSerializable(Constants.TRIPACTIVITY);
			isAdmin = extras.getBoolean(Constants.IS_ADMIN);
		}
		if (tripactivity.getLocation() != null) {
			tripactivity.setLocationName(tripactivity.getLocation().getName());
			tripactivity.setLocationId(tripactivity.getLocation().getId());
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_tripactivity);

		ButterKnife.bind(this);
		loadExtras();
		setupToolbar();
		loadUIData();

	}

	/**
	 * Adds the listeners to make the data modifiable
	 */
	private void addActionListenerToFields() {
		mLocationTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				NewTripactivityActivity.this.startActivityForResult(new Intent("fhhgb.travis.philipp.travis_android" +
								".SearchLocationActivity"),
						SELECT_LOCATION_REQUEST_CODE);
			}
		});

		mFromDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				DatePickerDialog datePickerDialog = new DatePickerDialog(NewTripactivityActivity.this, new DateSetListener(mFromDate, 1),
						tripactivity.getBegin().get(Calendar.YEAR),
						tripactivity.getBegin().get(Calendar.MONTH), tripactivity.getBegin().get(Calendar.DAY_OF_MONTH));
				datePickerDialog.show();
			}
		});

		mToDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				DatePickerDialog datePickerDialog = new DatePickerDialog(NewTripactivityActivity.this, new DateSetListener(mToDate, 2),
						tripactivity.getEnd().get(Calendar.YEAR),
						tripactivity.getEnd().get(Calendar.MONTH), tripactivity.getEnd().get(Calendar.DAY_OF_MONTH));
				datePickerDialog.show();
			}
		});

		mFromTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TimePickerDialog timePickerdialog = new TimePickerDialog(NewTripactivityActivity.this, new TimeSetListener(mFromTime, 1),
						tripactivity.getBegin().get(Calendar.HOUR_OF_DAY), tripactivity.getBegin().get(Calendar.MINUTE),
						true);
				timePickerdialog.show();
			}
		});

		mToTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TimePickerDialog timePickerdialog = new TimePickerDialog(NewTripactivityActivity.this, new TimeSetListener(mToTime, 2),
						tripactivity.getEnd().get(Calendar.HOUR_OF_DAY), tripactivity.getEnd().get(Calendar.MINUTE),
						true);
				timePickerdialog.show();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == SELECT_LOCATION_REQUEST_CODE) {
			if (data != null) {
				Bundle resultBundle = data.getExtras();
				tripactivity.setLocationId(resultBundle.getInt(Constants.LOCATION_ID));
				tripactivity.setLocationName(resultBundle.getString(Constants.LOCATION_NAME));
				mLocationTextView.setText(resultBundle.getString(Constants.LOCATION_NAME));
			}
		}
	}

	/**
	 * Set UI contents to the contents of the model
	 */
	private void loadUIData() {

		mActivityName.addTextChangedListener(new TextCountSetter(descriptionCount));
		mDescription.addTextChangedListener(new TextCountSetter(informationCount));

		if (isAdmin) {
			addActionListenerToFields();
			mDescription.setFocusableInTouchMode(true);
			mCosts.setFocusableInTouchMode(true);
			mDescription.setFocusableInTouchMode(true);
			mActivityName.setFocusableInTouchMode(true);
		} else {
			mCurrencySpinner.setEnabled(false);
			mCurrencySpinner.setClickable(false);
			mTimeZone.setEnabled(false);
			mTimeZone.setClickable(false);
		}

		mLocationTextView.setText(tripactivity.getLocationName());
		mDescription.setText(tripactivity.getText());
		mCosts.setText(tripactivity.getCosts() + "");

		try {
			if (tripactivity.getBeginDate() != null) {
				tripactivity.setBegin(ISO8601.toCalendar(tripactivity.getBeginDate()));
			}
			if (tripactivity.getEndDate() != null) {
				tripactivity.setEnd(ISO8601.toCalendar(tripactivity.getEndDate()));
			}
		} catch (
				ParseException e
				)

		{
			e.printStackTrace();
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, Constants.CURRENCIES);
		mCurrencySpinner.setAdapter(adapter);

		mCurrencySpinner.setSelection(adapter.getPosition(tripactivity.getCurrency()));

		ArrayAdapter<String> timeZoneAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, Constants.TIME_ZONES);
		mTimeZone.setAdapter(timeZoneAdapter);

		mTimeZone.setSelection(timeZoneAdapter.getPosition(tripactivity.getTimezone()));

		mActivityName.setText(tripactivity.getName());

		mFromDate.setText(DateUtils.formatDateInts(tripactivity.getBegin().get(Calendar
						.YEAR),
				tripactivity.getBegin().get(Calendar.MONTH), tripactivity.getBegin().get(Calendar.DAY_OF_MONTH)));

		mToDate.setText(DateUtils.formatDateInts(tripactivity.getEnd().get(Calendar.YEAR),
				tripactivity.getEnd().get(Calendar.MONTH), tripactivity.getEnd().get(Calendar.DAY_OF_MONTH)));

		mFromTime.setText(tripactivity.getBegin().get(Calendar.HOUR)
				+ ":" + tripactivity.getBegin().get(Calendar.MINUTE));

		mToTime.setText(tripactivity.getEnd().get(Calendar.HOUR)
				+ ":" + tripactivity.getEnd().get(Calendar.MINUTE));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_create_diary, menu);
		return true;
	}

	private void setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			setResult(RESULT_CANCELED);
			onBackPressed();
		}

		//Finished with activity details, return to the trip view
		if (item.getItemId() == R.id.saveDiaryEntry) {
			loadCurrentTripActivityFromUI();

			if (tripactivity.getName().isEmpty()) {
				new AlertDialog.Builder(this)
						.setTitle(getString(R.string.new_activity))
						.setMessage(getString(R.string.please_enter_the_activitys_name))
						.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// continue with delete
							}
						})
						.setIcon(android.R.drawable.ic_dialog_alert)
						.show();
			} else {
				Intent intent = new Intent();
				intent.putExtra(Constants.TRIPACTIVITY, tripactivity);
				setResult(RESULT_OK, intent);
				finish();
			}
		}

		return super.

				onOptionsItemSelected(item);
	}

	private void loadCurrentTripActivityFromUI() {

		tripactivity.setName(mActivityName.getText().toString());
		tripactivity.setText(mDescription.getText().toString());
		tripactivity.setCosts(Double.valueOf(String.valueOf(mCosts.getText())));
		//LocationId gets set in onActivityResult
		tripactivity.setCurrency(mCurrencySpinner.getSelectedItem().toString());
		tripactivity.setTimezone(mTimeZone.getSelectedItem().toString());

		//This is needed for the backend communication. The Date must be submitted
		//as ISO8601 String. That's why the string representation of the date is saved
		//in the field BeginDate while the Calender representation (for local purposes)
		//is stored in the field Begin
		tripactivity.setBeginDate(ISO8601.fromCalendar(tripactivity.getBegin()));
		tripactivity.setEndDate(ISO8601.fromCalendar(tripactivity.getEnd()));

	}
	/**
	 * Saves the new date from the UI in the trip
	 */
	class DateSetListener implements DatePickerDialog.OnDateSetListener {

		TextView dateTextView;
		int type;

		public DateSetListener(TextView dateTextView, int type) {
			this.dateTextView = dateTextView;
			this.type = type;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			dateTextView.setText(DateUtils.formatDateInts(year, monthOfYear, dayOfMonth));

			if (type == 1) {
				tripactivity.getBegin().set(year, monthOfYear, dayOfMonth);
			} else {
				tripactivity.getEnd().set(year, monthOfYear, dayOfMonth);
			}
		}
	}
	/**
	 * Saves the new time from the UI in the trip
	 */
	class TimeSetListener implements TimePickerDialog.OnTimeSetListener {

		TextView dateTextView;
		int type;

		public TimeSetListener(TextView dateTextView, int type) {
			this.dateTextView = dateTextView;
			this.type = type;
		}

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			dateTextView.setText(hourOfDay + ":" + minute);

			if (type == 1) {
				tripactivity.getBegin().set(Calendar.HOUR, hourOfDay);
				tripactivity.getBegin().set(Calendar.MINUTE, minute);
			} else {
				tripactivity.getEnd().set(Calendar.HOUR, hourOfDay);
				tripactivity.getEnd().set(Calendar.MINUTE, minute);
			}
		}
	}

	/**
	 * When the number of digits of a editText change, the change will be shown in the UI
	 */
	public class TextCountSetter implements TextWatcher {

		TextView countTextView;

		public TextCountSetter(TextView countTextView) {
			this.countTextView = countTextView;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			countTextView.setText(s.length() + "");
		}

		@Override
		public void afterTextChanged(Editable s) {

		}
	}
}
