package fhhgb.travis.philipp.travis_android.rest.model.request;

/**
 * Created by Hellmayr on 26.03.2016.
 */
public class ActivatePushData {

	private String registrationToken;
	private boolean newFollowEntry;
	private boolean tripInvite;

	public ActivatePushData(String registrationToken, boolean newFollowEntry, boolean tripInvite) {
		this.registrationToken = registrationToken;
		this.newFollowEntry = newFollowEntry;
		this.tripInvite = tripInvite;
	}

	public String getRegistrationToken() {
		return registrationToken;
	}

	public void setRegistrationToken(String registrationToken) {
		this.registrationToken = registrationToken;
	}

	public boolean isNewFollowEntry() {
		return newFollowEntry;
	}

	public void setNewFollowEntry(boolean newFollowEntry) {
		this.newFollowEntry = newFollowEntry;
	}

	public boolean isTripInvite() {
		return tripInvite;
	}

	public void setTripInvite(boolean tripInvite) {
		this.tripInvite = tripInvite;
	}
}
