package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.rest.model.response.Location;

/**
 * Created by Hellmayr on 19.04.2016.
 */
public class SearchPlaceAdapter extends ArrayAdapter<Location> {

	Context context;
	int layoutResourceId;
	List<Location> data = null;

	public SearchPlaceAdapter(Context context, int layoutResourceId, List<Location> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	public SearchPlaceAdapter(Context context, int resource) {
		super(context, resource);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		gameHolder gh;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			gh = new gameHolder();
//			holder.imgIcon = (ImageView) row.findViewById(R.id.imgIcon);
			gh.placeName = (TextView) row.findViewById(R.id.txtTitle);
			gh.profileImage = (RoundedImageView) row.findViewById(R.id.profileimage);
//			holder.visited = (TextView) row.findViewById(R.id.visited);
//			holder.iconDown = (TextView) row.findViewById(R.id.icon_down);

//			Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fontawesome-webfont.ttf");
//			holder.iconDown.setTypeface(font);

			row.setTag(gh);
		} else {
			gh = (gameHolder) row.getTag();
		}

		final gameHolder holder = gh;

		Location user = data.get(position);

		holder.placeName.setText(user.getName());

//		ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance
//
//		final Bitmap[] bitmap = new Bitmap[1];
//
//		String url = user.getLoginImageUrl();
//		url = url.replace("width=100", "width=150");
//		url = url.replace("height=100", "height=150");
//
//		imageLoader.loadImage(url, new SimpleImageLoadingListener() {
//			@Override
//			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//				holder.profileImage.setImageBitmap(loadedImage);
//				notifyDataSetChanged();
//			}
//		});

		return row;
	}

	static class gameHolder {

		ImageView imgIcon;
		TextView placeName;
		RoundedImageView profileImage;
		TextView visited;
		TextView iconDown;
	}
}
