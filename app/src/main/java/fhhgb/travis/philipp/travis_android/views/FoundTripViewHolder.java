package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.LocationDetailsActivity;
import fhhgb.travis.philipp.travis_android.NewTripActivity;
import fhhgb.travis.philipp.travis_android.R;

/**
 * Created by Hellmayr on 06.06.2016.
 */
public class FoundTripViewHolder extends RecyclerView.ViewHolder {

	TextView nameTextView;
	TextView tripNameTextView;
	TextView tripTimeTextView;
	ImageView statusImage;
//	MapView mapView;

	ImageView cityImageView;
	TextView planATripTextView;
	FrameLayout content;

	int tripId;
	int locationId;
	String locationName;

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public void setNameTextView(TextView nameTextView) {
		this.nameTextView = nameTextView;
	}

	public void setContent(FrameLayout content) {
		this.content = content;
	}

	public FoundTripViewHolder(View itemView, final Activity currentActivity) {
		super(itemView);

		nameTextView = (TextView) itemView.findViewById(R.id.nameTextView);

		cityImageView = (ImageView) itemView.findViewById(R.id.city_image);
		planATripTextView = (TextView) itemView.findViewById(R.id.planATrip);

		tripNameTextView = (TextView) itemView.findViewById(R.id.tripNameTextView);
		tripTimeTextView = (TextView) itemView.findViewById(R.id.timeTextView);
		statusImage = (ImageView) itemView.findViewById(R.id.statusImage);

		content = (FrameLayout) itemView.findViewById(R.id.content);

//		planATripTextView.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(currentActivity, NewTripActivity.class);
//				intent.putExtra(Constants.LOCATION_ID, locationId);
//				intent.putExtra(Constants.LOCATION_NAME, locationName);
//				currentActivity.startActivity(intent);
//			}
//		});
//
		content.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(currentActivity, NewTripActivity.class);
				intent.putExtra(Constants.LOCATION_ID, locationId);
				intent.putExtra(Constants.LOCATION_NAME, locationName);
				intent.putExtra(Constants.TRIP_ID, tripId);
				currentActivity.startActivity(intent);
			}
		});

		nameTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(currentActivity, LocationDetailsActivity.class);
				intent.putExtra(Constants.LOCATION_ID, locationId);
				currentActivity.startActivity(intent);
			}
		});

	}
}
