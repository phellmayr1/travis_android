package fhhgb.travis.philipp.travis_android.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.push.QuickstartPreferences;
import fhhgb.travis.philipp.travis_android.push.RegistrationIntentService;

/**
 * Created by Hellmayr on 11.05.2016.
 */
public class SettingsFragment extends android.preference.PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private BroadcastReceiver mRegistrationBroadcastReceiver;
	private boolean isReceiverRegistered;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Load the preferences from an XML resource
		addPreferencesFromResource(R.xml.preferences);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals(Constants.PREFERENCE_SERVER)) {
			Toast.makeText(getActivity(), "Restart app to apply Server change", Toast.LENGTH_LONG).show();
		}
		if (key.equals(Constants.PREFERENCE_PUSH_DIARY_CREATED) || key.equals(Constants.PREFERENCE_PUSH_TRIP_INVITED)) {
			mRegistrationBroadcastReceiver = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					//mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
					SharedPreferences sharedPreferences =
							PreferenceManager.getDefaultSharedPreferences(context);
					boolean sentToken = sharedPreferences
							.getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
					if (sentToken) {
						//mInformationTextView.setText(getString(R.string.gcm_send_message));
					} else {
						//	mInformationTextView.setText(getString(R.string.token_error_message));
					}
				}
			};

			registerReceiver();

			if (checkPlayServices()) {
				// Start IntentService to register this application with GCM.
				Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
				getActivity().startService(intent);
			}
		}
	}

	private void registerReceiver() {
		if (!isReceiverRegistered) {
			LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
					new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
			isReceiverRegistered = true;
		}
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the de vice's system settings.
	 */
	private boolean checkPlayServices() {
		GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
		int resultCode = apiAvailability.isGooglePlayServicesAvailable(getActivity());
		if (resultCode != ConnectionResult.SUCCESS) {
			if (apiAvailability.isUserResolvableError(resultCode)) {
				apiAvailability.getErrorDialog(getActivity(), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
						.show();
			} else {
				Log.i("Settings", "This device is not supported.");
				getActivity().finish();
			}
			return false;
		}
		return true;
	}

	@Override
	public void onResume() {
		super.onResume();
		getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

	}

	@Override
	public void onPause() {
		getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
		super.onPause();
	}
}
