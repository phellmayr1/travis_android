package fhhgb.travis.philipp.travis_android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.helper.UrlBuilder;
import fhhgb.travis.philipp.travis_android.rest.RestConstants;
import fhhgb.travis.philipp.travis_android.rest.model.response.DiaryCoordinatesElement;
import fhhgb.travis.philipp.travis_android.rest.model.response.DiaryElements;
import fhhgb.travis.philipp.travis_android.rest.model.response.DiaryImageElement;
import fhhgb.travis.philipp.travis_android.rest.model.response.DiaryTextElement;
import fhhgb.travis.philipp.travis_android.rest.service.DiaryService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Show the diary elements. Each diary might consist of image, map and text elements.
 */
public class ShowDiaryElementsActivity extends AppCompatActivity {

	@Bind(R.id.diaryElementsScrollView)
	NestedScrollView nestedScrollView;

	@Bind(R.id.username)
	TextView usernameTextView;

	@Bind(R.id.diaryEntryName)
	TextView diaryEntryNameTextView;

	@Bind(R.id.elementsContainer)
	LinearLayout elementsContainer;

	DiaryElements diaryElements;

	private void setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.close);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		finish();

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Iterates over all types of diary elements and defines the biggest number of the sort
	 * This is necessary, because it is possible that any number is missing. E.g 1,2,3,5 .
	 * Here 4 is missing and then, 5 would not be shown in the view.
	 * @param diaryElements
	 * @return
	 */
	public int getMaxSize(DiaryElements diaryElements) {
		int max = 0;
		for (DiaryTextElement textElement : diaryElements.getTextElements()) {
			if (textElement.getSort() > max) {
				max = textElement.getSort();
			}
		}

		for (DiaryImageElement textElement : diaryElements.getImageElements()) {
			if (textElement.getSort() > max) {
				max = textElement.getSort();
			}
		}

		for (DiaryCoordinatesElement textElement : diaryElements.getCoordinatesElements()) {
			if (textElement.getSort() > max) {
				max = textElement.getSort();
			}
		}
		return max;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_diary_elements);
		ButterKnife.bind(this);

		setupToolbar();

		diaryElements = (DiaryElements) getIntent().getSerializableExtra(Constants.DIARY_ELEMENTS);

		int dId = (int) getIntent().getSerializableExtra(Constants.DIARY_ID);

		//Load all the elements from the diary
		final DiaryService userService = SessionData.getRestClient().getDiaryService();
		Call<DiaryElements> retval = userService.findDiaryEntryElements(dId);
		retval.enqueue(new Callback<DiaryElements>() {

			@Override
			public void onResponse(Response<DiaryElements> response, Retrofit retrofit) {
				diaryElements = response.body();
				showDiaryElements();
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getParent(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	private void showDiaryElements() {
		String username = getIntent().getStringExtra(Constants.CURRENT_PROFILE_USERNAME);
		String diaryTitle = getIntent().getStringExtra(Constants.DIARY_TITLE);

		usernameTextView.setText(username);
		diaryEntryNameTextView.setText(diaryTitle);

		final LayoutInflater inflater = LayoutInflater.from(this);

		int currentSort = 0;
		int elementcount = 0;
		int allElements = getMaxSize(diaryElements);

		boolean resultFound = false;

		//Iterate over all the elements and add it according to the sorting
		do {

			for (DiaryTextElement textElement : diaryElements.getTextElements()) {

				if (textElement.getSort() == currentSort) {

					TextView textCardView = (TextView) inflater.inflate(R.layout.textview_diaryelement_text, elementsContainer, false);
					textCardView.setText(textElement.getText());

					elementsContainer.addView(textCardView);

					diaryElements.getTextElements().remove(textElement);
					resultFound = true;
					break;
				}
			}

			if (!resultFound) {

				for (final DiaryImageElement imageElement : diaryElements.getImageElements()) {

					if (imageElement.getSort() == currentSort) {

						final ImageView imageView = (ImageView) inflater.inflate(R.layout.imageview_diaryelement_image, elementsContainer,
								false);

						ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

						imageLoader.loadImage(RestConstants.BASE_URL + imageElement.getImageUrl(), new SimpleImageLoadingListener() {
							@Override
							public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
								imageView.setImageBitmap(loadedImage);
							}
						});

						elementsContainer.addView(imageView);

						diaryElements.getImageElements().remove(imageElement);
						resultFound = true;
						break;
					}
				}

			}

			if (!resultFound) {

				for (final DiaryCoordinatesElement coordinatesElement : diaryElements.getCoordinatesElements()) {

					if (coordinatesElement.getSort() == currentSort) {

						final ImageView imageView = (ImageView) inflater.inflate(R.layout.imageview_diaryelement_image, elementsContainer,
								false);

						ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

						imageLoader.loadImage(UrlBuilder.buildMapImageUrl(coordinatesElement.getCoordinates().getLongitude(),
								coordinatesElement.getCoordinates().getLatitude()), new
								SimpleImageLoadingListener
										() {
									@Override
									public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
										imageView.setImageBitmap(loadedImage);
									}
								});

						elementsContainer.addView(imageView);

						diaryElements.getCoordinatesElements().remove(coordinatesElement);
						resultFound = true;
						break;
					}
				}

			}

			resultFound = false;
			currentSort++;
			elementcount++;
		} while (elementcount <= allElements);

		overrideFonts(this, nestedScrollView);
	}

	/**
	 * Use a custom font to make this thing more beautiful
	 * @param context
	 * @param v
	 */
	private void overrideFonts(final Context context, final View v) {
		try {
			if (v instanceof ViewGroup) {
				ViewGroup vg = (ViewGroup) v;
				for (int i = 0; i < vg.getChildCount(); i++) {
					View child = vg.getChildAt(i);
					overrideFonts(context, child);
				}
			} else if (v instanceof TextView) {
				((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "NotoSerif-Regular.ttf"));
			}
		} catch (Exception e) {
		}
	}
}
