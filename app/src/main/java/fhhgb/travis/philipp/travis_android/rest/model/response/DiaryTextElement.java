package fhhgb.travis.philipp.travis_android.rest.model.response;

import java.io.Serializable;

/**
 * @author Philipp
 *         Created on 11.01.2016.
 */

public class DiaryTextElement implements Serializable {

	private int id;
	private int sort;
	private String text;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
