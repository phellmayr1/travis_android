package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;

import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.rest.model.response.Location;

/**
 * @author Philipp
 *         Created on 08.01.2016.
 */

public class SearchLocationAdapter extends ArrayAdapter<Location> {

	Context context;
	int layoutResourceId;
	LinkedList<Location> data = null;

	public SearchLocationAdapter(Context context, int layoutResourceId, LinkedList<Location> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		gameHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new gameHolder();
//			holder.imgIcon = (ImageView) row.findViewById(R.id.imgIcon);
			holder.placeName = (TextView) row.findViewById(R.id.txtTitle);
//			holder.visited = (TextView) row.findViewById(R.id.visited);
//			holder.iconDown = (TextView) row.findViewById(R.id.icon_down);

//			Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fontawesome-webfont.ttf");
//			holder.iconDown.setTypeface(font);

			row.setTag(holder);
		} else {
			holder = (gameHolder) row.getTag();
		}

		Location user = data.get(position);

		holder.placeName.setText(user.getName());
//		holder.imgIcon.setImageResource(R.drawable.profile_background);
//		holder.visited.setText("xy");

		return row;
	}

	static class gameHolder {

		ImageView imgIcon;
		TextView placeName;
		TextView visited;
		TextView iconDown;
	}


}
