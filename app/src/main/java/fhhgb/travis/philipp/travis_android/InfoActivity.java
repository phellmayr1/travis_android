package fhhgb.travis.philipp.travis_android;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONObject;

import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;
import fhhgb.travis.philipp.travis_android.helper.DateUtils;
import fhhgb.travis.philipp.travis_android.push.QuickstartPreferences;
import fhhgb.travis.philipp.travis_android.push.RegistrationIntentService;
import fhhgb.travis.philipp.travis_android.rest.RestClient;
import fhhgb.travis.philipp.travis_android.rest.RestConstants;
import fhhgb.travis.philipp.travis_android.rest.model.request.SmallUser;
import fhhgb.travis.philipp.travis_android.rest.model.response.SessionId;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;
import fhhgb.travis.philipp.travis_android.rest.service.LoginService;
import fhhgb.travis.philipp.travis_android.rest.service.UserService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class InfoActivity extends AppCompatActivity {

	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private static final String TAG = "InfoActivity";

	private BroadcastReceiver mRegistrationBroadcastReceiver;
	private boolean isReceiverRegistered;

	CallbackManager callbackManager;

	LoginResult fbLoginResult;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		FacebookSdk.sdkInitialize(getApplicationContext());
		callbackManager = CallbackManager.Factory.create();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);
		ButterKnife.bind(this);

		loadSettings();


		//This is the initial configuration of the image cache
		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.build();

		// Create global configuration and initialize ImageLoader with this config
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
				.defaultDisplayImageOptions(defaultOptions)
				.build();
		ImageLoader.getInstance().init(config);

		//This is true, if the user has already logged in with facebook
		if (AccessToken.getCurrentAccessToken() != null) {
			SessionData.setFbProfile(Profile.getCurrentProfile());
			loadProfilePicture();
			loginOnServer(AccessToken.getCurrentAccessToken().getToken());
		}

		LoginButton loginButton = (LoginButton) findViewById(R.id.facebook_login_button);
		loginButton.setReadPermissions("user_birthday");

		loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

			private ProfileTracker mProfileTracker;

			@Override
			public void onSuccess(final LoginResult loginResult) {
				Log.e("Login", "Success");

				if (Profile.getCurrentProfile() == null) {
					mProfileTracker = new ProfileTracker() {
						@Override
						protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
							Log.v("facebook - profile", profile2.getFirstName());
							mProfileTracker.stopTracking();
							loadFacebookProfileFinished(loginResult);
						}
					};
					mProfileTracker.startTracking();
				} else {
					loadFacebookProfileFinished(loginResult);
				}
			}

			@Override
			public void onCancel() {
				Log.e("Login", "Cancel");
			}

			@Override
			public void onError(FacebookException error) {
				Log.e("Login", "Error");
			}
		});

	}

	/**
	 * Reads the URL of the backend from the preferences
	 */
	private void loadSettings() {

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

		String serverUrl = preferences.getString(Constants.PREFERENCE_SERVER, "");

		if (!serverUrl.equals("")) {
			RestConstants.BASE_URL = serverUrl;
		}

	}

	/**
	 * Request the birthday from facebook and save it in the backend
	 * @param token
	 */
	private void requestBirthday(AccessToken token) {
		GraphRequest request = GraphRequest.newMeRequest(
				token,
				new GraphRequest.GraphJSONObjectCallback() {
					@Override
					public void onCompleted(JSONObject object, GraphResponse response) {
						Log.v("LoginActivity", response.toString());

						try {
							final String birthday = object.getString("birthday"); // 01/31/1980 format

							//First check if the user already has a birthday
							//This is important, if the user has set a different birthday than on the
							//facebook profile

							UserService userService = SessionData.getRestClient().getUserService();

							Call<User> retval = userService.getMyProfile();
							retval.enqueue(new Callback<User>() {

								@Override
								public void onResponse(Response<User> response, Retrofit retrofit) {

									if (ServerError.hasNoServerError(response, InfoActivity.this)) {

										if (response.body().getBirthday() == null) {

											String[] birthdaySplit = birthday.split("/");

											UserService userService = SessionData.getRestClient().getUserService();

											Call<Void> retval = userService.changeBirthday(DateUtils.intToDateString(Integer.valueOf
															(birthdaySplit[2]),
													Integer.valueOf(birthdaySplit[0]),
													Integer.valueOf(birthdaySplit[1])));
											retval.enqueue(new Callback<Void>() {

												@Override
												public void onResponse(Response<Void> response, Retrofit retrofit) {
												}

												@Override
												public void onFailure(Throwable t) {
													Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
												}
											});
										}
									}
								}

								@Override
								public void onFailure(Throwable t) {
									Toast.makeText(getBaseContext(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
								}
							});
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
		Bundle parameters = new Bundle();
		parameters.putString("fields", "id,name,email,gender, birthday");
		request.setParameters(parameters);
		request.executeAsync();
	}

	/**
	 * When Facebook login was successfull, the user must be logged in at the backend
	 * @param loginResult
	 */
	private void loadFacebookProfileFinished(LoginResult loginResult) {
		loadProfilePicture();
		SessionData.setFbProfile(Profile.getCurrentProfile());
		fbLoginResult = loginResult;
		loginOnServer(loginResult.getAccessToken().getToken());
	}

	/**
	 * Login on the backend server with the facebook credentials
	 * @param token
	 */
	private void loginOnServer(String token) {

		final AlertDialog dialog = new SpotsDialog(this, getString(R.string.login));
		dialog.show();

		RestClient rc = new RestClient();

		SessionData.setRestClient(rc);

		LoginService loginService = rc.getLoginService();
		SmallUser smallUser = new SmallUser(Constants.LOGIN_TYPE_FACEBOOK, token, SessionData
				.getFbProfile()
				.getFirstName(),
				SessionData
						.getFbProfile()
						.getLastName());
		Call<SessionId> b = loginService.loginUser(smallUser);
		b.enqueue(new Callback<SessionId>() {

			@Override
			public void onResponse(Response<SessionId> response, Retrofit retrofit) {

				if (ServerError.hasNoServerError(response, InfoActivity.this)) {
					SessionData.setSessionId(response.body().getSessionId());
					SessionData.setUserId(response.body().getUserId());
					if (fbLoginResult != null) {
						requestBirthday(fbLoginResult.getAccessToken());
					} else {
						requestBirthday(AccessToken.getCurrentAccessToken());

					}
				}

				//This is needed for Push notifications
				mRegistrationBroadcastReceiver = new BroadcastReceiver() {
					@Override
					public void onReceive(Context context, Intent intent) {
						SharedPreferences sharedPreferences =
								PreferenceManager.getDefaultSharedPreferences(context);
					}
				};

				registerReceiver();

				if (checkPlayServices()) {
					// Start IntentService to register this application with GCM.
					Intent intent = new Intent(getApplicationContext(), RegistrationIntentService.class);
					startService(intent);
				}

				goToOverview();
				dialog.dismiss();
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
				dialog.dismiss();
			}
		});
	}

	private void goToOverview() {
		Intent i = new Intent(getApplicationContext(), MainPageActivity.class);
		startActivity(i);
		finish();
	}

	private void loadProfilePicture() {
		Profile profile = Profile.getCurrentProfile();

		//TODO: use new introduced params for with and height from the server
		Uri fbProfilePicUrl = profile.getProfilePictureUri(300, 300);

		ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

		imageLoader.loadImage(fbProfilePicUrl.toString(), new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				SessionData.setFbProfilePic(loadedImage);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		//registerReceiver();
	}

	@Override
	protected void onPause() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
		isReceiverRegistered = false;
		super.onPause();
	}

	/**
	 * This receiver is used to receive the push notifications
	 */
	private void registerReceiver() {
		if (!isReceiverRegistered) {
			LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
					new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
			isReceiverRegistered = true;
		}
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the de vice's system settings.
	 */
	private boolean checkPlayServices() {
		GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
		int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (apiAvailability.isUserResolvableError(resultCode)) {
				apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
						.show();
			} else {
				Log.i(TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}
}
