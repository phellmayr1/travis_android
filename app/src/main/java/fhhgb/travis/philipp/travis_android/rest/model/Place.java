package fhhgb.travis.philipp.travis_android.rest.model;

import java.util.Date;

/**
 * @author Philipp
 *         Created on 22.12.2015.
 */

public class Place {

	String name;
	Date visited;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getVisited() {
		return visited;
	}

	public void setVisited(Date visited) {
		this.visited = visited;
	}
}
