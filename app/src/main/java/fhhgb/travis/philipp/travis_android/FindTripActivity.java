package fhhgb.travis.philipp.travis_android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fhhgb.travis.philipp.travis_android.fragments.TinderSwipeFragment;
import fhhgb.travis.philipp.travis_android.rest.service.RatingService;
import fhhgb.travis.philipp.travis_android.views.FlingCardListener;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Find a trip with the user's preferences
 */
public class FindTripActivity extends AppCompatActivity implements FlingCardListener.ActionDownInterface {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_find_trip);

		getServerData();

	}

	@Override
	protected void onResume() {
		super.onResume();
		getServerData();
	}

	@Override
	public void onActionDownPerform() {
	}

	/**
	 * Gets the different rating categories from the server.
	 * After that, the categories will be forwarded to the fragment.
	 */
	public void getServerData() {
		final RatingService ratingService = SessionData.getRestClient().getRatingService();

		Call<List<String>> retval = ratingService.findRatingCategories(Constants.RATING_GROUP_LOCATION);

		retval.enqueue(new Callback<List<String>>() {

			@Override
			public void onResponse(Response<List<String>> response, Retrofit retrofit) {

				//In this Fragment, the swipe view will be shown
				TinderSwipeFragment ratingFragment = new TinderSwipeFragment();

				Bundle bundle = new Bundle();
				bundle.putStringArrayList("cats", (ArrayList<String>) response.body());

				ratingFragment.setArguments(bundle);

				getSupportFragmentManager().beginTransaction()
						.add(R.id.frame, ratingFragment).commit();
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

}
