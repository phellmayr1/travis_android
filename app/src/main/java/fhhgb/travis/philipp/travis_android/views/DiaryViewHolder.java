package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.ShowDiaryElementsActivity;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;

/**
 * @author Philipp
 *         Created on 11.01.2016.
 */

public class DiaryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

	protected TextView diaryTitle;
	protected TextView locationText;
	protected TextView visitedTimeText;

	protected TextView visitedTimeIcon;
	protected TextView locationIcon;

	User userToShow;

	private int diaryId;

	public int getDiaryId() {
		return diaryId;
	}

	public void setDiaryId(int diaryId) {
		this.diaryId = diaryId;
	}

	protected Activity currentActivity;

	public DiaryViewHolder(View v, Activity activity, User userToShow) {
		super(v);
		diaryTitle = (TextView) v.findViewById(R.id.diary_title);
		locationText = (TextView) v.findViewById(R.id.location_text);
		visitedTimeText = (TextView) v.findViewById(R.id.visitedtime_text);
		currentActivity = activity;
		visitedTimeIcon = (TextView) v.findViewById(R.id.visitedtime);
		locationIcon = (TextView) v.findViewById(R.id.location);
		this.userToShow=userToShow;
		v.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {

				Intent intent = new Intent(currentActivity, ShowDiaryElementsActivity.class);
				intent.putExtra(Constants.CURRENT_PROFILE_USERNAME, userToShow.getFirstName()+" "+userToShow.getLastName());
				intent.putExtra(Constants.DIARY_TITLE, diaryTitle.getText()+"");
				intent.putExtra(Constants.DIARY_ID, diaryId);
				currentActivity.startActivity(intent);
	}
}
