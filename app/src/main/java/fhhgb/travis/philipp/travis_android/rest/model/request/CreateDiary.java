package fhhgb.travis.philipp.travis_android.rest.model.request;

/**
 * @author Philipp
 *         Created on 08.01.2016.
 */

public class CreateDiary {

	private String name;
	private int locationId;

	public CreateDiary(String name, int placeId) {
		this.name = name;
		this.locationId = placeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
}
