package fhhgb.travis.philipp.travis_android;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.ImageHolder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.fragments.OverviewFragment;
import fhhgb.travis.philipp.travis_android.rest.service.LoginService;
import fhhgb.travis.philipp.travis_android.views.BlurBuilder;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class MainPageActivity extends AppCompatActivity {

	@Bind(R.id.progressBar)
	ProgressBar progressBar;

	static RelativeLayout loadingPanel;

	public static void uploadingFinished() {
		loadingPanel.setVisibility(View.INVISIBLE);
	}

	private static final String SHOW_CASE_UPLOAD = "showcaseUpload";

	private Toolbar setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setTitle("Travis");
		return toolbar;
	}

	@Override
	public void onBackPressed() {

		//The user should not quit the app when an image upload is still in progress
		if (SessionData.getCurrentUploadingCounter() == 0) {
			super.onBackPressed();
		} else {
			Toast.makeText(MainPageActivity.this, "Uploading is still in progress...", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_overview, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		if (menuItem.getItemId() == R.id.searchMenuButton) {
			Intent intent = new Intent(getBaseContext(), SearchActivity.class);
			startActivity(intent);
		}
		return super.onOptionsItemSelected(menuItem);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_overview);
		ButterKnife.bind(this);

		loadingPanel = (RelativeLayout) findViewById(R.id.loadingPanel);

		//When an image upload is in progress, the loading panel should be visible in the toolbar
		if (SessionData.getCurrentUploadingCounter() == 0) {
			loadingPanel.setVisibility(View.INVISIBLE);
		} else {
			loadingPanel.setVisibility(View.VISIBLE);
			showShowCaseView();
		}

		progressBar.getIndeterminateDrawable().setColorFilter(0xFFffffff,
				android.graphics.PorterDuff.Mode.MULTIPLY);

		loadingPanel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getBaseContext(), UploadStatusActivity.class);
				startActivity(intent);
			}
		});

		Toolbar toolbar = setupToolbar();
		createSidebar(toolbar);

		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		OverviewFragment fragment = new OverviewFragment();
		fragmentTransaction.add(R.id.overview_fragment_container, fragment);
		fragmentTransaction.commit();
	}

	private void showShowCaseView() {

		new MaterialShowcaseView.Builder(this)
				.setTarget(loadingPanel)
				.setDismissText("GOT IT")
				.setContentText(getString(R.string.showcase_upload_diary_entry))
				.setDelay(500) // optional but starting animations immediately in onCreate can make them choppy
				.singleUse(SHOW_CASE_UPLOAD) // provide a unique ID used to ensure it is only shown once
				.show();

		ShowcaseConfig config = new ShowcaseConfig();
		config.setDelay(500); // half second between each showcase view

		MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this, SHOW_CASE_UPLOAD);

		sequence.setConfig(config);

		sequence.start();
	}

	private void createSidebar(Toolbar toolbar) {
		final PrimaryDrawerItem mainPageItem = new PrimaryDrawerItem().withName(R.string.start).withIcon(GoogleMaterial.Icon.gmd_explore);
		final PrimaryDrawerItem diaryItem = new PrimaryDrawerItem().withName(R.string.new_diary).withIcon(GoogleMaterial.Icon
				.gmd_account_circle);

		final PrimaryDrawerItem tripplanner = new PrimaryDrawerItem().withName(R.string.trip_planner).withIcon(GoogleMaterial.Icon
				.gmd_flight_takeoff);

		final SecondaryDrawerItem signOutItem = new SecondaryDrawerItem().withName(R.string.sign_out).withIcon(FontAwesome.Icon
				.faw_sign_out);

		final SecondaryDrawerItem settingsItem = new SecondaryDrawerItem().withName(R.string.settings).withIcon(FontAwesome.Icon
				.faw_gear);

		final SecondaryDrawerItem likedItem = new SecondaryDrawerItem().withName(R.string.liked_diary_entries).withIcon(FontAwesome.Icon
				.faw_heart);

		Bitmap image = SessionData.getBlurredStandardBackgroundImage();
		if (image == null) {
			Bitmap img = BitmapFactory.decodeResource(getResources(),
					R.drawable.profile_background);
			View content = findViewById(android.R.id.content).getRootView();
			image = BlurBuilder.blur(content, img);
			SessionData.setBlurredStandardBackgroundImage(image);
		}

		ImageHolder imageHolder = new ImageHolder(image);

		String username = "";

		if (SessionData.getFbProfile() != null) {
			username = SessionData.getFbProfile().getName();
		}

		// Create the AccountHeader
		AccountHeader headerResult = new AccountHeaderBuilder()
				.withActivity(this)
				.withHeaderBackground(imageHolder)
				.withSelectionListEnabledForSingleProfile(false)
				.addProfiles(
						new ProfileDrawerItem().withName(username).withIcon
								(SessionData
										.getFbProfilePic())
				)
				.withOnAccountHeaderSelectionViewClickListener(new AccountHeader.OnAccountHeaderSelectionViewClickListener() {
					@Override
					public boolean onClick(View view, IProfile profile) {
						openProfileFragment();
						return false;
					}
				})
				.withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
					@Override
					public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
						openProfileFragment();
						return false;
					}
				})
				.build();

//create the drawer and remember the `Drawer` result object
		Drawer result = new DrawerBuilder()
				.withAccountHeader(headerResult)
				.withActivity(this)
				.withToolbar(toolbar)
				.addDrawerItems(
						mainPageItem,
						//			item2,
						diaryItem,
						tripplanner,
						likedItem,
						//		item4,
						//		item5,
						new DividerDrawerItem(),
						//		item6,
						settingsItem,
						signOutItem
				)
				.withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
					@Override
					public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

						if (drawerItem == mainPageItem) {
							Intent i = new Intent(getBaseContext(), MainPageActivity.class);
							startActivity(i);
							finish();
						}

						if (drawerItem == diaryItem) {
							Intent i = new Intent(getBaseContext(), CreateDiaryActivity.class);
							startActivity(i);
						}
						if (drawerItem == tripplanner) {
							Intent i = new Intent(getBaseContext(), TripsOverviewActivity.class);
							startActivity(i);
						}
						if (drawerItem == likedItem) {
							Intent i = new Intent(getBaseContext(), LikedDiariesActivity.class);
							startActivity(i);
						}

						if (drawerItem == signOutItem) {
							LoginService loginService = SessionData.getRestClient().getLoginService();

							Call<Void> b = loginService.logoutUser();
							b.enqueue(new Callback<Void>() {

								@Override
								public void onResponse(Response<Void> response, Retrofit retrofit) {
								}

								@Override
								public void onFailure(Throwable t) {
									Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
								}
							});
							LoginManager.getInstance().logOut();
							Intent intent = new Intent(getBaseContext(), InfoActivity.class);
							startActivity(intent);
							finish();
						}
						if (drawerItem == settingsItem) {
							Intent i = new Intent(getBaseContext(), SettingsActivity.class);
							startActivity(i);
						}
						return true;
					}
				})
				.build();
	}

	private void openProfileFragment() {
		Intent intent = new Intent(this, ProfileActivity.class);
		intent.putExtra(Constants.SHOW_MY_USER, true);
		startActivity(intent);
	}
}
