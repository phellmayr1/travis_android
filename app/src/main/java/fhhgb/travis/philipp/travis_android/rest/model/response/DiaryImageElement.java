package fhhgb.travis.philipp.travis_android.rest.model.response;

import java.io.Serializable;

/**
 * @author Philipp
 *         Created on 11.01.2016.
 */

public class DiaryImageElement implements Serializable {

	private int id;
	private  int sort;
	private String imageUrl;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
}
