package fhhgb.travis.philipp.travis_android;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.LinkedList;

import butterknife.Bind;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;
import fhhgb.travis.philipp.travis_android.helper.UrlBuilder;
import fhhgb.travis.philipp.travis_android.model.CreateDiaryElement;
import fhhgb.travis.philipp.travis_android.model.CreateDiaryImage;
import fhhgb.travis.philipp.travis_android.model.CreateDiaryLocation;
import fhhgb.travis.philipp.travis_android.model.DiaryUploadStatus;
import fhhgb.travis.philipp.travis_android.rest.model.request.CreateDiary;
import fhhgb.travis.philipp.travis_android.rest.model.request.DiaryLocation;
import fhhgb.travis.philipp.travis_android.rest.model.request.DiaryText;
import fhhgb.travis.philipp.travis_android.rest.model.response.NewDiary;
import fhhgb.travis.philipp.travis_android.rest.service.DiaryService;
import fhhgb.travis.philipp.travis_android.views.ProgressRequestBody;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Create a new diary with up to 20 image, map and text elements and save it in the backend
 */
public class CreateDiaryActivity extends AppCompatActivity {

	@Bind(R.id.cardsContrainer)
	LinearLayout cardsContainer;

	@Bind(R.id.addText)
	FloatingActionButton addTextButton;

	@Bind(R.id.addImage)
	FloatingActionButton addImageButton;

	@Bind(R.id.addLocation)
	FloatingActionButton addLocationButton;

	@Bind(R.id.floating_menu)
	FloatingActionMenu fabMenu;

	@Bind(R.id.diary_entries)
	TextView diaryEntriesProgress;

	@Bind(R.id.card_scrollview)
	NestedScrollView nestedScrollView;

	@Bind(R.id.title_input)
	EditText entryName;

	@Bind(R.id.choose_location)
	Button chooseLocationIcon;

	private String locationName;

	int entriesCount = -1;

	int RESULT_LOAD_IMAGE = 77;

	int PLACE_PICKER_REQUEST = 1;

	int diaryLocationRequestCode = 2;

	private int selectedLocationId = -1;

	private static final int PERMISSION_CODE_READ_EXTERNAL_STORAGE = 1;

	LinkedList<CreateDiaryElement> diaryCardViews;

	NewDiary newDiary;

	SpotsDialog dialog;

	int diaryElementCounter = 0;

	/**
	 * @return the increased counter of the current elements
	 */
	public int getIncreasedElementCounter() {
		diaryElementCounter++;
		return diaryElementCounter;
	}

	public void removeFromList(CreateDiaryElement createDiaryElement) {
		diaryCardViews.remove(createDiaryElement);

		substractDiaryEntryCount();
	}

	/**
	 * If an image, text or map element gets deleted, increment the item counter
	 */
	private void addDiaryEntryCount() {
		entriesCount++;
		setProgressText();
	}

	/**
	 * If an image, text or map element gets deleted, decrement the item counter
	 */
	private void substractDiaryEntryCount() {
		entriesCount--;
		setProgressText();
	}

	private void setProgressText() {
		diaryEntriesProgress.setText(entriesCount + "/" + Constants.MAX_DIARY_ENTRIES);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_diary);
		ButterKnife.bind(this);

		setupToolbar();

		addDiaryEntryCount();

		setupCollapsingToolbar();

		loadExtras();

		Typeface material = Typeface.createFromAsset(this.getAssets(), "material.ttf");
		chooseLocationIcon.setTypeface(material);

		diaryCardViews = new LinkedList<>();

		final LayoutInflater inflater = LayoutInflater.from(this);

		chooseLocationIcon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				startActivityForResult(new Intent("fhhgb.travis.philipp.travis_android.SearchLocationActivity"),
						diaryLocationRequestCode);
			}
		});

		addLocationButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				try {
					int PLACE_PICKER_REQUEST = 1;
					PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

					startActivityForResult(builder.build(CreateDiaryActivity.this), PLACE_PICKER_REQUEST);
				} catch (Exception ex) {
					ex.printStackTrace();
					Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
				}
			}
		});

		addTextCardView(inflater);

		addImageCardView();
	}

	private void loadExtras() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			selectedLocationId = extras.getInt(Constants.LOCATION_ID, -1);
		}
	}

	/**
	 *Check the needed permission and show the systems image picker
	 */
	private void addImageCardView() {
		addImageButton.setOnClickListener(new View.OnClickListener() {
			                                  @Override
			                                  public void onClick(View view) {

				                                  // Here, thisActivity is the current activity
				                                  if (ContextCompat.checkSelfPermission(getBaseContext(),
						                                  Manifest.permission.READ_EXTERNAL_STORAGE)
						                                  != PackageManager.PERMISSION_GRANTED) {

					                                  // No explanation needed, we can request the permission.
					                                  ActivityCompat.requestPermissions(CreateDiaryActivity.this,
							                                  new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
							                                  PERMISSION_CODE_READ_EXTERNAL_STORAGE);

					                                  // PERMISSION_CODE_READ_EXTERNAL_STORAGE is an
					                                  // app-defined int constant. The callback method gets the
					                                  // result of the request.
				                                  } else {
					                                  showImagePicker();
				                                  }
			                                  }
		                                  }

		);
	}

	/**
	 * A permission for the external storage is needed, to load an image with the imagepicker.
	 *
	 * @param requestCode
	 * @param permissions
	 * @param grantResults
	 */
	@Override
	public void onRequestPermissionsResult(int requestCode,
	                                       String permissions[], int[] grantResults) {
		switch (requestCode) {
		case PERMISSION_CODE_READ_EXTERNAL_STORAGE: {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0
					&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// permission was granted, yay! Do the
				// contacts-related task you need to do.

				showImagePicker();

			} else {

				// permission denied, boo! Disable the
				// functionality that depends on this permission.
			}
			return;
		}

		// other 'case' lines to check for other
		// permissions this app might request
		}
	}

	/**
	 * Show the System Defaults Imagepicker
	 */
	private void showImagePicker() {
		Intent i = new Intent(
				Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

		startActivityForResult(i, RESULT_LOAD_IMAGE);
	}

	/**
	 * Add a Cardview to the view
	 *
	 * @param inflater
	 */
	private void addTextCardView(final LayoutInflater inflater) {
		addTextButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				addDiaryEntryCount();
				fabMenu.close(true);
				CardView textCardView = (CardView) inflater.inflate(R.layout.cardview_text, cardsContainer, false);

				EditText editText = (EditText) textCardView.findViewById(R.id.diary_text);
				editText.requestFocus();

				cardsContainer.addView(textCardView);
				nestedScrollView.requestLayout();

				//The Cardview uses the toolbar to make the element deletable
				Toolbar toolbar = (Toolbar) textCardView.findViewById(R.id.card_toolbar);
				if (toolbar != null) {
					toolbar.inflateMenu(R.menu.card_text);
					CreateDiaryElement element = new CreateDiaryElement(getIncreasedElementCounter());
					element.setCardview(textCardView);
					diaryCardViews.add(element);
					TextMenuItemClicked textMenuItemClicked = new TextMenuItemClicked(element, CreateDiaryActivity.this);
					toolbar.setOnMenuItemClickListener(textMenuItemClicked);
				}
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		/**
		 Result from the Place Picker
		 */
		if (requestCode == PLACE_PICKER_REQUEST) {
			if (resultCode == RESULT_OK) {
				final Place place = PlacePicker.getPlace(this, data);
				String toastMsg = String.format("Place: %s", place.getName());
				//	Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();

				ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

				imageLoader.loadImage(UrlBuilder.buildMapImageUrl(place.getLatLng()), new SimpleImageLoadingListener() {
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

						Drawable d = new BitmapDrawable(getResources(), loadedImage);
						createCardWithImage(d, new CreateDiaryLocation(getIncreasedElementCounter(), place.getLatLng().latitude, place
								.getLatLng().longitude, 0));
					}
				});
			}
		}

		/**
		 * Image from Systems default Imagepicker selected
		 */
		if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
			Uri selectedImage = data.getData();
			String[] filePathColumn = {MediaStore.Images.Media.DATA};

			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String picturePath = cursor.getString(columnIndex);
			cursor.close();

			//There is a problem with online images, now they are not supported
			if (picturePath == null) {
				Toast.makeText(CreateDiaryActivity.this, "Online images not supported yet", Toast.LENGTH_LONG).show();
				return;
			}

			Bitmap b = BitmapFactory.decodeFile(picturePath);

			b = scaleBitmap(b);

			// String picturePath contains the path of selected Image
			Drawable d = new BitmapDrawable(getResources(), b);

			createCardWithImage(d, new CreateDiaryImage(getIncreasedElementCounter(), picturePath));
		}

		/**
		 *A Location was selectet. This Diary now belongs to a Location
		 */
		if (requestCode == diaryLocationRequestCode) {
			if (data != null) {
				Bundle resultBundle = data.getExtras();
				Snackbar.make(getCurrentFocus(), "Selected " + resultBundle.getString(Constants.LOCATION_NAME), Snackbar.LENGTH_SHORT)
						.setAction("Action", null).show();
				locationName = resultBundle.getString(Constants.LOCATION_NAME);
				selectedLocationId = (resultBundle.getInt(Constants.LOCATION_ID));
			}
		}

	}

	/**
	 * Scale the bitmap if it is bigger than 4095 x 4095
	 *
	 * @param bitmap
	 * @return
	 */
	private Bitmap scaleBitmap(Bitmap bitmap) {
		if (bitmap != null && (bitmap.getWidth() > 4095 || bitmap.getHeight() > 4095)) {
			int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
			Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
			return scaled;
		}
		return bitmap;
	}

	/**
	 * Add a Card to the view. This card shows an image
	 *
	 * @param d
	 * @param createDiaryElement
	 */
	private void createCardWithImage(Drawable d, CreateDiaryElement createDiaryElement) {
		LayoutInflater inflater = LayoutInflater.from(this);
		addDiaryEntryCount();
		fabMenu.close(true);
		CardView imageCardView = (CardView) inflater.inflate(R.layout.cardview_image, cardsContainer, false);

		cardsContainer.addView(imageCardView);
		nestedScrollView.requestLayout();

		ImageView imageView = (ImageView) imageCardView.findViewById(R.id.card_image);
		Toolbar toolbar = (Toolbar) imageCardView.findViewById(R.id.card_toolbar);

		imageView.setImageDrawable(d);
		//imageView.setBackground(d);
		//	imageView.setMinimumHeight(400);
		toolbar.inflateMenu(R.menu.card_text);
		createDiaryElement.setCardview(imageCardView);

		diaryCardViews.add(createDiaryElement);
		TextMenuItemClicked textMenuItemClicked = new TextMenuItemClicked(createDiaryElement, CreateDiaryActivity.this);
		toolbar.setOnMenuItemClickListener(textMenuItemClicked);
	}

	private void setupCollapsingToolbar() {
		final CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
				R.id.collapse_toolbar);

		collapsingToolbar.setTitleEnabled(false);
	}

	private void setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
//		getSupportActionBar().setTitle("");
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_create_diary, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
		}

		if (item.getItemId() == R.id.saveDiaryEntry) {
			dialog = new SpotsDialog(CreateDiaryActivity.this, getString(R.string.loading));
			dialog.show();
			saveDiaryEntry();
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Do the validation stuff and save the basic diary data on the backend.
	 * Then calls a method, that saves all the text, image and mapelements in the backend
	 */
	private void saveDiaryEntry() {

		if (selectedLocationId == -1) {
			Snackbar.make(getCurrentFocus(), "Select a Location first", Snackbar.LENGTH_SHORT)
					.setAction("Action", null).show();
			dialog.dismiss();
		} else if (entryName.getText() == null || entryName.getText().toString().equals("")) {
			Snackbar.make(getCurrentFocus(), "Enter a name for the entry", Snackbar.LENGTH_SHORT)
					.setAction("Action", null).show();
			dialog.dismiss();
		} else if (diaryCardViews.size() <= 0) {
			Snackbar.make(getCurrentFocus(), "Enter at least one Element", Snackbar.LENGTH_SHORT)
					.setAction("Enter at least one Element", null).show();
			dialog.dismiss();
		} else {

			DiaryService diaryService = SessionData.getRestClient().getDiaryService();

			Call<NewDiary> retval = diaryService.createNewDiaryEntry(new CreateDiary(entryName.getText().toString(), selectedLocationId));
			retval.enqueue(new Callback<NewDiary>() {

				@Override
				public void onResponse(Response<NewDiary> response, Retrofit retrofit) {
					newDiary = response.body();
					if (newDiary != null) {
						saveDiaryElements();
					} else {
						Toast.makeText(getBaseContext(), "Diary is null ", Toast.LENGTH_LONG).show();
					}

				}

				@Override
				public void onFailure(Throwable t) {
					Toast.makeText(getBaseContext(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
				}
			});
		}
	}

	/**
	 * Save image, map and text elements at the backend.
	 */
	private void saveDiaryElements() {

		DiaryUploadStatus diaryUpdateStatus = new DiaryUploadStatus(newDiary, locationName);

		//Iterate over all the text, image and map elements of the diary
		for (CreateDiaryElement element : diaryCardViews) {

			//Save Location
			if (element instanceof CreateDiaryLocation) {
				DiaryService diaryService = SessionData.getRestClient().getDiaryService();

				DiaryLocation diaryLocation = new DiaryLocation(element.getSort(), ((CreateDiaryLocation) element).getLatitude(), (
						(CreateDiaryLocation) element)
						.getLongitude());

				Call<Void> retval = diaryService.addLocationElementToDiary(newDiary.getId(), diaryLocation);
				retval.enqueue(new Callback<Void>() {
					@Override
					public void onResponse(Response<Void> response, Retrofit retrofit) {
					}

					@Override
					public void onFailure(Throwable t) {
						Toast.makeText(getBaseContext(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
					}
				});
			}

			//Save Image
			else if (element instanceof CreateDiaryImage) {

				DiaryService diaryService = SessionData.getRestClient().getDiaryService();

				File file = new File(((CreateDiaryImage) element).getImagePath());

				Bitmap b = BitmapFactory.decodeFile(((CreateDiaryImage) element).getImagePath());

				RequestBody sort = RequestBody.create(MediaType.parse("text/plain"), element.getSort() + "");

				boolean isjpg = ((CreateDiaryImage) element).getImagePath().toLowerCase().endsWith(".jpg") || ((CreateDiaryImage) element)
						.getImagePath().toLowerCase().endsWith(".jpeg");

				boolean ispng = ((CreateDiaryImage) element).getImagePath().toLowerCase().endsWith(".png");

				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				if (isjpg) {
					b.compress(Bitmap.CompressFormat.JPEG, 20, stream);
				} else if (ispng) {
					b.compress(Bitmap.CompressFormat.PNG, 20, stream);
				} else {
					Toast.makeText(getBaseContext(), "Unknown Imageformat", Toast.LENGTH_LONG).show();
					return;
				}
				try {

					stream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

				final ProgressRequestBody progressRequestBody = new ProgressRequestBody(file, b, this);

				diaryUpdateStatus.getUploadImages().add(progressRequestBody);

				Call<Void> call = diaryService.addImageElementToDiary(progressRequestBody, sort, newDiary.getId());
				call.enqueue(new Callback<Void>() {
					@Override
					public void onResponse(Response<Void> response, Retrofit retrofit) {
						Log.v("Upload", "success");
						progressRequestBody.getProgressBar().setProgress(100);
						SessionData.decreaseCurrentUploadingCounter();

						SessionData.decreaseCurrentUploadingCounter();
						if (SessionData.getCurrentUploadingCounter() == 0) {
							MainPageActivity.uploadingFinished();
						}
					}

					@Override
					public void onFailure(Throwable t) {
						progressRequestBody.getProgressBar().setProgress(100);
						SessionData.decreaseCurrentUploadingCounter();

						SessionData.decreaseCurrentUploadingCounter();
						if (SessionData.getCurrentUploadingCounter() == 0) {
							MainPageActivity.uploadingFinished();
						}
					}
				});
			}

			//If element was not a location and not an image, it must be a text
			else {
				//Save Text
				DiaryService diaryService = SessionData.getRestClient().getDiaryService();
				DiaryText diaryText = new DiaryText(((EditText) element.getCardview().findViewById(R.id.diary_text)).getText().toString(),
						element
								.getSort());
				Call<Void> retval = diaryService.addTextElementToDiary(newDiary.getId(), diaryText);
				retval.enqueue(new Callback<Void>() {
					@Override
					public void onResponse(Response<Void> response, Retrofit retrofit) {

					}

					@Override
					public void onFailure(Throwable t) {
						Toast.makeText(getBaseContext(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
					}
				});
			}
		}
		dialog.dismiss();
		SessionData.getDiaryUploadStatuses().addFirst(diaryUpdateStatus);
		Intent intent = new Intent(getBaseContext(), MainPageActivity.class);
		startActivity(intent);
		finish();
	}
}
