package fhhgb.travis.philipp.travis_android.rest.model.request;

/**
 * @author Philipp
 *         Created on 24.12.2015.
 */

public class Text {

	private String text;

	public Text(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
