package fhhgb.travis.philipp.travis_android.rest.model.response;

/**
 * Created by Hellmayr on 03.05.2016.
 */
public class Rating {

	int count;
	double average;
	String ratingCategory;

	public Rating(int count, double average, String ratingCategory) {
		this.count = count;
		this.average = average;
		this.ratingCategory = ratingCategory;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}

	public String getRatingCategory() {
		return ratingCategory;
	}

	public void setRatingCategory(String ratingCategory) {
		this.ratingCategory = ratingCategory;
	}
}
