package fhhgb.travis.philipp.travis_android.rest.model.response;

/**
 * @author Philipp
 *         Created on 08.01.2016.
 */

public class NewDiary {

	private int id;
	private String name;
	private String createdTime;
	int userId;
	int placed;

	public NewDiary(int id, String name, String createdTime, int userId, int placed) {
		this.id = id;
		this.name = name;
		this.createdTime = createdTime;
		this.userId = userId;
		this.placed = placed;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPlaced() {
		return placed;
	}

	public void setPlaced(int placed) {
		this.placed = placed;
	}
}
