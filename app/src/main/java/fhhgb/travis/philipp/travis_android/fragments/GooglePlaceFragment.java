package fhhgb.travis.philipp.travis_android.fragments;

import android.support.v4.app.Fragment;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * This is used as an abstract class for the classes that need the GoogleApiClient for the Places Support to receive images of the
 * locations.
 * This type is needed to have a generic datatype to use different Fragments in the adapter for the tripoverview recyclerviews.
 * With this generic datatype the current GoogleApiClient can be created or taken from the class SessionData.
 * Created by Hellmayr on 04.07.2016.
 */
public abstract class GooglePlaceFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener {

}
