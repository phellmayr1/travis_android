package fhhgb.travis.philipp.travis_android.helper;

import com.google.android.gms.maps.model.LatLng;

/**
 * @author Philipp
 *         Created on 06.01.2016.
 */

public class UrlBuilder {

	private static int zoom=18;
	private static int width =450;
	private static int height =650;

	private static String urlStart="https://maps.googleapis.com/maps/api/staticmap?";

	private static String urlParamCenter="center=";
	private static String urlParamZoom="&zoom=";
	private static String urlParamSize="&size=";

	/*
	* Builds an URL to receive an image of the map
	*URL must look like the following
	* https://maps.googleapis.com/maps/api/staticmap?center=40.714728,-73.998672&zoom=14&size=600x300
	 */
	public static String buildMapImageUrl(double longitude, double latitude){

		return urlStart+urlParamCenter+latitude+","+longitude+urlParamZoom+zoom+urlParamSize+ width +"x"+ height;
	}

	public static String buildMapImageUrl(LatLng latLng) {
		return buildMapImageUrl(latLng.longitude, latLng.latitude);
	}
}
