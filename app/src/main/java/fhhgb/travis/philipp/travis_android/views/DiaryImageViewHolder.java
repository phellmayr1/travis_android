package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;

import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;

/**
 * Created by Hellmayr on 13.04.2016.
 */
public class DiaryImageViewHolder extends DiaryViewHolder {

	ImageView diaryImage;
	public DiaryImageViewHolder(View v, Activity activity, User userToShow) {
		super(v, activity, userToShow);
		diaryImage = (ImageView) itemView.findViewById(R.id.card_image);
	}
}
