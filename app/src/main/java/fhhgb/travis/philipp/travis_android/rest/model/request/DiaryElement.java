package fhhgb.travis.philipp.travis_android.rest.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import fhhgb.travis.philipp.travis_android.rest.model.response.DiaryCoordinatesElement;
import fhhgb.travis.philipp.travis_android.rest.model.response.DiaryImageElement;
import fhhgb.travis.philipp.travis_android.rest.model.response.DiaryTextElement;

/**
 * @author Philipp
 *         Created on 11.01.2016.
 */

public class DiaryElement {

	private int id;
	private String name;
	private Date createdTime;

	private boolean liked;

	/*
	* This field is only used at the mainpage,
	* not at the diary fragment at the profile page
	 */
	private int userId;

	private int commentCount;

	private LocationWithCoordinates location;

	@SerializedName("pictureElement")
	private DiaryImageElement diaryImageElement;
	private DiaryTextElement textElement;
	private DiaryCoordinatesElement coordinatesElement;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public int getCommentCount() { return commentCount; }

	public void setCommentCount(int commentCount) { this.commentCount = commentCount; }

	public LocationWithCoordinates getLocation() {
		return location;
	}

	public void setLocation(LocationWithCoordinates location) {
		this.location = location;
	}

	public DiaryImageElement getDiaryImageElement() {
		return diaryImageElement;
	}

	public void setDiaryImageElement(DiaryImageElement diaryImageElement) {
		this.diaryImageElement = diaryImageElement;
	}

	public DiaryTextElement getTextElement() {
		return textElement;
	}

	public void setTextElement(DiaryTextElement textElement) {
		this.textElement = textElement;
	}

	public DiaryCoordinatesElement getCoordinatesElement() {
		return coordinatesElement;
	}

	public void setCoordinatesElement(DiaryCoordinatesElement coordinatesElement) {
		this.coordinatesElement = coordinatesElement;
	}

	public boolean isLiked() {
		return liked;
	}

	public void setLiked(boolean liked) {
		this.liked = liked;
	}
}
