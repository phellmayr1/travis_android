package fhhgb.travis.philipp.travis_android.rest.model.request;

/**
 * @author Philipp
 *         Created on 23.12.2015.
 */

public class SmallUser {

	private String loginType;
	private String accessToken;
	private String firstName;
	private String lastName;
	private String birthday;

	public SmallUser(String loginType, String accessToken, String firstName, String lastName) {
		this.loginType = loginType;
		this.accessToken = accessToken;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public SmallUser(String loginType, String accessToken, String firstName, String lastName, String birthday) {
		this.loginType = loginType;
		this.accessToken = accessToken;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
}
