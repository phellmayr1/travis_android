package fhhgb.travis.philipp.travis_android;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.text.ParseException;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;
import fhhgb.travis.philipp.travis_android.chips.ContactsCompletionView;
import fhhgb.travis.philipp.travis_android.chips.Person;
import fhhgb.travis.philipp.travis_android.helper.ISO8601;
import fhhgb.travis.philipp.travis_android.rest.model.response.SearchResultUser;
import fhhgb.travis.philipp.travis_android.rest.model.response.Trip;
import fhhgb.travis.philipp.travis_android.rest.model.response.TripActivity;
import fhhgb.travis.philipp.travis_android.rest.model.response.TripUser;
import fhhgb.travis.philipp.travis_android.rest.service.SearchService;
import fhhgb.travis.philipp.travis_android.rest.service.TripPlannerService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Add, Modify a Trip
 */
public class NewTripActivity extends AppCompatActivity {

	private static final int SELECT_LOCATION_REQUEST_CODE = 5;
	private static final int RESULT_ACTIVITY_REQUEST_CODE = 2;
	@Bind(R.id.locationTextView)
	TextView locationTextView;

	@Bind(R.id.descriptionTextContent)
	EditText descriptionText;

	@Bind(R.id.informationTextContent)
	EditText informationText;

	@Bind(R.id.informationCount)
	TextView informationCount;

	@Bind(R.id.descriptionCount)
	TextView descriptionCount;

	@Bind(R.id.timeZoneSpinner)
	Spinner timeZoneSpinner;

	@Bind(R.id.fromTextContent)
	TextView fromText;

	@Bind(R.id.toTextContent)
	TextView toText;

	@Bind(R.id.fromTimeTextContent)
	TextView fromTimeText;

	@Bind(R.id.toTimeTextContent)
	TextView toTimeText;

	@Bind(R.id.titleCounter)
	TextView titleCounter;

	@Bind(R.id.title_input)
	TextView titleText;

	@Bind(R.id.activitiesContainer)
	LinearLayout activitiesContainer;

	@Bind(R.id.addActivityTextView)
	TextView addTripActivityTextView;

	@Bind(R.id.costsTextContent)
	EditText costsTextView;

	@Bind(R.id.curencySpinner)
	Spinner currencySpinner;

	@Bind(R.id.accept)
	FloatingActionButton acceptButton;

	@Bind(R.id.deny)
	FloatingActionButton denyButton;

	@Bind(R.id.floating_menu)
	FloatingActionMenu floatingMenu;

	android.app.AlertDialog dialog;

	private int locationId;
	private String locationName;
	private int tripId;

	ContactsCompletionView chipsView;
	Person[] people;
	ArrayAdapter<Person> adapter;

	Trip trip;

	TripActivity currentEditedTripActivity;

	LinkedList<Integer> deletedActivitiesIds;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_trip);
		ButterKnife.bind(this);

		//When the trip is not saved at the backend, the id is -1
		trip = new Trip(-1);
		deletedActivitiesIds = new LinkedList<>();

		setupToolbar();

		loadExtras();

		initTripData();

		dialog = new SpotsDialog(this, getString(R.string.loading));

	}

	/**
	 * Adds all the listeners to the fields. These listeners are needed to modify the data of the trip.
	 */
	private void addActionListenerToFields() {
		informationText.addTextChangedListener(new TextCountSetter(informationCount));
		descriptionText.addTextChangedListener(new TextCountSetter(descriptionCount));
		titleText.addTextChangedListener(new TextCountSetter(titleCounter));

		locationTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				NewTripActivity.this.startActivityForResult(new Intent("fhhgb.travis.philipp.travis_android.SearchLocationActivity"),
						SELECT_LOCATION_REQUEST_CODE);
			}
		});

		fromText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				DatePickerDialog datePickerDialog = new DatePickerDialog(NewTripActivity.this, new DateSetListener(fromText, 1),
						trip.getBegin().get(Calendar.YEAR),
						trip.getBegin().get(Calendar.MONTH), trip.getBegin().get(Calendar.DAY_OF_MONTH));
				datePickerDialog.show();
			}
		});

		toText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				DatePickerDialog datePickerDialog = new DatePickerDialog(NewTripActivity.this, new DateSetListener(toText, 2),
						trip.getEnd().get(Calendar.YEAR),
						trip.getEnd().get(Calendar.MONTH), trip.getEnd().get(Calendar.DAY_OF_MONTH));
				datePickerDialog.show();
			}
		});

		fromTimeText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TimePickerDialog timePickerdialog = new TimePickerDialog(NewTripActivity.this, new TimeSetListener(fromTimeText, 1),
						trip.getBegin().get(Calendar.HOUR_OF_DAY), trip.getBegin().get(Calendar.MINUTE),
						true);
				timePickerdialog.show();
			}
		});

		toTimeText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TimePickerDialog timePickerdialog = new TimePickerDialog(NewTripActivity.this, new TimeSetListener(toTimeText, 2),
						trip.getEnd().get(Calendar.HOUR_OF_DAY), trip.getEnd().get(Calendar.MINUTE),
						true);
				timePickerdialog.show();
			}
		});

		addTripActivityTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(NewTripActivity.this, NewTripactivityActivity.class);
				TripActivity tripActivity = new TripActivity(-1);
				tripActivity.setLocationId(trip.getLocationId());
				tripActivity.setLocationName(locationName);
				intent.putExtra(Constants.IS_ADMIN, trip.isAdmin());
				intent.putExtra(Constants.TRIPACTIVITY, tripActivity);
				NewTripActivity.this.startActivityForResult(intent, RESULT_ACTIVITY_REQUEST_CODE);
			}
		});
	}

	/**
	 * Loads the datasource for the chipview. It basically loads all the persons and adds it as datasource.
	 */
	private void loadPersons() {
		SearchService searchService = SessionData.getRestClient().getSearchService();

		//TODO: Loading all Persons will cause problems with more users
		Call<List<SearchResultUser>> b = searchService.searchUser(" ");
		b.enqueue(new Callback<List<SearchResultUser>>() {

			@Override
			public void onResponse(Response<List<SearchResultUser>> response, Retrofit retrofit) {

				if (response.body() != null && response.body() != null) {
					people = new Person[response.body().size()];

					int i = 0;
					for (SearchResultUser user : response.body()) {

						people[i] = new Person(user.getFirstName() + " " + user.getLastName(), user.getId() + "");
						i++;
					}

					adapter = new ArrayAdapter<>(NewTripActivity.this, android.R.layout.simple_list_item_1, people);

					//The chipsview is used to invite users to a trip
					chipsView = (ContactsCompletionView) findViewById(R.id.peopleChips);
					chipsView.allowDuplicates(false);
					chipsView.allowCollapse(false);
					chipsView.setThreshold(1);
					chipsView.setTokenLimit(10);
					chipsView.setAdapter(adapter);

					if (trip.isAdmin()) {
						chipsView.setFocusableInTouchMode(true);
					}
					loadUIPersons();
				}
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(NewTripActivity.this, "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	private void loadExtras() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			locationId = extras.getInt(Constants.LOCATION_ID, -1);
			locationName = extras.getString(Constants.LOCATION_NAME, "");
			tripId = extras.getInt(Constants.TRIP_ID, -1);
			if (locationId != -1) {
				trip.setLocationId(locationId);
			}
		}
	}

	/**
	 * Load the trip data from the backend if it exists OR load the default data
	 */
	private void initTripData() {
		if (tripId != -1) {
			TripPlannerService tripPlannerService = SessionData.getRestClient().getTripPlannerService();

			Call<Trip> b = tripPlannerService.loadTrip(tripId);
			b.enqueue(new Callback<Trip>() {

				@Override
				public void onResponse(Response<Trip> response, Retrofit retrofit) {
					trip = response.body();

					trip.setAdmin(false);
					for (TripUser tripUser : trip.getUsers()) {
						if (tripUser.getUser().getId() == SessionData.getUserId()) {
							if (tripUser.isAdmin()) {
								trip.setAdmin(true);
							}
							if (tripUser.getStatus().equals(Constants.USER_TRIP_ACCEPTED)) {
								setAcceptButtonSelectedColor();

							} else if (tripUser.getStatus().equals(Constants.USER_TRIP_REJECTED)) {
								setDenyButtonSelectedColor();
							}
						}
					}

					loadPersons();
					loadUIData();
				}

				@Override
				public void onFailure(Throwable t) {
					Toast.makeText(NewTripActivity.this, "FAIL", Toast.LENGTH_LONG).show();
				}
			});
		} else {
			trip.setAdmin(true);
			loadUIData();
			loadPersons();
		}
	}

	/**
	 * Load all the trip data into the UI
	 */
	private void loadUIData() {

		//Depending on the admin status of the user, the trip is editable or not
		if (trip.isAdmin()) {
			addActionListenerToFields();
			titleText.setFocusableInTouchMode(true);
			currencySpinner.setClickable(true);
			timeZoneSpinner.setClickable(true);
			descriptionText.setFocusableInTouchMode(true);
			informationText.setFocusableInTouchMode(true);
			costsTextView.setFocusableInTouchMode(true);
		} else {
			currencySpinner.setEnabled(false);
			currencySpinner.setClickable(false);
			timeZoneSpinner.setEnabled(false);
			timeZoneSpinner.setClickable(false);
			floatingMenu.setVisibility(View.VISIBLE);

			acceptButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					setAcceptButtonSelectedColor();
					acceptTrip();
				}
			});

			denyButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					setDenyButtonSelectedColor();
					denyTrip();
				}
			});
		}

		titleText.setText(trip.getTitle());
		descriptionText.setText(trip.getDescription());
		informationText.setText(trip.getTravelInformation());
		costsTextView.setText(trip.getBudget() + "");

		try {
			if (trip.getBeginDate() != null) {
				trip.setBegin(ISO8601.toCalendar(trip.getBeginDate()));
			}
			if (trip.getEndDate() != null) {
				trip.setEnd(ISO8601.toCalendar(trip.getEndDate()));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		locationTextView.setText(locationName);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, Constants.CURRENCIES);
		currencySpinner.setAdapter(adapter);

		currencySpinner.setSelection(adapter.getPosition(trip.getCurrency()));

		ArrayAdapter<String> timeZoneAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, Constants.TIME_ZONES);
		timeZoneSpinner.setAdapter(timeZoneAdapter);

		timeZoneSpinner.setSelection(timeZoneAdapter.getPosition(trip.getTimezone()));

		fromText.setText(fhhgb.travis.philipp.travis_android.helper.DateUtils.formatDateInts(trip.getBegin().
				get(Calendar.YEAR), trip.getBegin().get(Calendar.MONTH), trip.getBegin().get(Calendar.DAY_OF_MONTH)));

		toText.setText(fhhgb.travis.philipp.travis_android.helper.DateUtils.formatDateInts(trip.getEnd().get(Calendar.YEAR), trip.getEnd().
				get(Calendar.MONTH), trip.getEnd().get(Calendar.DAY_OF_MONTH)));

		fromTimeText.setText(trip.getBegin().get(Calendar.HOUR) + ":" + trip.getBegin().get(Calendar.MINUTE));

		toTimeText.setText(trip.getEnd().get(Calendar.HOUR) + ":" + trip.getEnd().get(Calendar.MINUTE)

		);

		reloadActivitesContainer();
	}

	private void setDenyButtonSelectedColor() {
		acceptButton.setColorNormal(Color.WHITE);
		denyButton.setColorNormal(Color.RED);
	}

	private void setAcceptButtonSelectedColor() {
		acceptButton.setColorNormal(Color.GREEN);
		denyButton.setColorNormal(Color.WHITE);
	}

	/**
	 * Add all the invited users to the chipsView.
	 */
	private void loadUIPersons() {
		for (TripUser user : trip.getUsers()) {
			if (user.getUser().getId() != SessionData.getUserId()) {
				chipsView.addObject(new Person(user.getUser().getFirstName() + " " + user.getUser().getLastName(), user.getUser()
						.getId

								() + ""));
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == SELECT_LOCATION_REQUEST_CODE) {
			if (data != null) {
				Bundle resultBundle = data.getExtras();
				locationId = resultBundle.getInt(Constants.LOCATION_ID);
				locationName = resultBundle.getString(Constants.LOCATION_NAME);
				trip.setLocationId(locationId);
				locationTextView.setText(locationName);
			}
		} else if (requestCode == RESULT_ACTIVITY_REQUEST_CODE) {
			if (data != null) {
				Bundle resultBundle = data.getExtras();

				TripActivity tp = (TripActivity) resultBundle.getSerializable(Constants.TRIPACTIVITY);

				trip.getActivities().remove(currentEditedTripActivity);
				trip.getActivities().add(tp);

				reloadActivitesContainer();
			}
		}

	}

	/**
	 * Shows all the tripActivites in the Container
	 */
	private void reloadActivitesContainer() {

		activitiesContainer.removeAllViews();

		for (TripActivity tripactivity : trip.getActivities()) {
			final LayoutInflater inflater = LayoutInflater.from(this);
			RelativeLayout activityLayout = (RelativeLayout) inflater.inflate(R.layout.relativlayout_tripactivity, activitiesContainer,
					false);
			TextView textView = (TextView) activityLayout.findViewById(R.id.nameTextView);
			textView.setOnClickListener(new TripActivityOnClickListener(tripactivity));
			ImageView imageView = (ImageView) activityLayout.findViewById(R.id.trashImageView);

			if (trip.isAdmin()) {

				imageView.setOnClickListener(new TripActivityOnDeleteClickListener(tripactivity));
			} else {
				imageView.setVisibility(View.GONE);
			}

			textView.setText(tripactivity.getName());
			activitiesContainer.addView(activityLayout);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_create_diary, menu);
		return true;
	}

	private void setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
		}

		//Save the trip
		if (item.getItemId() == R.id.saveDiaryEntry) {

			loadTripFromUI();

			//A trip cannot be saved without a name
			if (trip.getTitle().isEmpty()) {
				new AlertDialog.Builder(this)

						.setTitle(getString(R.string.new_trip))
						.setMessage(getString(R.string.please_enter_the_trips_name))
						.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// continue with delete
							}
						})
						.setIcon(android.R.drawable.ic_dialog_alert)
						.show();
			} else {
				dialog.show();

				//if the id is -1 the trip is not yet saved at the backend and must be created new
				if (trip.getId() == -1) {
					final TripPlannerService tripPlannerService = SessionData.getRestClient().getTripPlannerService();

					Call<Trip> b = tripPlannerService.createTrip(trip);
					b.enqueue(new Callback<Trip>() {

						@Override
						public void onResponse(Response<Trip> response, Retrofit retrofit) {

							trip.setId(response.body().getId());

							if (trip.getActivities() != null) {
								for (TripActivity tripActivity : trip.getActivities()) {
									addActivity(tripActivity);
								}
							}

							for (Person person : chipsView.getObjects()) {
								addUser(Integer.parseInt(person.getId()));
							}

							dialog.dismiss();
							finish();
						}

						@Override
						public void onFailure(Throwable t) {
							Toast.makeText(NewTripActivity.this, "FAIL", Toast.LENGTH_LONG).show();
						}
					});
				}
				//The Trip already exists in the backend and must be modified
				else {
					TripPlannerService tripPlannerService = SessionData.getRestClient().getTripPlannerService();

					Call<Void> b = tripPlannerService.changeTrip(trip.getId(), trip);
					b.enqueue(new Callback<Void>() {

						@Override
						public void onResponse(Response<Void> response, Retrofit retrofit) {

							//Delete the Tripactivities, deleted from the user, on the backend
							for (Integer deletedTripId : deletedActivitiesIds) {
								deleteActivity(deletedTripId);
							}
							for (TripActivity tripActivity : trip.getActivities()) {

								//If the activity is new and not on the server, then add it
								if (tripActivity.getId() == -1) {
									addActivity(tripActivity);
								}
								//If the Tripactivity is not new, modify it on the backend
								else {
									TripPlannerService tripPlannerService = SessionData.getRestClient().getTripPlannerService();

									Call<Void> b = tripPlannerService.changeTripActivityData(tripActivity.getId(), tripActivity);
									b.enqueue(new Callback<Void>() {

										@Override
										public void onResponse(Response<Void> response, Retrofit retrofit) {

										}

										@Override
										public void onFailure(Throwable t) {

										}

									});
								}
							}

							//Add new added users
							for (Person person : chipsView.getObjects()) {

								boolean saved = false;
								for (TripUser tripUser : trip.getUsers()) {
									if (Integer.parseInt(person.getId()) == tripUser.getUser().getId()) {
										saved = true;
									}
								}

								if (!saved) {
									addUser(Integer.parseInt(person.getId()));
								}
							}
							//Delete Users
							for (TripUser tripUser : trip.getUsers()) {

								boolean deleted = false;

								for (Person person : chipsView.getObjects()) {
									if (Integer.parseInt(person.getId()) == tripUser.getUser().getId()) {
										deleted = true;
									}
								}

								if (!deleted) {
									if (tripUser.getUser().getId() != SessionData.getUserId()) {
										removeUser(Integer.parseInt(String.valueOf(tripUser.getUser().getId())));
									}
								}
							}

							dialog.dismiss();
							finish();
						}

						@Override
						public void onFailure(Throwable t) {
							Toast.makeText(NewTripActivity.this, "FAIL", Toast.LENGTH_LONG).show();
						}
					});
				}

			}
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * A invited user can accept a trip
	 */
	private void acceptTrip() {
		TripPlannerService tripPlannerService = SessionData.getRestClient().getTripPlannerService();

		Call<Void> b = tripPlannerService.acceptTrip(trip.getId());
		b.enqueue(new Callback<Void>() {

			@Override
			public void onResponse(Response<Void> response, Retrofit retrofit) {
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(NewTripActivity.this, "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	/**
	 * A denied user can deny the invitation for the trip
	 */
	private void denyTrip() {
		TripPlannerService tripPlannerService = SessionData.getRestClient().getTripPlannerService();

		Call<Void> b = tripPlannerService.denyTrip(trip.getId());
		b.enqueue(new Callback<Void>() {

			@Override
			public void onResponse(Response<Void> response, Retrofit retrofit) {
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(NewTripActivity.this, "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	/**
	 * Save the new tripactivity for the trip on the backend
	 *
	 * @param tripActivity
	 */
	private void addActivity(TripActivity tripActivity) {
		tripActivity.setBeginDate(ISO8601.fromCalendar(tripActivity.getBegin()));
		tripActivity.setEndDate(ISO8601.fromCalendar(tripActivity.getEnd()));

		TripPlannerService tripPlannerService = SessionData.getRestClient().getTripPlannerService();

		Call<Void> b = tripPlannerService.addTripToActivity(trip.getId(), tripActivity);
		b.enqueue(new Callback<Void>() {

			@Override
			public void onResponse(Response<Void> response, Retrofit retrofit) {
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(NewTripActivity.this, "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	/**
	 * Add the user to the trip and save it in the backend. This means invite the user to participate the trip.
	 *
	 * @param userId
	 */
	private void addUser(int userId) {

		TripPlannerService tripPlannerService = SessionData.getRestClient().getTripPlannerService();

		Call<Void> b = tripPlannerService.addUserToTrip(trip.getId(), userId);
		b.enqueue(new Callback<Void>() {

			@Override
			public void onResponse(Response<Void> response, Retrofit retrofit) {
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(NewTripActivity.this, "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	/**
	 * Remove the user from the trip in the backend.
	 *
	 * @param userId
	 */
	private void removeUser(int userId) {

		TripPlannerService tripPlannerService = SessionData.getRestClient().getTripPlannerService();

		Call<Void> b = tripPlannerService.removeUserFromTrip(trip.getId(), userId);
		b.enqueue(new Callback<Void>() {

			@Override
			public void onResponse(Response<Void> response, Retrofit retrofit) {
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(NewTripActivity.this, "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	/**
	 * Delete the activity from the trip in the backend.
	 *
	 * @param tripActivityId
	 */
	private void deleteActivity(int tripActivityId) {
		TripPlannerService tripPlannerService = SessionData.getRestClient().getTripPlannerService();

		Call<Void> b = tripPlannerService.removeTripFromActivity(tripActivityId);
		b.enqueue(new Callback<Void>() {

			@Override
			public void onResponse(Response<Void> response, Retrofit retrofit) {
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(NewTripActivity.this, "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	/**
	 * Set the trip data to the current UI's data
	 */
	private void loadTripFromUI() {

		trip.setTitle(titleText.getText().toString());
		trip.setBudget(Double.parseDouble(costsTextView.getText().toString()));
		trip.setDescription(descriptionText.getText().toString());
		trip.setTravelInformation(informationText.getText().toString());
		trip.setPersons(chipsView.getObjects());
		trip.setCurrency(currencySpinner.getSelectedItem().toString());
		trip.setTimezone(timeZoneSpinner.getSelectedItem().toString());

		//This is needed for the backend communication. The Date must be submitted
		//as ISO8601 String. That's why the string representation of the date is saved
		//in the field BeginDate while the Calender representation (for local purposes)
		//is stored in the field Begin
		trip.setBeginDate(ISO8601.fromCalendar(trip.getBegin()));
		trip.setEndDate(ISO8601.fromCalendar(trip.getEnd()));

	}

	/**
	 * When the number of digits of a editText change, the change will be shown in the UI
	 */
	public class TextCountSetter implements TextWatcher {

		TextView countTextView;

		public TextCountSetter(TextView countTextView) {
			this.countTextView = countTextView;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			countTextView.setText(s.length() + "");
		}

		@Override
		public void afterTextChanged(Editable s) {

		}
	}

	/**
	 * Saves the new date from the UI in the trip
	 */
	class DateSetListener implements DatePickerDialog.OnDateSetListener {

		TextView dateTextView;
		int type;

		public DateSetListener(TextView dateTextView, int type) {
			this.dateTextView = dateTextView;
			this.type = type;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			dateTextView.setText(fhhgb.travis.philipp.travis_android.helper.DateUtils.formatDateInts(year, monthOfYear, dayOfMonth));

			if (type == 1) {
				trip.getBegin().set(year, monthOfYear, dayOfMonth);
			} else {
				trip.getEnd().set(year, monthOfYear, dayOfMonth);

			}
		}
	}

	/**
	 * Saves the new time from the UI in the trip
	 */
	class TimeSetListener implements TimePickerDialog.OnTimeSetListener {

		TextView dateTextView;
		int type;

		public TimeSetListener(TextView dateTextView, int type) {
			this.dateTextView = dateTextView;
			this.type = type;
		}

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			dateTextView.setText(hourOfDay + ":" + minute);

			if (type == 1) {
				trip.getBegin().set(Calendar.HOUR, hourOfDay);
				trip.getBegin().set(Calendar.MINUTE, minute);
			} else {
				trip.getEnd().set(Calendar.HOUR, hourOfDay);
				trip.getEnd().set(Calendar.MINUTE, minute);
			}
		}
	}

	/**
	 *Calls the activity details, when a activity was clicked
	 */
	class TripActivityOnClickListener implements View.OnClickListener {

		TripActivity tripActivity;

		public TripActivityOnClickListener(TripActivity tripActivity) {
			this.tripActivity = tripActivity;
		}

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(NewTripActivity.this, NewTripactivityActivity.class);
			//This must be saved, to know later which activity has been modified
			currentEditedTripActivity = tripActivity;
			intent.putExtra(Constants.TRIPACTIVITY, tripActivity);
			intent.putExtra(Constants.IS_ADMIN, trip.isAdmin());
			NewTripActivity.this.startActivityForResult(intent, RESULT_ACTIVITY_REQUEST_CODE);
		}
	}

	/**
	 * When a activity of the trip gets deleted, it will be added to the deleted List
	 * This list will be used afterwards to delete the activites from the backend
	 */
	class TripActivityOnDeleteClickListener implements View.OnClickListener {

		TripActivity tripActivity;

		public TripActivityOnDeleteClickListener(TripActivity tripActivity) {
			this.tripActivity = tripActivity;
		}

		@Override
		public void onClick(View v) {
			if (tripActivity.getId() != -1) {
				deletedActivitiesIds.add(tripActivity.getId());
			}
			trip.getActivities().remove(tripActivity);
			reloadActivitesContainer();
		}
	}

}
