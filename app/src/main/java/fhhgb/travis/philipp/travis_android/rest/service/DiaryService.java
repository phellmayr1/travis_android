package fhhgb.travis.philipp.travis_android.rest.service;

import com.squareup.okhttp.RequestBody;

import java.util.List;

import fhhgb.travis.philipp.travis_android.rest.model.request.CommentText;
import fhhgb.travis.philipp.travis_android.rest.model.request.CreateDiary;
import fhhgb.travis.philipp.travis_android.rest.model.request.DiaryElement;
import fhhgb.travis.philipp.travis_android.rest.model.request.DiaryLocation;
import fhhgb.travis.philipp.travis_android.rest.model.request.DiaryText;
import fhhgb.travis.philipp.travis_android.rest.model.response.DiaryElements;
import fhhgb.travis.philipp.travis_android.rest.model.response.NewDiary;
import fhhgb.travis.philipp.travis_android.rest.model.response.UserComment;
import fhhgb.travis.philipp.travis_android.views.ProgressRequestBody;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * @author Philipp
 *         Created on 08.01.2016.
 */

public interface DiaryService {

	@Headers("Content-Type: application/json")
	@POST("diary")
	public Call<NewDiary> createNewDiaryEntry(@Body CreateDiary newDiary);

	@Headers("Content-Type: application/json")
	@POST("diary/textElement/{diaryEntryId}")
	public Call<Void> addTextElementToDiary(@Path("diaryEntryId") int diaryEntryId, @Body DiaryText diaryText);

	@Multipart
	@POST("diary/pictureElement/{diaryEntryId}")
//	@Headers("Content-Type:multipart/form-data")
	public Call<Void> addImageElementToDiary(@Part("picture\"; filename=\"image\" ") ProgressRequestBody picture,
	                                         @Part("sort") RequestBody sort, @Path("diaryEntryId") int diaryEntryId);

	@Headers("Content-Type: application/json")
	@POST("/diary/coordinatesElement/{diaryEntryId}")
	public Call<Void> addLocationElementToDiary(@Path("diaryEntryId") int diaryEntryId, @Body DiaryLocation diaryLocation);

	@Headers("Content-Type: application/json")
	@GET("user/{userid}/diary")
	public Call<List<DiaryElement>> findDiaryEntries(@Path("userid") int userId, @Query("page") int page, @Query("pageSize") int pageSize);

	@Headers("Content-Type: application/json")
	@GET("diary/elements/{diaryEntryId}")
	public Call<DiaryElements> findDiaryEntryElements(@Path("diaryEntryId") int diaryEntryId);

	@Headers("Content-Type: application/json")
	@POST("diary/{diaryEntryId}/like")
	public Call<Void> likeDiaryEntry(@Path("diaryEntryId") int diaryEntryId);

	@Headers("Content-Type: application/json")
	@DELETE("diary/{diaryEntryId}/like")
	public Call<Void> disslikeDiaryEntry(@Path("diaryEntryId") int diaryEntryId);

	@Headers("Content-Type: application/json")
	@GET("diary/comment/{diaryEntryId}")
	public Call<List<UserComment>> findCommentsForDiaryEntry(@Path("diaryEntryId") int diaryEntryId,@Query("page") int page, @Query("pageSize") int pageSize, @Query("pImgWidth") int pImgWidth, @Query("pImgHeight") int pImgHeight);

	@Headers("Content-Type: application/json")
	@POST("/diary/comment/{diaryEntryId}")
	public Call<Void> addCommentToDiary(@Path("diaryEntryId") int diaryEntryId, @Body CommentText text);
}
