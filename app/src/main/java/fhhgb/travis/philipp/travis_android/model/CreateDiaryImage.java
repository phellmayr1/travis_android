package fhhgb.travis.philipp.travis_android.model;

/**
 * @author Philipp
 *         Created on 08.01.2016.
 */

public class CreateDiaryImage extends CreateDiaryElement {

	String imagePath;

	public CreateDiaryImage(int sort, String imPath) {
		super(sort);
		imagePath = imPath;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
}
