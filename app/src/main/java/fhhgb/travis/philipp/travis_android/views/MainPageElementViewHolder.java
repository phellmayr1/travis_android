package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import fhhgb.travis.philipp.travis_android.CommentActivity;
import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.ProfileActivity;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.SessionData;
import fhhgb.travis.philipp.travis_android.ShowDiaryElementsActivity;
import fhhgb.travis.philipp.travis_android.rest.service.DiaryService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * @author Philipp
 *         Created on 26.01.2016.
 */

public class MainPageElementViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

	public RoundedImageView profileImage;
	public TextView nameTextView;
	public TextView timeTextView;

	public TextView diaryName;
	public TextView diaryLocation;

	public FrameLayout imageFrame;
	public RelativeLayout entryHeader;

	public TextView likeIcon;
	public TextView shareIcon;
	public TextView commentCount;
	public TextView commentIcon;

	public RoundedImageView commentProfileImage;
	public TextView commentProfileName;
	public TextView commentText;

	public Activity currentActivity;

	private String username;
	private int diaryId;
	boolean isLiked = false;
	private String diaryTitle;
	private int userId;

	public boolean isLiked() {
		return isLiked;
	}

	public void setIsLiked(boolean isLiked) {
		this.isLiked = isLiked;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getDiaryTitle() {
		return diaryTitle;
	}

	public void setDiaryTitle(String diaryTitle) {
		this.diaryTitle = diaryTitle;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getDiaryId() {
		return diaryId;
	}

	public void setDiaryId(int diaryId) {
		this.diaryId = diaryId;
	}


	public MainPageElementViewHolder(View itemView, Activity activity) {
		super(itemView);
		currentActivity = activity;
//		type = viewType;

		/*
		* Profile Information is included in every Entry
		 */

		profileImage = (RoundedImageView) itemView.findViewById(R.id.profileimage);
		nameTextView = (TextView) itemView.findViewById(R.id.nameTextView);
		timeTextView = (TextView) itemView.findViewById(R.id.timeTextView);

		diaryName = (TextView) itemView.findViewById(R.id.diaryName);
		diaryLocation = (TextView) itemView.findViewById(R.id.diaryLocation);
		imageFrame = (FrameLayout) itemView.findViewById(R.id.imageFrame);
		entryHeader = (RelativeLayout) itemView.findViewById(R.id.entryHeader);

		/*
		* The footer menu icons are included in every entry
		 */

		likeIcon = (TextView) itemView.findViewById(R.id.like);
		shareIcon = (TextView) itemView.findViewById(R.id.share);
		commentCount = (TextView) itemView.findViewById(R.id.commentCount);
		commentIcon = (TextView) itemView.findViewById(R.id.commentIcon);

		/*
		* Comment block
		 */

		//commentProfileImage = (RoundedImageView) itemView.findViewById(R.id.commentProfileImage);
		//commentProfileName  = (TextView) itemView.findViewById(R.id.commentProfileName);
		//commentText = (TextView) itemView.findViewById(R.id.commentText);

		Typeface material = Typeface.createFromAsset(currentActivity.getAssets(), "material.ttf");
		likeIcon.setTypeface(material);
		shareIcon.setTypeface(material);
		commentIcon.setTypeface(material);

		shareIcon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent share = new Intent(Intent.ACTION_SEND);
				share.setType("text/plain"); // might be text, sound, whatever
				share.putExtra(Intent.EXTRA_TEXT, "I created a new diary entry for" + diaryLocation.getText() + ". Check it out now with" +
						" " +
						"Travis!");
				currentActivity.startActivity(Intent.createChooser(share, "share"));
			}
		});

		likeIcon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (!isLiked) {
					DiaryService diaryService = SessionData.getRestClient().getDiaryService();
					likeIcon.setTextColor(currentActivity.getResources().getColor(R.color.travis_primary));

					Call<Void> retval = diaryService.likeDiaryEntry(diaryId);
					retval.enqueue(new Callback<Void>() {
						@Override
						public void onResponse(Response<Void> response, Retrofit retrofit) {
						}

						@Override
						public void onFailure(Throwable t) {
							Toast.makeText(currentActivity, "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
						}
					});
				} else {
					DiaryService diaryService = SessionData.getRestClient().getDiaryService();
					likeIcon.setTextColor(Color.parseColor("#89000000"));

					Call<Void> retval = diaryService.disslikeDiaryEntry(diaryId);
					retval.enqueue(new Callback<Void>() {
						@Override
						public void onResponse(Response<Void> response, Retrofit retrofit) {
						}

						@Override
						public void onFailure(Throwable t) {
							Toast.makeText(currentActivity, "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
						}
					});
				}
				isLiked = !isLiked;

			}

		});

		imageFrame.setOnClickListener(this);

		entryHeader.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(currentActivity, ProfileActivity.class);
				intent.putExtra(Constants.USER_ID_TO_SHOW, userId);
				currentActivity.startActivity(intent);
			}
		});

		commentIcon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(currentActivity, CommentActivity.class);
				intent.putExtra(Constants.DIARY_ID, diaryId);
				currentActivity.startActivity(intent);
			}
		});
	}

	@Override
	public void onClick(View view) {
		Intent intent = new Intent(currentActivity, ShowDiaryElementsActivity.class);
		intent.putExtra(Constants.CURRENT_PROFILE_USERNAME, username);
		intent.putExtra(Constants.DIARY_TITLE, diaryTitle);
		intent.putExtra(Constants.DIARY_ID, diaryId);
		currentActivity.startActivity(intent);
	}
}
