package fhhgb.travis.philipp.travis_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.LinkedList;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.rest.model.request.LocationWithCoordinates;
import fhhgb.travis.philipp.travis_android.rest.model.response.Location;
import fhhgb.travis.philipp.travis_android.rest.model.response.SearchResultLocationList;
import fhhgb.travis.philipp.travis_android.rest.service.LocationService;
import fhhgb.travis.philipp.travis_android.views.SearchLocationAdapter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Search for a Location in the Backend.
 * When no Location is found, call the Google Place Picker to add a new Location to the Backend
 */
public class SearchLocationActivity extends AppCompatActivity {

	@Bind(R.id.searchInput)
	EditText searchInput;

	@Bind(R.id.search_results_listview)
	ListView searchResultsListView;

	SearchLocationAdapter arrayAdapter = null;

	int PLACE_PICKER_REQUEST = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_location);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		ButterKnife.bind(this);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		searchInput.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

				LocationService locationService = SessionData.getRestClient().getLocationService();

				//Search for locations according to the changed text
				Call<SearchResultLocationList> b = locationService.searchLocation(searchInput.getText().toString());
				b.enqueue(new Callback<SearchResultLocationList>() {

					@Override
					public void onResponse(Response<SearchResultLocationList> response, Retrofit retrofit) {

						//Location found in the backend
						if (response.body() != null && response.body().getSearchResults() != null && response.body().getSearchResults()
								.size() != 0) {

							arrayAdapter = new SearchLocationAdapter(SearchLocationActivity.this, R.layout.location_listitem,
									response.body().getSearchResults());

							searchResultsListView.setAdapter(arrayAdapter);

							searchResultsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
								@Override
								public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

									if (id != -1) {
										Location selectedFromList = (Location) (searchResultsListView.getItemAtPosition
												(position));
										Intent data = new Intent();
										//TODO remove the above 2 putextra, cause the whole object should be used
										data.putExtra(Constants.LOCATION_NAME, selectedFromList.getName());
										data.putExtra(Constants.LOCATION_ID, selectedFromList.getId());
										data.putExtra(Constants.FOUND_LOCATION, selectedFromList);
										setResult(RESULT_OK, data);
										finish();
									}

								}
							});
						}
						//No Location found in the backend
						//A new one can be created by using the Google Place Picker
						else {

							LinkedList<Location> mockNewLoc = new LinkedList<Location>();
							Location l = new Location();
							l.setName("select new Location...");
							mockNewLoc.add(l);

							arrayAdapter = new SearchLocationAdapter(SearchLocationActivity.this, R.layout.location_listitem,
									mockNewLoc);

							searchResultsListView.setAdapter(arrayAdapter);

							searchResultsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
								@Override
								public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
									try {

										PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

										startActivityForResult(builder.build(SearchLocationActivity.this), PLACE_PICKER_REQUEST);
									} catch (Exception ex) {
										ex.printStackTrace();
										Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
									}
								}

							});
						}
					}

					@Override
					public void onFailure(Throwable t) {
						Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
					}
				});

			}

			@Override
			public void afterTextChanged(Editable editable) {

			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		final String googlePlaceId;

		//Location selected from the placepicker
		if (requestCode == PLACE_PICKER_REQUEST) {
			if (resultCode == RESULT_OK) {
				final Place place = PlacePicker.getPlace(this, data);

				LocationService locationService = SessionData.getRestClient().getLocationService();

				googlePlaceId=place.getId();

				//Save the selected Location in the backend
				Call<Location> retval = locationService.createLocation(new LocationWithCoordinates(place.getName().toString(), place
						.getLatLng()
						.latitude, place.getLatLng().longitude,googlePlaceId));

				retval.enqueue(new Callback<Location>() {

					@Override
					public void onResponse(Response<Location> response, Retrofit retrofit) {
						//Toast.makeText(getContext(), "RESP " + response.body().getId(), Toast.LENGTH_LONG).show();
						Intent data = new Intent();
						data.putExtra(Constants.LOCATION_NAME, place.getName());
						data.putExtra(Constants.LOCATION_ID, response.body().getId());
						data.putExtra(Constants.FOUND_LOCATION, response.body());
						response.body().setGooglePlaceId(googlePlaceId);
						setResult(RESULT_OK, data);
						finish();
					}

					@Override
					public void onFailure(Throwable t) {
						Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
					}
				});

			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
		}
		return super.onOptionsItemSelected(item);
	}

}
