package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.SessionData;
import fhhgb.travis.philipp.travis_android.async.AttributedPhoto;
import fhhgb.travis.philipp.travis_android.async.LoadGooglePlacesPhotoTask;
import fhhgb.travis.philipp.travis_android.profile.PlacesFragment;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;
import fhhgb.travis.philipp.travis_android.rest.model.response.VisitedLocationElement;

/**
 * @author Philipp
 *         Created on 11.01.2016.
 */

public class LocationAdapter extends RecyclerView.Adapter<LocationViewHolder> {

	private List<VisitedLocationElement> diaryList;

	private Activity currentActivity;
	private User userToShow;

	PlacesFragment pf;

	public LocationAdapter(List<VisitedLocationElement> diaryList, Activity currentActivity, User userToShow,
	                       PlacesFragment placesFragment) {
		this.diaryList = diaryList;
		this.currentActivity = currentActivity;
		this.userToShow = userToShow;
		pf=placesFragment;
	}

	@Override
	public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.
				from(parent.getContext()).
				inflate(R.layout.cardview_preview_location, parent, false);
		return new LocationViewHolder(itemView, currentActivity, userToShow);
	}

	@Override
	public void onBindViewHolder(final LocationViewHolder holder, int position) {
		VisitedLocationElement location = diaryList.get(position);

		Display display = currentActivity.getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;

		String placeId;

		final ImageView mImageView = holder.imgIcon;
		mImageView.setImageResource(R.drawable.profile_background);

		if (location.getGooglePlaceId() != null) {
			//	placeId = "ChIJrTLr-GyuEmsRBfy61i59si0"; // Australian Cruise Group
			//placeId = "ChIJdd4hrwug2EcRmSrV3Vo6llI"; // London
			//} else {
			placeId = location.getGooglePlaceId();

			//Set the size of a location Element depending on the screen size
			int imgSize = ((size.x - 6) / 2);
			holder.imgIcon.setMaxHeight(imgSize);
			holder.imgIcon.getLayoutParams().height = imgSize;
			holder.imgIcon.setMaxWidth(imgSize);
			holder.imgIcon.setMinimumHeight(imgSize);
			holder.imgIcon.setMinimumWidth(imgSize);

			// Create a new AsyncTask that displays the bitmap and attribution once loaded.
			new LoadGooglePlacesPhotoTask(imgSize, imgSize, SessionData.getmGoogleApiClient(pf)) {
				@Override
				protected void onPreExecute() {
					// Display a temporary image to show while bitmap is loading.
					mImageView.setImageResource(R.drawable.profile_background);
				}

				@Override
				protected void onPostExecute(AttributedPhoto attributedPhoto) {
					if (attributedPhoto != null) {
						// Photo has been loaded, display it.
						mImageView.setImageBitmap(attributedPhoto.bitmap);
					}
				}
			}.execute(placeId);

			holder.setGooglePlaceId(location.getGooglePlaceId());
		}

		holder.placeName.setText(location.getName());
		holder.setLocationId(location.getId());
	}

	@Override
	public int getItemCount() {
		if (diaryList != null) {
			return diaryList.size();
		} else {
			return 0;
		}
	}
}
