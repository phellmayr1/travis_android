package fhhgb.travis.philipp.travis_android.rest.model.request;

/**
 * Created by Mane on 15.05.16.
 */
public class CommentText {

    private String text;

    public CommentText() {}

    public CommentText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
