package fhhgb.travis.philipp.travis_android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.model.DiaryUploadStatus;
import fhhgb.travis.philipp.travis_android.views.ProgressRequestBody;

/**
 * Show image upload details with a progressbar for each image to upload
 */
public class UploadStatusActivity extends AppCompatActivity {

	@Bind(R.id.diaryContainer)
	LinearLayout diaryContainer;

	private void setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setTitle(getString(R.string.diary_image_uploads));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload_status);
		ButterKnife.bind(this);

		setupToolbar();

		diaryContainer.removeAllViews();

		final LayoutInflater inflater = LayoutInflater.from(this);

		//Dynamically build the view with the progressbars for the images
		for (DiaryUploadStatus diaryUploadStatus : SessionData.getDiaryUploadStatuses()) {

			CardView diaryCard = (CardView) inflater.inflate(R.layout.cardview_diary_upload, diaryContainer, false);

			TextView diaryTitle = (TextView) diaryCard.findViewById(R.id.diaryTitle);
			diaryTitle.setText(diaryUploadStatus.getNewDiary().getName());

			TextView diaryLocation = (TextView) diaryCard.findViewById(R.id.diaryLocation);
			diaryLocation.setText(diaryUploadStatus.getLocationName());

			LinearLayout progressContainer = (LinearLayout) diaryCard.findViewById(R.id.progressContainer);
			progressContainer.removeAllViews();

			boolean uploadingFinished = true;

			for (ProgressRequestBody progressRequestBody : diaryUploadStatus.getUploadImages()) {

				if (progressRequestBody.getProgressElement().getParent() != null) {
					((ViewGroup) progressRequestBody.getProgressElement().getParent()).removeView(progressRequestBody.getProgressElement
							());
				}

				progressContainer.addView(progressRequestBody.getProgressElement());

				if (progressRequestBody.getProgressBar().getProgress() < 100) {
					uploadingFinished = false;
				}

			}
			//	if (!uploadingFinished) {
			diaryContainer.addView(diaryCard);
			//	} else {
			//TODO: remove not needed objects from list. Not removable while iterating throug the list!
			//SessionData.getDiaryUploadStatuses().remove(diaryUploadStatus);
			//	}
		}

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		diaryContainer.removeAllViews();
	}
}
