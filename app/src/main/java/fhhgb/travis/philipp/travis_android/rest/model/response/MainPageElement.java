package fhhgb.travis.philipp.travis_android.rest.model.response;

import com.google.gson.annotations.SerializedName;

import fhhgb.travis.philipp.travis_android.rest.model.request.DiaryElement;

/**
 * @author Philipp
 *         Created on 26.01.2016.
 */

public class MainPageElement {

	private User user;

	@SerializedName("diaryEntry")
	private DiaryElement diaryElement;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public DiaryElement getDiaryElement() {
		return diaryElement;
	}

	public void setDiaryElement(DiaryElement diaryElement) {
		this.diaryElement = diaryElement;
	}
}
