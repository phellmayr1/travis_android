package fhhgb.travis.philipp.travis_android.views;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;

import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.SessionData;
import fhhgb.travis.philipp.travis_android.async.AttributedPhoto;
import fhhgb.travis.philipp.travis_android.async.LoadGooglePlacesPhotoTask;
import fhhgb.travis.philipp.travis_android.fragments.GooglePlaceFragment;
import fhhgb.travis.philipp.travis_android.helper.DateUtils;
import fhhgb.travis.philipp.travis_android.rest.model.response.FoundTrip;

/**
 * Created by Hellmayr on 06.06.2016.
 */
public class FoundTripsAdapter extends RecyclerView.Adapter<FoundTripViewHolder> {

	LinkedList<FoundTrip> foundTrips;
	Activity currentActivity;

	GooglePlaceFragment tripCreatedFragment;

	public FoundTripsAdapter(LinkedList<FoundTrip> foundTrips, Activity currActivity, GooglePlaceFragment tripCreatedFragment) {
		this.foundTrips = foundTrips;
		this.currentActivity = currActivity;
		this.tripCreatedFragment = tripCreatedFragment;
	}

	@Override
	public FoundTripViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.
				from(parent.getContext()).
				inflate(R.layout.cardview_found_trip, parent, false);
		return new FoundTripViewHolder(itemView, currentActivity);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	@Override
	public void onBindViewHolder(final FoundTripViewHolder holder, int position) {
		FoundTrip currentElement = foundTrips.get(position);

		holder.setTripId(currentElement.getId());
		holder.setLocationId(currentElement.getLocation().getId());
		holder.setLocationName(currentElement.getLocation().getName());
		holder.nameTextView.setText(currentElement.getLocation().getName());
		holder.tripNameTextView.setText(currentElement.getTitle());
		holder.tripTimeTextView.setText(DateUtils.formatDate(currentElement.getBeginDate()) + " - " + DateUtils.formatDate(currentElement
				.getEndDate()));

		if (currentElement.getStatus().equals(Constants.USER_TRIP_ACCEPTED)) {
			holder.statusImage.setImageDrawable(currentActivity.getDrawable(R.drawable.check));
		} else if (currentElement.getStatus().equals(Constants.USER_TRIP_REJECTED)) {
			holder.statusImage.setImageDrawable(currentActivity.getDrawable(R.drawable.ic_close_white_24dp));

		}

		Display display = currentActivity.getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;

		// Create a new AsyncTask that displays the bitmap and attribution once loaded.
		new LoadGooglePlacesPhotoTask(width, width, SessionData.getmGoogleApiClient(tripCreatedFragment)) {
			@Override
			protected void onPreExecute() {
				// Display a temporary image to show while bitmap is loading.
				//mImageView.setImageResource(R.drawable.profile_background);
			}

			@Override
			protected void onPostExecute(AttributedPhoto attributedPhoto) {
				if (attributedPhoto != null) {
					// Photo has been loaded, display it.
					holder.cityImageView.setImageBitmap(attributedPhoto.bitmap);
				}
			}
		}.execute(currentElement.getLocation().getGooglePlaceId());

	}

	@Override
	public int getItemCount() {
		return foundTrips.size();
	}
}
