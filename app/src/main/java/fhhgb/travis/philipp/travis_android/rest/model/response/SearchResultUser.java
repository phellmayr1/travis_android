package fhhgb.travis.philipp.travis_android.rest.model.response;

/**
 * @author Philipp
 *         Created on 30.12.2015.
 */

public class SearchResultUser {

	private int id;
	private String firstName;
	private String lastName;
	private String birthday;
	private String loginImageUrl;

	public SearchResultUser(int id, String firstName, String lastName, String birthday) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
	}

	public String getLoginImageUrl() {
		return loginImageUrl;
	}

	public void setLoginImageUrl(String loginImageUrl) {
		this.loginImageUrl = loginImageUrl;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
}
