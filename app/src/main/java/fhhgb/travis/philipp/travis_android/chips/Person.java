package fhhgb.travis.philipp.travis_android.chips;

import java.io.Serializable;

public class Person implements Serializable {

	private String name;
	private String id;

	public Person(String name, String id) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		return this.id ==((Person)o).getId();
	}
}