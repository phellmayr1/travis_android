package fhhgb.travis.philipp.travis_android.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.SessionData;
import fhhgb.travis.philipp.travis_android.LocationDetailsActivity;
import fhhgb.travis.philipp.travis_android.rest.model.response.Location;
import fhhgb.travis.philipp.travis_android.rest.service.SearchService;
import fhhgb.travis.philipp.travis_android.views.SearchPlaceAdapter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 *Search for a Location on the backend
 */
public class SearchPlaceFragment extends Fragment {

	@Bind(R.id.search_results_listview)
	ListView searchResultsListView;

	SearchPlaceAdapter arrayAdapter = null;

	public SearchPlaceFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_search_place, container, false);
		ButterKnife.bind(this, view);

		return view;
	}

	public void loadPlaces(String searchText) {
		SearchService searchService = SessionData.getRestClient().getSearchService();

		if (searchText.equals("")) {
			searchText = " ";
		}
		Call<List<Location>> b = searchService.searchPlace(searchText);
		b.enqueue(new Callback<List<Location>>() {

			@Override
			public void onResponse(Response<List<Location>> response, Retrofit retrofit) {

				if (response.body() != null && response.body() != null) {

					arrayAdapter = new SearchPlaceAdapter(getActivity(), R.layout.user_listitem,
							response.body());

					searchResultsListView.setAdapter(arrayAdapter);

					searchResultsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

							if (id != -1) {
								Location selectedFromList = (Location) (searchResultsListView.getItemAtPosition
										(position));

								Intent intent = new Intent(getActivity(), LocationDetailsActivity.class);
								intent.putExtra(Constants.LOCATION_ID, selectedFromList.getId());
								startActivity(intent);
							}
						}
					});
				}
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getActivity(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p/>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener {

		// TODO: Update argument type and name
		void onFragmentInteraction(Uri uri);
	}
}
