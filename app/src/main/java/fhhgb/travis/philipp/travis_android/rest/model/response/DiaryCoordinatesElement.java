package fhhgb.travis.philipp.travis_android.rest.model.response;

import java.io.Serializable;

/**
 * @author Philipp
 *         Created on 11.01.2016.
 */

public class DiaryCoordinatesElement implements Serializable {

	private int id;
	private int sort;
	private DiaryCoordinatesElementDetails coordinates;

	public DiaryCoordinatesElement(int id, int sort, DiaryCoordinatesElementDetails coordinates) {
		this.id = id;
		this.sort = sort;
		this.coordinates = coordinates;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public DiaryCoordinatesElementDetails getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(DiaryCoordinatesElementDetails coordinates) {
		this.coordinates = coordinates;
	}
}
