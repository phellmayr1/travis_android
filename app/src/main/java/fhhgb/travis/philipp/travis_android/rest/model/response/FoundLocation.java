package fhhgb.travis.philipp.travis_android.rest.model.response;

/**
 * Created by Hellmayr on 29.05.2016.
 */
public class FoundLocation {

	Location location;
	double match;

	public FoundLocation(Location location, double match) {
		this.location = location;
		this.match = match;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public double getMatch() {
		return match;
	}

	public void setMatch(double match) {
		this.match = match;
	}
}
