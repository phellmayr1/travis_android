package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.LocationDetailsActivity;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;

/**
 * @author Philipp
 *         Created on 26.01.2016.
 */

public class LocationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

	protected ImageView imgIcon;
	protected TextView placeName;

	private String googlePlaceId;

	User userToShow;

	private int locationId;

	public String getGooglePlaceId() {
		return googlePlaceId;
	}

	public void setGooglePlaceId(String googlePlaceId) {
		this.googlePlaceId = googlePlaceId;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	protected Activity currentActivity;

	public LocationViewHolder(View v, Activity activity, User userToShow) {
		super(v);

		imgIcon = (ImageView) v.findViewById(R.id.card_image);
		placeName = (TextView) v.findViewById(R.id.locationName);
		currentActivity = activity;
		this.userToShow = userToShow;
		v.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		Intent intent = new Intent(currentActivity, LocationDetailsActivity.class);
		intent.putExtra(Constants.LOCATION_ID, locationId);
		currentActivity.startActivity(intent);
	}
}