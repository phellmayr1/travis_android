package fhhgb.travis.philipp.travis_android;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.fragments.SearchPlaceFragment;
import fhhgb.travis.philipp.travis_android.fragments.SearchUserFragment;

/**
 * Search for Users or Locations
 */
public class SearchActivity extends AppCompatActivity {

	@Bind(R.id.pager)
	ViewPager viewPager;

	@Bind(R.id.tabs)
	TabLayout tabLayout;

	@Bind(R.id.searchView)
	SearchView searchView;

	SearchUserFragment searchUserFragment;
	SearchPlaceFragment searchPlaceFragment;


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		onBackPressed();
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		ButterKnife.bind(this);

		searchView.setIconifiedByDefault(false);

		//There is one fragment for the user search and one for the location search
		searchUserFragment = new SearchUserFragment();
		searchPlaceFragment = new SearchPlaceFragment();

		setupViewPager();

		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		tabLayout.setupWithViewPager(viewPager);

		//start the initial search
		searchUserFragment.loadUsers("");
		searchPlaceFragment.loadPlaces("");

		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				searchUserFragment.loadUsers(newText);
				searchPlaceFragment.loadPlaces(newText);
				return false;
			}
		});

	}

	private void setupViewPager() {

		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

		adapter.addFragment(searchUserFragment, getString(R.string.user));
		adapter.addFragment(searchPlaceFragment, getString(R.string.places));
		viewPager.setAdapter(adapter);
	}

	class ViewPagerAdapter extends FragmentPagerAdapter {

		private final List<Fragment> mFragmentList = new ArrayList<>();
		private final List<String> mFragmentTitleList = new ArrayList<>();

		public ViewPagerAdapter(FragmentManager manager) {
			super(manager);
		}

		@Override
		public Fragment getItem(int position) {
			return mFragmentList.get(position);
		}

		@Override
		public int getCount() {
			return mFragmentList.size();
		}

		public void addFragment(Fragment fragment, String title) {
			mFragmentList.add(fragment);
			mFragmentTitleList.add(title);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return mFragmentTitleList.get(position);
		}
	}

}
