package fhhgb.travis.philipp.travis_android.rest.model.request;

/**
 * @author Philipp
 *         Created on 30.12.2015.
 */

public class SearchText {

	public String searchText;

	public SearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
}
