package fhhgb.travis.philipp.travis_android.rest.model.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * @author Philipp
 *         Created on 12.01.2016.
 */

public class DiaryElements implements Serializable {

	@SerializedName("pictureElements")
	LinkedList<DiaryImageElement> imageElements;

	LinkedList<DiaryTextElement> textElements;

	LinkedList<DiaryCoordinatesElement> coordinatesElements;

	public LinkedList<DiaryImageElement> getImageElements() {
		return imageElements;
	}

	public void setImageElements(LinkedList<DiaryImageElement> imageElements) {
		this.imageElements = imageElements;
	}
	public LinkedList<DiaryTextElement> getTextElements() {
		return textElements;
	}

	public void setTextElements(LinkedList<DiaryTextElement> textElements) {
		this.textElements = textElements;
	}

	public LinkedList<DiaryCoordinatesElement> getCoordinatesElements() {
		return coordinatesElements;
	}

	public void setCoordinatesElements(LinkedList<DiaryCoordinatesElement> coordinatesElements) {
		this.coordinatesElements = coordinatesElements;
	}
}
