package fhhgb.travis.philipp.travis_android.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.SessionData;
import fhhgb.travis.philipp.travis_android.rest.model.response.Location;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;
import fhhgb.travis.philipp.travis_android.rest.model.response.VisitedLocation;
import fhhgb.travis.philipp.travis_android.rest.model.response.VisitedLocationElement;
import fhhgb.travis.philipp.travis_android.rest.service.LocationService;
import fhhgb.travis.philipp.travis_android.views.LocationAdapter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlacesFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener {

	public PlacesFragment() {
		// Required empty public constructor
	}


	LocationAdapter locationAdapter;
	User userToShow;
	RecyclerView recList;

	@Bind(R.id.add_location_actionbutton)
	FloatingActionButton adLocationButton;

	int visitedLocationRequestCode = 1;

	LinkedList<VisitedLocationElement> locations;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_places, container, false);
		ButterKnife.bind(this, view);

//		userToShow = ((ProfileActivity) getActivity()).getUserToShow();

		recList = (RecyclerView) view.findViewById(R.id.placesList);
//		recList.setNestedScrollingEnabled(false);
		recList.setHasFixedSize(true);
		GridLayoutManager llm = new GridLayoutManager(getContext(), 2);
		llm.setOrientation(GridLayoutManager.VERTICAL);
		recList.setLayoutManager(llm);

		locationAdapter = new LocationAdapter(locations, getActivity(), userToShow, this);
		recList.setAdapter(locationAdapter);

		if (SessionData.getCurrentUser() != null) {
			if (userToShow.getId() == SessionData.getCurrentUser().getId()) {
				adLocationButton.setVisibility(View.VISIBLE);
				adLocationButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						getActivity().startActivityForResult(new Intent("fhhgb.travis.philipp.travis_android.SearchLocationActivity"),
								visitedLocationRequestCode);

					}
				});
			} else {
				adLocationButton.setVisibility(View.INVISIBLE);
			}
		}
		if (locations != null) {
			if (locations.size() <= 0) {
				initDiaryView();
			}
		}

		// Inflate the layout for this fragment
		return view;
	}

	public void setUserToShow(User userProfile) {
		userToShow = userProfile;
		locations = new LinkedList<>();
	}

	/*
	* this method gets called from the activity ProfileActivity
	 */
	public void initDiaryView() {

		LocationService locationService = SessionData.getRestClient().getLocationService();

		Call<List<VisitedLocationElement>> retval = locationService.showVisitedLocationsForUser(userToShow.getId());

		retval.enqueue(new Callback<List<VisitedLocationElement>>() {

			@Override
			public void onResponse(Response<List<VisitedLocationElement>> response, Retrofit retrofit) {
				//Toast.makeText(getContext(), "RESP ", Toast.LENGTH_LONG).show();

				if (response.body() != null) {
					locations.addAll(response.body());
					locationAdapter.notifyDataSetChanged();
				}
			}

			@Override
			public void onFailure(Throwable t) {
//				Toast.makeText(PlacesFragment.this.getActivity(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			if (requestCode == visitedLocationRequestCode) {
				Bundle resultBundle = data.getExtras();
				Location foundLocation = (Location) resultBundle.getSerializable(Constants.FOUND_LOCATION);
				addFoundLocation(foundLocation);
			}
		}
	}

	private void addFoundLocation(final Location foundLocation) {
		LocationService locationService = SessionData.getRestClient().getLocationService();

		Call<Void> retval = locationService.userVisitedLocation(foundLocation.getId());

		retval.enqueue(new Callback<Void>() {

			@Override
			public void onResponse(Response<Void> response, Retrofit retrofit) {
				//Toast.makeText(getContext(), "RESP ", Toast.LENGTH_LONG).show();

				VisitedLocation visitedLocation = new VisitedLocation();
				VisitedLocationElement visitedLocationElement = new VisitedLocationElement();
				visitedLocationElement.setId(foundLocation.getId());
				visitedLocationElement.setName(foundLocation.getName());
				visitedLocationElement.setGooglePlaceId(foundLocation.getGooglePlaceId());
				visitedLocation.setLocation(visitedLocationElement);

				locations.add(visitedLocationElement);
				locationAdapter.notifyDataSetChanged();
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getContext(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public void onConnected(Bundle bundle) {

	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {

	}
}
