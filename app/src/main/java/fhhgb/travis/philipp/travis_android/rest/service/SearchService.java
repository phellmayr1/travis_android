package fhhgb.travis.philipp.travis_android.rest.service;

import java.util.List;

import fhhgb.travis.philipp.travis_android.rest.model.response.Location;
import fhhgb.travis.philipp.travis_android.rest.model.response.SearchResultUser;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;

/**
 * Created by Hellmayr on 19.04.2016.
 */
public interface SearchService {


	@Headers("Content-Type: application/json")
	@GET("search/user/{searchText}")
	public Call<List<SearchResultUser>> searchUser(@Path("searchText") String text);

	@Headers("Content-Type: application/json")
	@GET("search/location/{searchText}")
	public Call<List<Location>> searchPlace(@Path("searchText") String text);

}
