package fhhgb.travis.philipp.travis_android.rest.service;

import fhhgb.travis.philipp.travis_android.rest.model.request.ActivatePushData;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.PUT;

/**
 * Created by Hellmayr on 26.03.2016.
 */
public interface PushService {

	@Headers("Content-Type: application/json")
	@PUT("user/pushNotification")
	public Call<Void> activatePush(@Body ActivatePushData pushData);


}
