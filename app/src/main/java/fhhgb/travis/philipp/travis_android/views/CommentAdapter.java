package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

import fhhgb.travis.philipp.travis_android.Constants;
import fhhgb.travis.philipp.travis_android.ProfileActivity;
import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.helper.DateUtils;
import fhhgb.travis.philipp.travis_android.rest.model.response.UserComment;

/**
 * Created by Mane on 09.05.16.
 */
public class CommentAdapter extends ArrayAdapter<UserComment> {

    Context context;
    int resource;
    List<UserComment> data = null;

    public CommentAdapter(Context context, int resource, List<UserComment> data) {
        super(context, resource, data);
        this.context = context;
        this.resource = resource;
        this.data = data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        ViewHolder viewHolder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.profileImage = (RoundedImageView) row.findViewById(R.id.profileimage);
            viewHolder.name = (TextView) row.findViewById(R.id.profileName);
            viewHolder.comment = (TextView) row.findViewById(R.id.txtTitle);
            viewHolder.time = (TextView) row.findViewById(R.id.createdTime);

            viewHolder.profileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProfileActivity.class);
                    intent.putExtra(Constants.USER_ID_TO_SHOW, data.get(position).getUser().getId());
                    context.startActivity(intent);
                }
            });

            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        final ViewHolder holder = viewHolder;

        UserComment user = data.get(position);
        viewHolder.comment.setText(user.getText());
        viewHolder.name.setText(user.getUser().getFirstName()+" "+user.getUser().getLastName());
        viewHolder.time.setText(DateUtils.formatDate(user.getCreatedTime()));

        ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

        final Bitmap[] bitmap = new Bitmap[1];

        String url = user.getUser().getImageUrl();
        url = url.replace("width=100", "width=150");
        url = url.replace("height=100", "height=150");


        imageLoader.loadImage(url, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.profileImage.setImageBitmap(loadedImage);
                notifyDataSetChanged();
            }
        });

        return row;
    }

    static class ViewHolder {

        TextView comment;
        TextView time;
        TextView name;
        RoundedImageView profileImage;
    }

}