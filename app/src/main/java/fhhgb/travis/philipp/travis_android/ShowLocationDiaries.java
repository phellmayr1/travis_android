package fhhgb.travis.philipp.travis_android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.rest.model.response.MainPageElement;
import fhhgb.travis.philipp.travis_android.rest.service.LocationService;
import fhhgb.travis.philipp.travis_android.views.MainPageElementAdapter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Show all diaries for a Location
 */
public class ShowLocationDiaries extends AppCompatActivity {

	//There is no paging implemented yet, so the last 500 entries will be shown
	int currentPage = 0;
	int pageSize = 500;

	MainPageElementAdapter mainPageAdapter;
	RecyclerView recList;

	int locationId;
	String locationName = "";

	private boolean loading = true;
	int pastVisiblesItems, visibleItemCount, totalItemCount;

	LinkedList<MainPageElement> mainPageElements;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_location_diaries);

		ButterKnife.bind(this);

		loadExtras();

		mainPageElements = new LinkedList<>();

		recList = (RecyclerView) findViewById(R.id.mainList);
		recList.setHasFixedSize(true);
		final LinearLayoutManager llm = new LinearLayoutManager(this);
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		recList.setLayoutManager(llm);

		mainPageAdapter = new MainPageElementAdapter(mainPageElements, this);
		recList.setAdapter(mainPageAdapter);

		recList.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				if (dy > 0) //check for scroll down
				{
					visibleItemCount = llm.getChildCount();
					totalItemCount = llm.getItemCount();
					pastVisiblesItems = llm.findFirstVisibleItemPosition();

					if (loading) {
						if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
							loading = false;
							loadOverviewElements();
						}
					}
				}
			}
		});

		loadOverviewElements();

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
		}

		return super.onOptionsItemSelected(item);
	}

	private void setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setTitle(locationName);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	private void loadExtras() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			locationId = extras.getInt(Constants.LOCATION_ID, -1);
			locationName = extras.getString(Constants.LOCATION_NAME, "");
			setupToolbar();
		}
	}

	public void loadOverviewElements() {

		LocationService locationService = SessionData.getRestClient().getLocationService();

		Call<List<MainPageElement>> retval = locationService.showDiariesForLocation(locationId, currentPage, pageSize);
		retval.enqueue(new Callback<List<MainPageElement>>() {
			@Override
			public void onResponse(Response<List<MainPageElement>> response, Retrofit retrofit) {

				mainPageElements.addAll(response.body());
				mainPageAdapter.notifyDataSetChanged();
				currentPage++;
				loading = true;
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getBaseContext(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
			}
		});

	}
}
