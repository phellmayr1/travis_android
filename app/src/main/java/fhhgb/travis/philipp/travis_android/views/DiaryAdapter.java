package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

import fhhgb.travis.philipp.travis_android.R;
import fhhgb.travis.philipp.travis_android.helper.DateUtils;
import fhhgb.travis.philipp.travis_android.helper.UrlBuilder;
import fhhgb.travis.philipp.travis_android.rest.RestConstants;
import fhhgb.travis.philipp.travis_android.rest.model.request.DiaryElement;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;

/**
 * @author Philipp
 *         Created on 11.01.2016.
 */

public class DiaryAdapter extends RecyclerView.Adapter<DiaryViewHolder> {

	public static final int VIEW_TYPE_IMAGE = 0;
	public static final int VIEW_TYPE_COORDINATES = 1;
	public static final int VIEW_TYPE_TEXT = 2;

	private List<DiaryElement> diaryList;

	private Activity currentActivity;
	private User userToShow;

	public DiaryAdapter(List<DiaryElement> diaryList, Activity currentActivity, User userToShow) {
		this.diaryList = diaryList;
		this.currentActivity = currentActivity;
		this.userToShow = userToShow;
	}

	@Override
	public int getItemViewType(int position) {
		DiaryElement currentElement = diaryList.get(position);

		if (currentElement.getDiaryImageElement() != null) {
			return VIEW_TYPE_IMAGE;
		} else if (currentElement.getCoordinatesElement() != null) {
			return VIEW_TYPE_COORDINATES;
		} else {
			return VIEW_TYPE_TEXT;
		}
	}

	@Override
	public DiaryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

		if (viewType == VIEW_TYPE_IMAGE || viewType == VIEW_TYPE_COORDINATES) {
			View itemView = LayoutInflater.
					from(parent.getContext()).
					inflate(R.layout.cardview_preview_diaryimage, parent, false);
			return new DiaryImageViewHolder(itemView, currentActivity, userToShow);

		} else {
			View itemView = LayoutInflater.
					from(parent.getContext()).
					inflate(R.layout.cardview_preview_diarytext, parent, false);
			return new DiaryTextViewHolder(itemView, currentActivity, userToShow);
		}
	}

	@Override
	public void onBindViewHolder(final DiaryViewHolder holder, int position) {
		final DiaryElement diaryElement = diaryList.get(position);
		holder.diaryTitle.setText(diaryElement.getName());

		holder.locationText.setText(diaryElement.getLocation().getName());
		holder.visitedTimeText.setText(DateUtils.formatDate(diaryElement.getCreatedTime()));
		holder.setDiaryId(diaryElement.getId());
		Typeface material = Typeface.createFromAsset(currentActivity.getAssets(), "material.ttf");
		holder.locationIcon.setTypeface(material);
		holder.visitedTimeIcon.setTypeface(material);

		if (holder instanceof DiaryTextViewHolder) {

			if (diaryElement.getTextElement() != null && diaryElement.getTextElement().getText() != null) {
				((DiaryTextViewHolder) holder).diaryText.setText(diaryElement.getTextElement().getText());
			}
		}

		if (holder instanceof DiaryImageViewHolder) {

			if (diaryElement.getDiaryImageElement() != null) {

				ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

				imageLoader.loadImage(RestConstants.BASE_URL + diaryElement.getDiaryImageElement().getImageUrl(), new
						SimpleImageLoadingListener() {
							@Override
							public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
								((DiaryImageViewHolder) holder).diaryImage.setImageBitmap(loadedImage);
							}
						});
			} else if (diaryElement.getCoordinatesElement() != null) {

				ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

				imageLoader.loadImage(UrlBuilder.buildMapImageUrl(diaryElement
						.getCoordinatesElement().getCoordinates().getLongitude(), diaryElement
						.getCoordinatesElement().getCoordinates().getLatitude()), new
						SimpleImageLoadingListener
								() {
							@Override
							public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
								((DiaryImageViewHolder) holder).diaryImage.setImageBitmap(loadedImage);
							}
						});
			}

		}
	}

	@Override
	public int getItemCount() {

		if (diaryList != null) {
			return diaryList.size();
		} else

		{
			return 0;
		}
	}
}