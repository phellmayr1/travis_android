package fhhgb.travis.philipp.travis_android.rest.model.response;

import java.io.Serializable;

/**
 * @author Philipp
 *         Created on 30.12.2015.
 */

public class Location implements Serializable {

	private int id;
	private String name;
	private int coordinatesId;

	private String googlePlaceId;

	public String getGooglePlaceId() {
		return googlePlaceId;
	}

	public void setGooglePlaceId(String googlePlaceId) {
		this.googlePlaceId = googlePlaceId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCoordinatesId() {
		return coordinatesId;
	}

	public void setCoordinatesId(int coordinatesId) {
		this.coordinatesId = coordinatesId;
	}
}
