package fhhgb.travis.philipp.travis_android.rest.model.response;

import java.util.Date;

/**
 * Created by Mane on 09.05.16.
 */
public class UserComment {
    private User user;
    private String text;
    private Date createdTime;

    public UserComment(){}

    public UserComment(User user, String text, Date createdTime) {
        this.user = user;
        this.text = text;
        this.createdTime = createdTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }
}