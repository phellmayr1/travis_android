package fhhgb.travis.philipp.travis_android.views;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;

import fhhgb.travis.philipp.travis_android.R;

/**
 * @author Philipp
 *         Created on 27.01.2016.
 */

public class MainPageImageElementViewHolder extends MainPageElementViewHolder {

	public ImageView diaryImage;

	public MainPageImageElementViewHolder(View itemView, Activity activity) {
		super(itemView, activity);
		diaryImage = (ImageView) itemView.findViewById(R.id.card_image);


	}
}
