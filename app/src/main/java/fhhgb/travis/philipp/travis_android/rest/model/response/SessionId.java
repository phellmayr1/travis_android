package fhhgb.travis.philipp.travis_android.rest.model.response;

/**
 * @author Philipp
 *         Created on 23.12.2015.
 */

public class SessionId {

	private String sessionId;

	private int userId;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
}
