package fhhgb.travis.philipp.travis_android.rest.model.response;

/**
 * @author Philipp
 *         Created on 22.01.2016.
 */

public class DiaryCoordinatesElementDetails {

	private int id;
	private double latitude;
	private double longitude;
	private double altitude;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}
}
