package fhhgb.travis.philipp.travis_android.rest.model.request;

/**
 * Created by Hellmayr on 30.04.2016.
 */
public class Rating {

	String ratingCategory;
	int rating;

	public Rating(String ratingCategory, int rating) {
		this.ratingCategory = ratingCategory;
		this.rating = rating;
	}

	public String getRatingCategory() {
		return ratingCategory;
	}

	public void setRatingCategory(String ratingCategory) {
		this.ratingCategory = ratingCategory;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
}
