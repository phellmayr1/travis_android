package fhhgb.travis.philipp.travis_android.rest.service;

import java.util.List;

import fhhgb.travis.philipp.travis_android.rest.model.request.LocationWithCoordinates;
import fhhgb.travis.philipp.travis_android.rest.model.response.DiaryCoordinatesElementDetails;
import fhhgb.travis.philipp.travis_android.rest.model.response.Location;
import fhhgb.travis.philipp.travis_android.rest.model.response.MainPageElement;
import fhhgb.travis.philipp.travis_android.rest.model.response.SearchResultLocationList;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;
import fhhgb.travis.philipp.travis_android.rest.model.response.VisitedLocationElement;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * @author Philipp
 *         Created on 30.12.2015.
 */

public interface LocationService {

	@Headers("Content-Type: application/json")
	@POST("location")
	public Call<Location> createLocation(@Body LocationWithCoordinates location);

	@Headers("Content-Type: application/json")
	@GET("location/search/{searchText}")
	public Call<SearchResultLocationList> searchLocation(@Path("searchText") String searchText);

	@Headers("Content-Type: application/json")
	@GET("/coordinates/{coordinatesId}")
	public Call<DiaryCoordinatesElementDetails> searchCoordinatesForLocation(@Path("coordinatesId") int coordinatesId);

	@Headers("Content-Type: application/json")
	@GET("location/{locationId}")
	Call<LocationWithCoordinates> searchLocationById(@Path("locationId") int locationId);

	@Headers("Content-Type: application/json")
	@POST("user/visitedLocation/{locationId}")
	Call<Void> userVisitedLocation(@Path("locationId") int locationId);

	@Headers("Content-Type: application/json")
	@DELETE("user/visitedLocation/{locationId}")
	Call<Void> userUnVisitedLocation(@Path("locationId") int locationId);

	@Headers("Content-Type: application/json")
	@GET("user/{id}/visitedLocation")
	Call<List<VisitedLocationElement>> showVisitedLocationsForUser(@Path("id") int userId);

	@Headers("Content-Type: application/json")
	@GET("location/{locationId}/diary")
	Call<List<MainPageElement>> showDiariesForLocation(@Path("locationId") int locationId);

	@Headers("Content-Type: application/json")
	@GET("location/{locationId}/diary")
	Call<List<MainPageElement>> showDiariesForLocation(@Path("locationId") int locationId, @Query("page") int page, @Query("pageSize")
	int pageSize);

	@Headers("Content-Type: application/json")
	@GET("location/{locationId}/user")
	Call<List<User>> showVisitorsForLocation(@Path("locationId") int locationId, @Query("page") int page, @Query("pageSize")
	int pageSize, @Query("pImgWidth") int imgWidth, @Query("pImgHeight") int imgHeight);

}
