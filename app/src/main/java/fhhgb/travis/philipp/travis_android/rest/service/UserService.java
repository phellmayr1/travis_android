package fhhgb.travis.philipp.travis_android.rest.service;

import java.util.List;

import fhhgb.travis.philipp.travis_android.rest.model.request.Text;
import fhhgb.travis.philipp.travis_android.rest.model.response.Id;
import fhhgb.travis.philipp.travis_android.rest.model.response.MainPageElement;
import fhhgb.travis.philipp.travis_android.rest.model.response.SearchResultUser;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * @author Philipp
 *         Created on 24.12.2015.
 */

public interface UserService {

	@Headers("Content-Type: application/json")
	@GET("user/myProfile")
	public Call<User> getMyProfile();

	@Headers("Content-Type: application/json")
	@POST("user/profileShortText")
	public Call<Id> changeShortText(@Body Text text);

	@Headers("Content-Type: application/json")
	@POST("user/profileLongText")
	public Call<Id> changeLongText(@Body Text text);

	@Headers("Content-Type: application/json")
	@POST("user/currentLocation/{locationId}")
	public Call<Id> changeCurrentLocation(@Path("locationId") int locationId);

	@Headers("Content-Type: application/json")
	@POST("user/nextLocation/{locationId}")
	public Call<Id> changeNextLocation(@Path("locationId") int locationId);

	@Headers("Content-Type: application/json")
	@POST("user/homeLocation/{locationId}")
	public Call<Id> changeHomeLocation(@Path("locationId") int locationId);

	@Headers("Content-Type: application/json")
	@GET("user/{id}/profile")
	public Call<User> getProfile(@Path("id") String profileId, @Query("pImgWidth") int imgWidth, @Query("pImgHeight") int imgHeight);

	@Headers("Content-Type: application/json")
	@POST("user/followUser/{followUserId}")
	public Call<Void> followUser(@Path("followUserId") int followUserId);

	@Headers("Content-Type: application/json")
	@DELETE("user/followUser/{followUserId}")
	public Call<Void> unFollowUser(@Path("followUserId") int followUserId);

	@Headers("Content-Type: application/json")
	@PUT("user/birthday/{birthday}")
	public Call<Void> changeBirthday(@Path("birthday") String birthday);

	@Headers("Content-Type: application/json")
	@GET("user/mainPage")
	public Call<List<MainPageElement>> createMainPage(@Query("page") int page, @Query("pageSize") int order);

	@Headers("Content-Type: application/json")
	@GET("user/{id}/likedDiaryEntries")
	public Call<List<MainPageElement>> searchLikedDiaryEntries(@Path("id") int userId, @Query("page") int page, @Query("pageSize") int
			pageSize,  @Query("pImgWidth") int imgWidth, @Query("pImgHeight") int imgHeight);

	@Headers("Content-Type: application/json")
	@GET("user/{id}/follower")
	public Call<List<SearchResultUser>> findFollowerForUser(@Path("id") int userId, @Query("pImgWidth") int imgWidth, @Query
			("pImgHeight")
	int imgHeight);

	@Headers("Content-Type: application/json")
	@GET("user/{id}/following")
	Call<List<SearchResultUser>> findFollowingsForUser(@Path("id") int userId, @Query("pImgWidth") int imgWidth, @Query
			("pImgHeight")
	int imgHeight);

}
