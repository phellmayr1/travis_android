package fhhgb.travis.philipp.travis_android;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.helper.DateUtils;
import fhhgb.travis.philipp.travis_android.helper.UrlBuilder;
import fhhgb.travis.philipp.travis_android.rest.RestConstants;
import fhhgb.travis.philipp.travis_android.rest.model.request.LocationWithCoordinates;
import fhhgb.travis.philipp.travis_android.rest.model.response.MainPageElement;
import fhhgb.travis.philipp.travis_android.rest.model.response.Rating;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;
import fhhgb.travis.philipp.travis_android.rest.service.LocationService;
import fhhgb.travis.philipp.travis_android.rest.service.RatingService;
import fhhgb.travis.philipp.travis_android.views.LocationVisitorsAdapter;
import fhhgb.travis.philipp.travis_android.views.MainPageElementViewHolder;
import fhhgb.travis.philipp.travis_android.views.MainPageImageElementViewHolder;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Shows information for a location,
 * including a map, friends that visited this location, the last three diary entries and the ratings
 */
public class LocationDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

	@Bind(R.id.newDiaryEntryButton)
	Button newDiaryButton;

	@Bind(R.id.visitedButton)
	Button visitedButton;

	@Bind(R.id.locationName)
	TextView locationName;

	@Bind(R.id.newRatingButton)
	Button newRatingButton;

	@Bind(R.id.collapsing_toolbar)
	CollapsingToolbarLayout collapsingToolbarLayout;

	@Bind(R.id.diaryCardsContainer)
	LinearLayout diaryCardsContainer;

	@Bind(R.id.averageRatingText)
	TextView averageRatingTextView;

	@Bind(R.id.averageRatingBar)
	RatingBar averageRatingBar;

	@Bind(R.id.foodText)
	TextView foodText;

	@Bind(R.id.foodRatingBar)
	RatingBar foodRatingBar;

	@Bind(R.id.partyText)
	TextView partyText;

	@Bind(R.id.partyRatingBar)
	RatingBar partyRatingBar;

	@Bind(R.id.sightText)
	TextView sightText;

	@Bind(R.id.sightRatingBar)
	RatingBar sightRatingBar;

	@Bind(R.id.ratingSection)
	RelativeLayout ratingSection;

	@Bind(R.id.diarySection)
	RelativeLayout diarySection;

	@Bind(R.id.viewMoreCardView)
	CardView viewMoreCardView;

	@Bind(R.id.ratingCountText)
	TextView countTextView;

	@Bind(R.id.visitorsRecyclerView)
	RecyclerView visitorsRecyclerView;

	@Bind(R.id.visitorsSection)
	RelativeLayout visitorsSection;

	@Bind(R.id.viewMoreButton)
	Button viewMoreButton;

	@Bind(R.id.reviewedSection)
	LinearLayout reviewSection;

	int locationId;
	String googlePlaceId;
	LocationWithCoordinates locationToShow;

	LocationVisitorsAdapter visitedUserAdapter;
	LinkedList<User> users;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_location_details);
		ButterKnife.bind(this);

		setupToolbar();

		users = new LinkedList<>();

		loadExtras();
		setupVisitorsRecyclerView();

		ViewCompat.setTransitionName(findViewById(R.id.app_bar_layout), "EXTRA_IMAGE");
		collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

		//First, get all the locationDetails from the locationId
		final LocationService locationService = SessionData.getRestClient().getLocationService();

		Call<LocationWithCoordinates> retval = locationService.searchLocationById(locationId);
		retval.enqueue(new Callback<LocationWithCoordinates>() {

			@Override
			public void onResponse(Response<LocationWithCoordinates> response, Retrofit retrofit) {
				locationToShow = response.body();

				googlePlaceId = response.body().getGooglePlaceId();

				//Setup all the UI stuff with the received backend information

				setupMap();

				locationName.setText(locationToShow.getName());
				collapsingToolbarLayout.setTitle(locationToShow.getName());

				if (locationToShow.isRated()) {
					reviewSection.setVisibility(View.VISIBLE);
				}

				if (locationToShow.isVisited()) {
					visitedButton.setText(getResources().getString(R.string.visited));
					Drawable myIcon = getResources().getDrawable(R.drawable.location_visited);
					visitedButton.setCompoundDrawablesWithIntrinsicBounds(null, myIcon, null, null);
				} else {
					visitedButton.setText(getResources().getString(R.string.visit));
					Drawable myIcon = getResources().getDrawable(R.drawable.flag_not_visited);
					visitedButton.setCompoundDrawablesWithIntrinsicBounds(null, myIcon, null, null);
				}
				initVisitButtonListener();

				//Show all Diaries for this Location when this button was clicked
				viewMoreCardView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {

						Intent intent = new Intent(LocationDetailsActivity.this, ShowLocationDiaries.class);
						intent.putExtra(Constants.LOCATION_ID, locationId);
						intent.putExtra(Constants.LOCATION_NAME, locationToShow.getName());
						startActivity(intent);

					}
				});

			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getBaseContext(), "FAIL " + t.getMessage(), Toast.LENGTH_LONG).show();
			}
		});

		loadDiaryPreviewElements();

		newDiaryButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LocationDetailsActivity.this, CreateDiaryActivity.class);
				intent.putExtra(Constants.LOCATION_ID, locationId);
				startActivity(intent);
			}
		});

		newRatingButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(LocationDetailsActivity.this, RatingActivity.class);
				intent.putExtra(Constants.LOCATION_ID, locationId);
				startActivity(intent);
				finish();

			}
		});

		viewMoreButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LocationDetailsActivity.this, RatingDetailsActivity.class);
				intent.putExtra(Constants.LOCATION_ID, locationId);
				startActivity(intent);
			}
		});

		initRatingCard();

	}

	private void setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	}

	@Override
	public void onMapReady(GoogleMap map) {
		LatLng sydney = new LatLng(locationToShow.getLatitude(), locationToShow.getLongitude());

		map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));

		map.getUiSettings().setScrollGesturesEnabled(false);

	}

	/**
	 * Setup the map on the top of the view, to show the user the location of the place.
	 */
	private void setupMap() {

		MapFragment mapFragment = (MapFragment) getFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);

	}

	/**
	 * This method shows friends that have already visited this location in a RecyclerView
	 */
	private void setupVisitorsRecyclerView() {

		LinearLayoutManager layoutManager
				= new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

		visitorsRecyclerView.setLayoutManager(layoutManager);

		LocationService locationService = SessionData.getRestClient().getLocationService();

		Call<List<User>> retval = locationService.showVisitorsForLocation(locationId, 0, 20, Constants.FACEBOOK_IMAGE_WIDTH, Constants
				.FACEBOOK_IMAGE_HEIGHT);

		retval.enqueue(new Callback<List<User>>() {

			@Override
			public void onResponse(Response<List<User>> response, Retrofit retrofit) {
				//Toast.makeText(getContext(), "RESP ", Toast.LENGTH_LONG).show();

				if (response.body() != null && response.body().size() > 0) {
					visitorsSection.setVisibility(View.VISIBLE);
				}
				users.clear();
				users.addAll(response.body());

				visitedUserAdapter = new LocationVisitorsAdapter(users, LocationDetailsActivity.this);
				visitorsRecyclerView.setAdapter(visitedUserAdapter);

				visitedUserAdapter.notifyDataSetChanged();

			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});

	}

	/**
	 * This methods loads the UI information for the ratings from the server.
	 * This is to display one Cardview with a summary of all the ratings for the location.
	 */
	private void initRatingCard() {

		RatingService ratingService = SessionData.getRestClient().getRatingService();

		//Get the ratings for this current location
		Call<List<Rating>> b = ratingService.findRatingsForLocation(locationId);

		b.enqueue(new Callback<List<Rating>>() {

			@Override
			public void onResponse(Response<List<Rating>> response, Retrofit
					retrofit) {

				if (response.body() != null) {
					if (response.body().size() > 0) {
						ratingSection.setVisibility(View.VISIBLE);
					}

					//Iterate over all the rating categories
					for (Rating rating : response.body()) {

						//If the rating category matches a certain type, that is shown in the UI,
						// then set the rating to the UI

						if (rating.getRatingCategory().equals(Constants.RATING_CATEGORY_FOOD)) {
							foodText.setText(getString(R.string.food));
							foodRatingBar.setRating((float) rating.getAverage());
						}

						if (rating.getRatingCategory().equals(Constants.RATING_CATEGORY_PARTY)) {
							partyText.setText(getString(R.string.party));
							partyRatingBar.setRating((float) rating.getAverage());
						}

						if (rating.getRatingCategory().equals(Constants.RATING_CATEGORY_ADVENTURE)) {
							sightText.setText(getString(R.string.adventure));
							sightRatingBar.setRating((float) rating.getAverage());
						}

						//This is a summary of all rating categories
						if (rating.getRatingCategory().equals(Constants.RATING_CATEGORY_ALL)) {

							//Round the rating so it does not show to many digits after the comma
							String roundedRating = String.format("%.1f", rating.getAverage());

							averageRatingTextView.setText(roundedRating);
							averageRatingBar.setRating((float) rating.getAverage());
							countTextView.setText(rating.getCount() + " " + getString(R.string.reviews));
						}

					}
				}
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
			}

		});
	}

	/**
	 * This methods is responsible for the look of the visited button.
	 * Deponding on weather the user has visited the location or not, the button looks different.
	 */
	private void initVisitButtonListener() {

		visitedButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (locationToShow.isVisited()) {
					locationToShow.setVisited(false);
					setLocationAsUnVisited();
					visitedButton.setText(getResources().getString(R.string.visit));
					Drawable myIcon = getResources().getDrawable(R.drawable.flag_not_visited);
					visitedButton.setCompoundDrawablesWithIntrinsicBounds(null, myIcon, null, null);
				} else {
					locationToShow.setVisited(true);
					setLocationAsVisited();
					visitedButton.setText(getResources().getString(R.string.visited));
					Drawable myIcon = getResources().getDrawable(R.drawable.location_visited);
					visitedButton.setCompoundDrawablesWithIntrinsicBounds(null, myIcon, null, null);
				}
			}
		});
	}

	private void setLocationAsUnVisited() {

		LocationService locationService = SessionData.getRestClient().getLocationService();

		Call<Void> retval = locationService.userUnVisitedLocation(locationId);

		retval.enqueue(new Callback<Void>() {

			@Override
			public void onResponse(Response<Void> response, Retrofit retrofit) {
				//Toast.makeText(getContext(), "RESP ", Toast.LENGTH_LONG).show();
				setupVisitorsRecyclerView();

			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	private void setLocationAsVisited() {

		LocationService locationService = SessionData.getRestClient().getLocationService();

		Call<Void> retval = locationService.userVisitedLocation(locationId);

		retval.enqueue(new Callback<Void>() {

			@Override
			public void onResponse(Response<Void> response, Retrofit retrofit) {
				//Toast.makeText(getContext(), "RESP ", Toast.LENGTH_LONG).show();
				setupVisitorsRecyclerView();
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
		}

		return super.onOptionsItemSelected(item);
	}

	private void loadExtras() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			locationId = extras.getInt(Constants.LOCATION_ID, -1);
		}
	}

	/**
	 * Show the last three diary entries for this Location as preview
	 */
	private void loadDiaryPreviewElements() {
		final LayoutInflater inflater = LayoutInflater.from(this);

		LocationService locationService = SessionData.getRestClient().getLocationService();

		//Get three elements
		Call<List<MainPageElement>> b = locationService.showDiariesForLocation(locationId, 0, 3);
		b.enqueue(new Callback<List<MainPageElement>>() {

			          @Override
			          public void onResponse(Response<List<MainPageElement>> response, Retrofit retrofit) {

				          if (response.body() != null) {

					          if (response.body().size() > 0) {
						          diarySection.setVisibility(View.VISIBLE);
					          }

					          //Iterate over the three elements and check if it is an image, text or map element
					          for (MainPageElement element : response.body()) {

						          //Is it a map or an image?
						          if (element.getDiaryElement().getDiaryImageElement() != null || element.getDiaryElement()
								          .getCoordinatesElement() != null) {
							          CardView imageCardView = (CardView) inflater.inflate(R.layout.cardview_mainpage_diaryimage,
									          diaryCardsContainer, false);

							          final MainPageImageElementViewHolder holder = new MainPageImageElementViewHolder(imageCardView,
									          LocationDetailsActivity
											          .this);
							          initCardContent(element, holder);

							          final ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

							          //Do the image element specific stuff
							          if (element.getDiaryElement().getDiaryImageElement() != null) {

								          imageLoader.loadImage(RestConstants.BASE_URL + element.getDiaryElement().getDiaryImageElement()
										          .getImageUrl(), new
										          SimpleImageLoadingListener() {
											          @Override
											          public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
												          holder.diaryImage.setImageBitmap(loadedImage);
											          }
										          });

							          }
							          //Do the map element specific stuff
							          else if (element.getDiaryElement().getCoordinatesElement() != null) {

								          imageLoader.loadImage(UrlBuilder.buildMapImageUrl(element.getDiaryElement()
												          .getCoordinatesElement().getCoordinates().getLongitude(),
										          element.getDiaryElement()
												          .getCoordinatesElement().getCoordinates().getLatitude()), new
										          SimpleImageLoadingListener
												          () {
											          @Override
											          public void onLoadingComplete(String imageUri, View view, Bitmap
													          loadedImage) {
												          ((MainPageImageElementViewHolder) holder).diaryImage.setImageBitmap
														          (loadedImage);
											          }
										          });
							          }

							          diaryCardsContainer.addView(imageCardView);
						          }
						          //If it is not a map or an image, it must be a text element
						          else {

							          CardView textCardView = (CardView) inflater.inflate(R.layout.cardview_mainpage_diarytext,
									          diaryCardsContainer, false);

							          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
								          textCardView.setElevation(8);
							          }
							          textCardView.setUseCompatPadding(true);

							          final MainPageElementViewHolder holder = new MainPageElementViewHolder(textCardView,
									          LocationDetailsActivity
											          .this);

							          initCardContent(element, holder);

							          diaryCardsContainer.addView(textCardView);
							          diaryCardsContainer.requestLayout();
						          }
					          }
				          }
			          }

			          @Override
			          public void onFailure(Throwable t) {
				          Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
			          }
		          }

		);
	}

	/**
	 * Init the Cardview contents of the diary entry preview
	 *
	 * @param element
	 * @param holder
	 */
	private void initCardContent(MainPageElement element, final MainPageElementViewHolder holder) {
		holder.nameTextView.setText(element.getUser().getFirstName() + " " + element.getUser().getLastName());
		holder.timeTextView.setText(DateUtils.formatDate(element.getDiaryElement().getCreatedTime()));
		holder.diaryLocation.setText(element.getDiaryElement().getLocation().getName());
		holder.diaryName.setText(element.getDiaryElement().getName());
		holder.setDiaryId(element.getDiaryElement().getId());
		holder.setUsername(element.getUser().getFirstName() + " " + element.getUser().getLastName());
		holder.setDiaryTitle(element.getDiaryElement().getName());
		holder.setUserId(element.getUser().getId());
		holder.commentCount.setText(element.getDiaryElement().getCommentCount() + "");

		ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

		imageLoader.loadImage(element.getUser().getImageUrl(), new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				holder.profileImage.setImageBitmap(loadedImage);
			}
		});
	}
}
