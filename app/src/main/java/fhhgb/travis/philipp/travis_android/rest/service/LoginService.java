package fhhgb.travis.philipp.travis_android.rest.service;

import fhhgb.travis.philipp.travis_android.rest.model.request.SmallUser;
import fhhgb.travis.philipp.travis_android.rest.model.response.SessionId;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * @author Philipp
 *         Created on 23.12.2015.
 */

public interface LoginService {

	@Headers("Content-Type: application/json")
	@POST("user/login")
	public Call<SessionId> loginUser(@Body SmallUser smallUser);

	@Headers("Content-Type: application/json")
	@POST("user/logout")
	public Call<Void> logoutUser();
}
