package fhhgb.travis.philipp.travis_android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.RatingBar;
import android.widget.Toast;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fhhgb.travis.philipp.travis_android.rest.model.response.Rating;
import fhhgb.travis.philipp.travis_android.rest.service.RatingService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Show all accumulated ratings for all rating categories for a certain location
 */
public class RatingDetailsActivity extends AppCompatActivity {

	@Bind(R.id.foodRatingBar)
	RatingBar foodRatingBar;

	@Bind(R.id.partyRatingBar)
	RatingBar partyRatingBar;

	@Bind(R.id.sightRatingBar)
	RatingBar sightRatingBar;

	@Bind(R.id.accomodationRatingBar)
	RatingBar accomodationRatingBar;

	@Bind(R.id.shoppingRatingBar)
	RatingBar shoppingRatingBar;

	@Bind(R.id.activitiesRatingBar)
	RatingBar activitiesRatingBar;

	@Bind(R.id.adventureRatingBar)
	RatingBar adventureRatingBar;

	@Bind(R.id.safetyRatingBar)
	RatingBar safetyRatingBar;

	@Bind(R.id.wellnessRatingBar)
	RatingBar wellnessRatingBar;

	private int locationId;

	private void loadExtras() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			locationId = extras.getInt(Constants.LOCATION_ID, -1);
		}
	}

	/**
	 * Load the ratings for all categories from the backend and display the results in the UI
	 */
	private void initRatings() {

		RatingService ratingService = SessionData.getRestClient().getRatingService();

		Call<List<Rating>> b = ratingService.findRatingsForLocation(locationId);

		b.enqueue(new Callback<List<Rating>>() {

			@Override
			public void onResponse(Response<List<Rating>> response, Retrofit
					retrofit) {

				if (response.body() != null) {

					for (Rating rating : response.body()) {

						if (rating.getRatingCategory().equals(Constants.RATING_CATEGORY_FOOD)) {
							foodRatingBar.setRating((float) rating.getAverage());
						}

						if (rating.getRatingCategory().equals(Constants.RATING_CATEGORY_PARTY)) {
							partyRatingBar.setRating((float) rating.getAverage());
						}

						if (rating.getRatingCategory().equals(Constants.RATING_CATEGORY_ATTRACTIONS)) {
							sightRatingBar.setRating((float) rating.getAverage());
						}

						if (rating.getRatingCategory().equals(Constants.RATING_CATEGORY_ADVENTURE)) {
							adventureRatingBar.setRating((float) rating.getAverage());
						}
						if (rating.getRatingCategory().equals(Constants.RATING_CATEGORY_ACCOMODATION)) {
							accomodationRatingBar.setRating((float) rating.getAverage());
						}
						if (rating.getRatingCategory().equals(Constants.RATING_CATEGORY_SHOPPING)) {
							shoppingRatingBar.setRating((float) rating.getAverage());
						}
						if (rating.getRatingCategory().equals(Constants.RATING_CATEGORY_ACTIVITIES)) {
							activitiesRatingBar.setRating((float) rating.getAverage());
						}
						if (rating.getRatingCategory().equals(Constants.RATING_CATEGORY_SAFETY)) {
							safetyRatingBar.setRating((float) rating.getAverage());
						}
						if (rating.getRatingCategory().equals(Constants.RATING_CATEGORY_WELLNES)) {
							wellnessRatingBar.setRating((float) rating.getAverage());
						}
					}
				}
			}

			@Override
			public void onFailure(Throwable t) {
				Toast.makeText(getBaseContext(), "FAIL", Toast.LENGTH_LONG).show();
			}

		});
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rating_details);
		ButterKnife.bind(this);

		loadExtras();
		setupToolbar();
		initRatings();

	}

	private void setupToolbar() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
		}

		return super.onOptionsItemSelected(item);
	}
}
