package fhhgb.travis.philipp.travis_android.model;

import android.support.v7.widget.CardView;

/**
 * @author Philipp
 *         Created on 04.01.2016.
 */

public class CreateDiaryElement {

	CardView cardview;
	int sort;

	public CreateDiaryElement(int sort) {
		this.sort = sort;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public CardView getCardview() {
		return cardview;
	}

	public void setCardview(CardView cardview) {
		this.cardview = cardview;
	}

}
