package fhhgb.travis.philipp.travis_android.helper;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Philipp
 *         Created on 23.12.2015.
 */

public class DateUtils {

	/*
	* returns a string in the format 2014-06-03
	 */
	public static String intToDateString(int year, int month, int day) {
		String monthstr = month + "";
		String daystr = day + "";
		if (month < 10) {
			monthstr = "0" + monthstr;
		}
		if (day < 10) {
			daystr = "0" + daystr;
		}
		return year + "-" + monthstr + "-" + daystr;
	}

	public static int calculateAge(Date dateOfBirth) {
		Calendar dob = Calendar.getInstance();
		dob.setTime(dateOfBirth);
		Calendar today = Calendar.getInstance();
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
			age--;
		} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
				&& today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
			age--;
		}
		return age;
	}

	public static String formatDate(Date date) {

		//TODO: add to resources
		if (isDateYesterday(date)) {
			return "Yesterday";
		} else if (isDayToday(date)) {
			return "Today";
		}

		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(date);
	}

	public static boolean isDateYesterday(Date date) {
		Calendar c1 = Calendar.getInstance(); // today
		c1.add(Calendar.DAY_OF_YEAR, -1); // yesterday

		Calendar c2 = Calendar.getInstance();
		c2.setTime(date); // your date

		if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
				&& c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR)) {
			return true;
		}
		return false;
	}

	public static boolean isDayToday(Date date) {
		Calendar c1 = Calendar.getInstance(); // today
		Calendar c2 = Calendar.getInstance();
		c2.setTime(date); // your date

		if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
				&& c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR)) {
			return true;
		}
		return false;
	}

	public static Date intToDate(int year, int month, int day) {

		Calendar cal = Calendar.getInstance();
		cal.set(year, month, day);
		return cal.getTime();

	}

	public static String formatDateInts(int year, int monthOfYear, int dayOfMonth) {
		Date date = new Date(year-1900, monthOfYear, dayOfMonth);
		SimpleDateFormat dt1 = new SimpleDateFormat("EE, MMM d, yyyy");
		return dt1.format(date);
	}
}
