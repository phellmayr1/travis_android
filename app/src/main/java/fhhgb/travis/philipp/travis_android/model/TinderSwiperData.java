package fhhgb.travis.philipp.travis_android.model;

import android.graphics.Bitmap;

/**
 * Created by Hellmayr on 21.05.2016.
 */
public class TinderSwiperData {

	private String description;
	private Bitmap image;
	private String categoryType;

	public TinderSwiperData(String description, Bitmap image, String categoryType) {
		this.description = description;
		this.image = image;
		this.categoryType = categoryType;
	}

	public TinderSwiperData(Bitmap image, String description) {
		this.image = image;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public Bitmap getImage() {
		return image;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
}
