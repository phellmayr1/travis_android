package fhhgb.travis.philipp.travis_android;

import android.graphics.Bitmap;

import com.facebook.Profile;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;

import java.util.LinkedList;

import fhhgb.travis.philipp.travis_android.model.DiaryUploadStatus;
import fhhgb.travis.philipp.travis_android.rest.RestClient;
import fhhgb.travis.philipp.travis_android.rest.model.response.User;

/**
 * @author Philipp
 *         Created on 01.11.2015.
 */

public class SessionData {

	//Used to get photos from Google Places
	private static GoogleApiClient mGoogleApiClient;

	/**
	 * Get the current GoogleApiClient or create a new one, if it does not exist.
	 * This is kind of a hacky method, but there are sometimes very strange issues when always starting and stoping the
	 * GoogleApiClient.
	 * @param currentFragment the currentFragment is needed for the context
	 * @param <T>
	 * @return
	 */
	public static <T extends android.support.v4.app.Fragment & GoogleApiClient.ConnectionCallbacks & GoogleApiClient
			.OnConnectionFailedListener> GoogleApiClient
	getmGoogleApiClient(T
			                    currentFragment) {

		if (mGoogleApiClient == null) {
			mGoogleApiClient = new GoogleApiClient
					.Builder(currentFragment.getActivity())
					.addApi(Places.GEO_DATA_API)
					.addApi(Places.PLACE_DETECTION_API)
					.addConnectionCallbacks(currentFragment)
					.addOnConnectionFailedListener(currentFragment)
					.build();

			mGoogleApiClient.connect();

		}

		return mGoogleApiClient;
	}

	public static int tripOverviewFragmentId=0;

	public void setmGoogleApiClient(GoogleApiClient mGoogleApiClient) {
		this.mGoogleApiClient = mGoogleApiClient;
	}

	private static RestClient restClient;

	private static Profile fbProfile;

	private static Bitmap fbProfilePic;

	private static String sessionId;

	private static int userId;

	private static User currentUser;

	private static int currentUploadingCounter = 0;

	public static void increaseCurrentUploadingCounter() {
		currentUploadingCounter++;
	}

	public static void decreaseCurrentUploadingCounter() {
		currentUploadingCounter--;
	}

	private static Bitmap blurredStandardBackgroundImage;

	public static int getCurrentUploadingCounter() {
		return currentUploadingCounter;
	}

	public static void setCurrentUploadingCounter(int currentUploadingCounter) {
		SessionData.currentUploadingCounter = currentUploadingCounter;
	}

	private static LinkedList<DiaryUploadStatus> diaryUploadStatuses;

	public static LinkedList<DiaryUploadStatus> getDiaryUploadStatuses() {
		if (diaryUploadStatuses == null) {
			diaryUploadStatuses = new LinkedList<>();
		}
		return diaryUploadStatuses;
	}

	public static void setDiaryUploadStatuses(LinkedList<DiaryUploadStatus> diaryUploadStatuses) {
		SessionData.diaryUploadStatuses = diaryUploadStatuses;
	}

	public static Bitmap getBlurredStandardBackgroundImage() {
		return blurredStandardBackgroundImage;
	}

	public static void setBlurredStandardBackgroundImage(Bitmap blurredStandardBackgroundImage) {
		SessionData.blurredStandardBackgroundImage = blurredStandardBackgroundImage;
	}

	public static User getCurrentUser() {
		return currentUser;
	}

	public static void setCurrentUser(User currentUser) {
		SessionData.currentUser = currentUser;
	}

	public static String getSessionId() {
		return sessionId;
	}

	public static void setSessionId(String sessionId) {
		SessionData.sessionId = sessionId;
	}

	public static RestClient getRestClient() {
		return restClient;
	}

	public static void setRestClient(RestClient restClient) {
		SessionData.restClient = restClient;
	}

	public static Profile getFbProfile() {
		return fbProfile;
	}

	public static void setFbProfile(Profile fbProfile) {
		SessionData.fbProfile = fbProfile;
	}

	public static Bitmap getFbProfilePic() {
		return fbProfilePic;
	}

	public static void setFbProfilePic(Bitmap fbProfilePic) {
		SessionData.fbProfilePic = fbProfilePic;
	}

	public static int getUserId() {
		return userId;
	}

	public static void setUserId(int userId) {
		SessionData.userId = userId;
	}
}
